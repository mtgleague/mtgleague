@echo off

set server_cmd=-d server realize start
set db_cmd=-d db docker-compose up
set client_cmd= -d client yarn.cmd start

rem node 17系対策
set NODE_OPTIONS=--openssl-legacy-provider

start wt %server_cmd% ; split-pane -V %db_cmd% ; split-pane -V %client_cmd%
