
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE files(
    id serial primary key,
    name text not null,
    type text not null,
    data bytea not null,
    cid integer not null
);

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE files;
