# 分割カードを縦向きにする処理
############################

$Shell = New-Object -ComObject Shell.Application
$path = (Get-Location).Path + "\download"
$folder = $Shell.NameSpace($path)

$folder.Items() |
    ? { $_.Name -match "\.png$" } |
    ? {
        $width = $folder.GetDetailsOf($_, 176) -replace "\D*(\d+).+","`$1"
        $height = $folder.GetDetailsOf($_, 178) -replace "\D*(\d+).+","`$1"
        $width -gt $height
    } |
    % {
        ffmpeg -i $_.path -vf "transpose=1" temp.png
        Copy-Item temp.png $_.path -Force
        Remove-Item temp.png
    }

