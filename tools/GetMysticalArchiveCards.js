const cheerio = require("cheerio");
const fs = require("fs");
const request = require('request');

const TargetNames = [
"果敢な一撃/Defiant Strike",
"活力回復/Revitalize",
"神聖なる計略/Divine Gambit",
"神々の思し召し/Gods Willing",
"剣を鍬に/Swords to Plowshares",
"土地の寄進/Gift of Estates",
"儚い存在/Ephemerate",
"マナの税収/Mana Tithe",
"審判の日/Day of Judgment",
"テフェリーの防御/Teferi's Protection",
"副陽の接近/Approach of the Second Sun",
"選択/Opt",
"旋風のごとき否定/Whirlwind Denial",
"巧みな軍略/Strategic Planning",
"否認/Negate",
"渦まく知識/Brainstorm",
"記憶の欠落/Memory Lapse",
"強迫的な研究/Compulsive Research",
"対抗呪文/Counterspell",
"テゼレットの計略/Tezzeret's Gambit",
"青の太陽の頂点/Blue Sun's Zenith",
"時間のねじれ/Time Warp",
"精神の願望/Mind's Desire",
"強迫/Duress",
"苦悶の悔恨/Agonizing Remorse",
"取り除き/Eliminate",
"村の儀式/Village Rites",
"暗黒の儀式/Dark Ritual",
"苦悶の触手/Tendrils of Agony",
"コジレックの審問/Inquisition of Kozilek",
"血の署名/Sign in Blood",
"破滅の刃/Doom Blade",
"悪魔の教示者/Demonic Tutor",
"命運の核心/Crux of Fate",
"汚れた契約/Tainted Pact",
"初子さらい/Claim the Firstborn",
"ショック/Shock",
"胸躍る可能性/Thrill of Possibility",
"立腹/Infuriate",
"石の雨/Stone Rain",
"稲妻/Lightning Bolt",
"ウルザの激怒/Urza's Rage",
"信仰無き物あさり/Faithless Looting",
"ぶどう弾/Grapeshot",
"混沌のねじれ/Chaos Warp",
"高まる復讐心/Increasing Vengeance",
"ミジックスの熟達/Mizzix's Mastery",
"耕作/Cultivate",
"蛇皮のヴェール/Snakeskin Veil",
"冒険の衝動/Adventurous Impulse",
"嵐の乗り切り/Weather the Storm",
"新たな芽吹き/Regrowth",
"クローサの掌握/Krosan Grip",
"調和/Harmonize",
"豊穣な収穫/Abundant Harvest",
"自然の秩序/Natural Order",
"原初の命令/Primal Command",
"チャネル/Channel",
"灯の燼滅/Despark",
"電解/Electrolyze",
"化膿/Putrefy",
"稲妻のらせん/Lightning Helix",
"成長のらせん/Growth Spiral",
].map(s => s.split("/"));

const parseCardlist = sets => {
    const path = `./download/${sets}.txt`;
    if (!fs.existsSync(path)) return "";
    // console.log("load " + sets);
    const text = fs.readFileSync(path).toString();
    // return text.split("\n\n\n");
    return text;
};
const cardlistText = parseCardlist("STA");

function requestAsync(url, opt = {}) {
    return new Promise((resolve, reject) => {
        request(url, opt, (err, res, body) => {
            if (!err && res.statusCode == 200) {
                resolve(body);
            } else {
                reject(err);
            }
        });
    });
}
async function requestPage(url) {
    const body = await requestAsync(url);
    return cheerio.load(body);
}

async function getCard(id, imgObj) {
    const title = imgObj.attr("alt");
    console.log(`save ${title}...`);

    const cardPath = imgObj.attr("src");
    const path = /2021\/(.+)\/(.+)\.png/.exec(cardPath);
    const cardName = `STA_${("0000" + id).slice(-4)}`;

    if (fs.existsSync(`./download/${cardName}.json`)) {
        return JSON.parse(fs.readFileSync(`./download/${cardName}.json`));
    }

    request(cardPath, {encoding: null}, (err, res, body) => {
        fs.writeFileSync(
            `./download/${cardName}.png`,
            body,
            'binary');
    });

    const name = TargetNames.find(name => name[0] === title);
    const $wikiPage = await requestPage(`http://mtgwiki.com/wiki/${encodeURIComponent(name[0])}/${name[1]}`);

    const text = $wikiPage(".card").text();
    const cost = text.match(/(\(.+?\))+/);
    let costs = cost[0].replace(/\)\(/g, "),(");
    costs = costs.split(",")
        .map(s => s
            .replace("白", "W")
            .replace("青", "U")
            .replace("黒", "B")
            .replace("赤", "R")
            .replace("緑", "G")
            .replace("Ｘ", "X")
            .replace(/[０-９]/g, n => String.fromCharCode(n.charCodeAt(0) - 0xFEE0))
            )
        .join(",");
    // ミスティカルアーカイブはインスタントorソーサリーしかない
    const type = text.match(/(インスタント|ソーサリー)/);
    const textJP = $wikiPage(".card p").html().split("<br />").map(e => $wikiPage(`<div>${e}</div>`).text());

    const results = [];
    let reality = "";
    if (title === "高まる復讐心") {
        reality = "神話レア";
    } else {
        reality = $wikiPage.text().match(/カード個別評価：ミスティカルアーカイブ - (アンコモン|レア|神話レア)/)[1];
    }

    const res = {
        key: cardName,
        no: id,
        name: title,
        name_kana: "",
        set: "STA",
        cost: costs,
        power: null,
        toughness: null,
        loyalty: null,
        supertype: null,
        cardtype: type[0],
        subtype: null,
        reality,
        text: textJP.join("\n"),
        flavor_text: "",
    };

    m = cardlistText.match(`日本語名：${title}（(.+?)）`);
    if (m) {
        res.name_kana = m[1];
    } else {
        throw "parse kana error";
    }

    console.log(res);

    results.push(res);

    fs.writeFileSync(`./download/${cardName}.json`, JSON.stringify(results));
    console.log(`complete ${title}.`);
    return results;
}

async function main() {
    const $ = await requestPage("https://magic.wizards.com/ja/articles/archive/card-image-gallery/strixhaven-variants");

    const list0 = Array.from($(".resizing-cig img"))
        .filter(e => TargetNames.some(name => name[0] === $(e).attr("alt")));
    const list = [];
    for (let item of list0) {
        if (!list.some(i => $(i).attr("alt") === $(item).attr("alt"))) {
            list.push(item);
        }
    }
    let i = 1;
    const results = [];
    for (let item of list) {
        const obj = await getCard(i, $(item));
        i++;
        results.push(obj);
    }
    fs.writeFileSync(`./listdata/STA.json`, JSON.stringify(results));

    console.log("done.");
}

main();

//(async () => {
//    await getCard("https://mtg-jp.com/products/card-gallery/0000184/472967/");
//})();
