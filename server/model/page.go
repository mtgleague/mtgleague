package model

// Page はページの型
type Page struct {
	Key  string `json:"key" db:"key, primarykey"`
	Text string `json:"text" db:"text"`
}
