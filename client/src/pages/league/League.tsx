import React, { Component } from 'react';
import { RouteChildrenProps } from 'react-router-dom';
import { notification, Layout, Skeleton, DatePicker } from 'antd';
import { Content, FileData } from 'model/Content';
import API from 'api/API';
import { EventData } from 'model/Event';
import FileListView from './FileListView';
import PageTitle from './PageTitle';
import { User } from 'model/User';
import Comments from './Comments';
import MarkdownContent from './MarkdownContent';
import SaveButton from './SaveButton';
import moment from 'moment';
import { Card } from 'model/Card';
import Socket from 'api/Socket';

interface Props extends RouteChildrenProps {
  me: User;
  contentId: number | null;
  contents: Content[];
  onDelete: (deletedContentId: number) => void;
}
interface State {
  title: string;
  titleOriginal: string;
  content: string;
  contentOriginal: string;
  target_sets: string;
  week_max: number;
  events: EventData[];
  target_cards: Card[];
  files: FileData[];
  range: string[];
  rangeOriginal: string[];
  isEdit: boolean;
  isLoaded: boolean;
  isNotFound: boolean;
}

class League extends Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      title: "",
      titleOriginal: "",
      content: "",
      contentOriginal: "",
      target_sets: "",
      week_max: 3,
      events: [],
      target_cards: [],
      files: [],
      range: [],
      rangeOriginal: [],
      isEdit: false,
      isLoaded: false,
      isNotFound: false,
    };

    this.handleEditTitle = this.handleEditTitle.bind(this);
    this.handleEditContent = this.handleEditContent.bind(this);
    this.handleEditCancel = this.handleEditCancel.bind(this);
    this.handleEditSave = this.handleEditSave.bind(this);
    this.handleChangeEvent = this.handleChangeEvent.bind(this);
    this.handleUpdateFiles = this.handleUpdateFiles.bind(this);
    this.handleDeleteFile = this.handleDeleteFile.bind(this);

    this.handleSocketAddEvent = this.handleSocketAddEvent.bind(this);
    this.handleSocketEditEvent = this.handleSocketEditEvent.bind(this);
    this.handleSocketDeleteEvent = this.handleSocketDeleteEvent.bind(this);
  }

  componentDidMount() {
    this.reloadContent(this.props.contentId);
    Socket.addListener("add_event", this.handleSocketAddEvent);
    Socket.addListener("edit_event", this.handleSocketEditEvent);
    Socket.addListener("delete_event", this.handleSocketDeleteEvent);
  }

  componentWillUnmount() {
    Socket.removeListener("add_event", this.handleSocketAddEvent);
    Socket.removeListener("edit_event", this.handleSocketEditEvent);
    Socket.removeListener("delete_event", this.handleSocketDeleteEvent);
  }

  componentDidUpdate(prevProps: Props) {
    if (this.props.contentId !== prevProps.contentId) {
      this.reloadContent(this.props.contentId);
    }
  }

  handleSocketAddEvent(e: EventData) {
    if (this.props.contentId === e.content_id) {
      const index = this.state.events.findIndex(item => item.id === e.id);
      if (index === -1) {
        const events = this.state.events.slice();
        const data = { ...e, date: moment(e.date) };
        events.push(data);
        events.sort((a: EventData, b: EventData) => {
          if (a.date < b.date) return -1;
          if (a.date > b.date) return 1;
          return a.id - b.id;
        });
        this.setState({ events });
      }
    }
  }
  handleSocketEditEvent(e: EventData) {
    if (this.props.contentId === e.content_id) {
      const index = this.state.events.findIndex(item => item.id === e.id);
      if (index !== -1) {
        const events = this.state.events.slice();
        const data = { ...e, date: moment(e.date) };
        events[index] = data;
        events.sort((a: EventData, b: EventData) => {
          if (a.date < b.date) return -1;
          if (a.date > b.date) return 1;
          return a.id - b.id;
        });
        this.setState({ events });
      }
    }
  }
  handleSocketDeleteEvent(e: EventData) {
    if (this.props.contentId === e.content_id) {
      const index = this.state.events.findIndex(item => item.id === e.id);
      if (index !== -1) {
        const events = this.state.events.slice();
        events.splice(index, 1);
        this.setState({ events });
      }
    }
  }

  async reloadContent(index: number | null) {

    console.log("reload content to " + index);
    this.setState({ isLoaded: false });

    const state: any = {
      title: "",
      titleOriginal: "",
      content: "",
      contentOriginal: "",
      range: [],
      rangeOriginal: [],
      target_sets: "",
      isLoaded: false,
      isNotFound: false,
    };
    if (index === null) {
      const contents = this.props.contents;
      if (contents.length > 0) {
        const current = contents.find(c => moment(c.start_date).isBefore());
        current && this.props.history.push("/?id=" + current.id);
      }
      return state;
    }
    const promises: Promise<any>[] = [];
    {
      const res = await API.GET(`/contents/${index}`, e => {
        document.title = `${e.title} - ギャザ部`;
        state.title = state.titleOriginal = e.title;
        state.content = state.contentOriginal = e.text;
        state.target_sets = e.target_sets;
        state.range = state.rangeOriginal = [ e.start_date, e.end_date ];
        state.week_max = e.week_max;
      }, _ => {});
      // ページ取得エラー時は見つからないとして終わる
      if (res.code && res.code !== 200) {
        state.isLoaded = true;
        state.isNotFound = true;
        this.setState(state);
        return state;
      }
    }
    promises.push(API.GET(`/contents/${index}/files`, e => {
      state.files = e;
    }));
    promises.push(API.GET(`/events?content_id=${index}`, e => {
      state.events = e.map((v: any) => ({ ...v, date: moment(v.date) }));
      state.events.sort((a: EventData, b: EventData) => {
        if (a.date < b.date) return -1;
        if (a.date > b.date) return 1;
        return a.id - b.id;
      });
    }));
    // 対象のカードセットがある場合だけ取得する
    // これはページに必須ではないのでロード完了を待たなくていい
    if (state.target_sets) {
      API.GET(`/cards?sets=${state.target_sets.replace(",", " ")}&limit=2000`, e => {
        this.setState({ target_cards: e.cards });
      });
    } else {
      state.target_cards = [];
    }
    await Promise.all(promises);
    state.isLoaded = true;
    this.setState(state);
    return state;
  }

  handleEditTitle(value: string) {
    this.setState({ title: value });
  }
  handleEditContent(value: string) {
    this.setState({ content: value });
  }
  handleEditCancel() {
    this.setState(s => ({
      title: s.titleOriginal,
      range: s.rangeOriginal,
      content: s.contentOriginal,
      isEdit: false,
    }));
  }

  handleEditSave() {
    API.PUT(`/contents/${this.props.contentId}`, {
      title: this.state.title,
      text: this.state.content,
      week_max: this.state.week_max,
      target_sets: this.state.target_sets,
      start_date: moment(this.state.range[0]).format(),
      end_date: moment(this.state.range[1]).format(),
    }, e => {
      document.title = `${e.title} - ギャザ部`;
      this.setState({
        isEdit: false,
        title: e.title,
        titleOriginal: e.title,
        content: e.text,
        contentOriginal: e.text,
        range: [ e.start_date, e.end_date ],
        rangeOriginal: [ e.start_date, e.end_date ],
      });
      notification.success({
        message: "保存しました"
      });
    });
  }
  handleChangeEvent() {
    API.GET(`/events?content_id=${this.props.contentId}`, e => {
      const events = e.map((v: any) => ({ ...v, date: moment(v.date) }));
      events.sort((a: EventData, b: EventData) => {
        if (a.date < b.date) return -1;
        if (a.date > b.date) return 1;
        return a.id - b.id;
      });
      this.setState({ events });
    });
  }

  handleUpdateFiles(addedFiles: FileData[]) {
      this.setState(s => {
        let files = s.files.slice();
        files.push(...addedFiles);
        return { files };
      });
  }
  handleDeleteFile(item: FileData) {
    this.setState(s => {
      let files = s.files.slice();
      const i = files.findIndex(f => f.id === item.id);
      if (i === -1) {
        return null;
      }
      files.splice(i, 1);
      return { files };
    });
  }

  render = () => {

    const isChanged = 
      this.state.content !== this.state.contentOriginal ||
      this.state.title !== this.state.titleOriginal ||
      this.state.range.join(",") !== this.state.rangeOriginal.join(",");

    if (this.state.isNotFound) {
      return <div style={{ fontSize: "200%" }}>
        ページが見つかりません
      </div>;
    }
    return <div>
      {this.props.contentId && (this.state.isLoaded ? (
        <div>
          <PageTitle
            me={this.props.me}
            title={this.state.title}
            contentId={this.props.contentId}
            isEdit={this.state.isEdit}
            targetSets={this.state.target_sets}
            onChange={this.handleEditTitle}
            onMenuDelete={this.props.onDelete}
            onMenuEdit={() => this.setState({ isEdit: true })}
            />
          {this.state.isEdit && <>期間：<DatePicker.RangePicker
            defaultValue={[
              moment(this.state.range[0]),
              moment(this.state.range[1])
            ]}
            onChange={(_: any, formatString: string[]) => this.setState({ range: formatString })}
          /></>}
          <MarkdownContent
            me={this.props.me}
            contentId={this.props.contentId}
            content={this.state.content}
            events={this.state.events}
            target_cards={this.state.target_cards}
            isEdit={this.state.isEdit}
            onEventChange={this.handleChangeEvent}
            onChange={this.handleEditContent}
            onUploadFiles={this.handleUpdateFiles}
          />
          {(isChanged || this.state.isEdit) && <SaveButton
              onCancel={this.handleEditCancel}
              onSave={this.handleEditSave}
              canSave={isChanged}
              />
          }
          <Comments
            me={this.props.me}
            contentId={this.props.contentId}
            onUploadFiles={this.handleUpdateFiles}
            />
          <Layout.Footer>
            <FileListView
              files={this.state.files}
              onDelete={this.handleDeleteFile}
              />
          </Layout.Footer>
        </div>
      ) : <Skeleton active paragraph={{ rows: 30 }} />)}
    </div>
  };
}

export default League;
