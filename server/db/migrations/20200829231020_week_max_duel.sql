
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE contents ADD week_max integer;
UPDATE contents SET week_max = 3;
ALTER TABLE contents ALTER week_max SET NOT NULL;
ALTER TABLE contents ALTER week_max SET DEFAULT 3;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE contents DROP COLUMN week_max;
