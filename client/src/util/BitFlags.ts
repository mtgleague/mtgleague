import { isNumber } from "lodash";

interface TermType
{
  Term: number;
}

class BitFlags<T extends number>
{
  constructor(value: number | TermType = 0) {
    if (isNumber(value)) {
      this.value = value;
    } else {
      this.fill(value);
    }
  }
  get(index: T): boolean {
    return !!(this.value & (1 << index));
  }
  set(index: T, value: boolean = true) {
    if (value) {
      this.value |= (1 << index);
    } else {
      this.value &= ~(1 << index);
    }
  }
  fill(term: TermType) {
    this.value = (1 << term.Term) - 1;
  }
  clear() {
    this.value = 0;
  }
  any(): boolean {
    return !!this.value;
  }
  all(term: TermType): boolean {
    return this.value === (1 << term.Term) - 1;
  }
  clone(): BitFlags<T> {
    const newVal = new BitFlags<T>();
    newVal.value = this.value;
    return newVal;
  }

  value: number = 0;
}

export default BitFlags;
