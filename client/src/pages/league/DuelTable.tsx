import React, { useEffect, useState } from 'react';
import { Table, Tooltip } from 'antd';
import { 
  CheckCircleFilled,
  CloseCircleFilled,
} from '@ant-design/icons';
import style from 'assets/League.module.css';
import { EventData, DuelDetail, SystemDetail } from 'model/Event';

interface DuelTableData {
  name: string;
  data: boolean[];
  added: number[];
  win: number;
  lose: number;
  loseFromReset: number;
  point: number;
  rightTo: number;
  lastReset: number;
}

const DuelTableColumns = [
  {
    title: "名前",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "勝敗",
    dataIndex: "data",
    key: "data",
    render: (data: boolean[], row: DuelTableData) => <Tooltip title={
      <div>
        最終リセット後対戦数：{data.length - row.lastReset}
      </div>
    }>
      {data.map((b, i) => <span key={i} className={i < row.lastReset ? style["before-reset-icon"] : undefined} >{
        b ? <CheckCircleFilled className={style["win-icon"]} /> : <CloseCircleFilled className={style["lose-icon"]} />
      }</span>)}
    </Tooltip>
  },
  {
    title: "パック追加数",
    dataIndex: "added",
    key: "added",
    render: (data: number[]) => <span>
      {data.some(d => d !== 0) && (data.filter(d => d !== 0).map(d => d < 0 ? "reset" : d).join(" + "))}
    </span>
  },
  {
    title: "集計結果",
    dataIndex: "data",
    key: "total",
    render: (data: boolean[]) => <span>
      <span className={style["win-text"]}>{data.filter(b => b).length}勝</span> {data.filter(b => !b).length}敗
    </span>
  },
  {
    title: "勝ち点",
    dataIndex: "point",
    key: "point",
  },
  {
    title: "パック購入権",
    dataIndex: "rightTo",
    key: "rightTo",
    render: (data: number, row: DuelTableData) => {
      const battleCountAfterReset = row.data.length - row.lastReset;
      return <span>
        {((row.added[row.added.length - 1] >= 3 && data > 0)
        || battleCountAfterReset >= 30
        ) ? "リセット可能" : data}
      </span>
    }
  },
];

const CreateDuelTableData: (user:string)=>DuelTableData = user => ({
  name: user,
  data: [],
  added: [ 0 ],
  win: 0,
  lose: 0,
  loseFromReset: 0,
  point: 0,
  rightTo: 0,
  lastReset: 0,
});

const DuelTable = React.memo((props: { events: EventData[] }) => {
  const [data, setData] = useState<DuelTableData[]>([]);
  useEffect(() => {
    const events = props.events.filter(e => e.active);
    const data = new Map<string, DuelTableData>();
    let distribution = 0;
    events.forEach(e => {
      switch (e.type) {
        case "duel":
          {
            const d = e.detail as DuelDetail;
            const item1 = data.get(d.user1) || CreateDuelTableData(d.user1);
            const item2 = data.get(d.user2) || CreateDuelTableData(d.user2);
            !data.has(d.user1) && data.set(d.user1, item1);
            !data.has(d.user2) && data.set(d.user2, item2);
  
            item1.data.push(d.result === 0);
            item2.data.push(d.result === 1);
            if (d.result === 0) {
              item1.win++;
              item2.lose++;
              item1.point++;
              item2.point--;
              item2.loseFromReset++;
              item2.loseFromReset % 3 === 0 && item2.rightTo++;
            } else {
              item2.win++;
              item1.lose++;
              item2.point++;
              item1.point--;
              item1.loseFromReset++;
              item1.loseFromReset % 3 === 0 && item1.rightTo++;
            }
            break;
          }
        case "system":
          {
            const d = e.detail as SystemDetail;
            const item = data.get(d.username) || CreateDuelTableData(d.username);
            !data.has(d.username) && data.set(d.username, item);
            if (d.kind === "add") {
              item.added[item.added.length - 1]++;
              item.rightTo--;
            } else if (d.kind === "reset") {
              // リセットしたことを積む
              item.added.push(-1);
              // リセット後なので追加数はゼロとして積む
              item.added.push(0);
              // 権利のない状態で30戦ルールでリセットした場合
              if (item.rightTo <= 0 && item.data.length - item.lastReset >= 30) {
                item.rightTo++;
              }
              item.rightTo = Math.min(0, item.rightTo - 1);
              item.lastReset = item.data.length;
              item.loseFromReset = 0;
            } else if (d.kind === "distribution") {
              distribution++;
            }
            break;
          }
      }
    });
    data.forEach(v => v.rightTo += distribution);
    setData(
      Array.from(data.values()).sort((a, b) => (b.win * 100 + b.point) - (a.win * 100 + a.point))
    );
  }, [props.events]);
  return <Table
    bordered
    columns={DuelTableColumns}
    rowKey={data => data.name}
    dataSource={data}
    size="small"
    pagination={false}
    ></Table>
});

export default DuelTable;
