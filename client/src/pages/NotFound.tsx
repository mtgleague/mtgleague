import React, { Component } from 'react';
import { RouteChildrenProps, Link } from 'react-router-dom';

interface Props extends RouteChildrenProps {
}

class NotFound extends Component<Props, any> {

  render = () => {
    return <div>
      ページが見つかりません。<br />
      <Link to="/">トップに戻る</Link>
    </div>
  };
}

export default NotFound;
