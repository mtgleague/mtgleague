import React from 'react';
import { useHistory } from 'react-router-dom';
import {
  Button,
  notification,
  Typography,
  PageHeader,
  Dropdown,
  Menu,
  Modal,
  Input,
} from 'antd';
import {
  EllipsisOutlined,
  ExclamationCircleOutlined,
} from '@ant-design/icons';
import style from 'assets/League.module.css';
import API from 'api/API';
import { User } from 'model/User';
import CardSetTags from 'pages/deck/CardSetTags';

const { confirm } = Modal;

const PageTitle = (props: {
  me: User,
  title: string,
  contentId: number | null,
  isEdit: boolean,
  targetSets: string,
  onMenuEdit: () => void,
  onMenuDelete: (contentId: number) => void,
  onChange: (text: string) => void,
}) => {

  const history = useHistory();
  const handleMenuClick = (info: any) => {
    if (props.contentId === null) return;
    const contentId = props.contentId;
    const deleteOk = () => {
      API.DELETE(`/contents/${contentId}`, _ => {
        notification.success({ message: "削除しました" });
        props.onMenuDelete(contentId);
        history.push("/");
      })
    };

    switch (info.key) {
      case "edit":
        props.onMenuEdit();
        break;
      case "delete":
        confirm({
          title: "ページを削除",
          icon: <ExclamationCircleOutlined />,
          content: "削除したページ、履歴は復元することができません\n本当削除しますか？",
          okText: "削除",
          okType: "danger",
          cancelText: "キャンセル",
          onOk: deleteOk,
        });
        break;
    }
  }
  if (props.isEdit) {
    const handleEdit = (e: React.ChangeEvent<HTMLInputElement>) => props.onChange(e.target.value);
    return <Input
      value={props.title}
      onChange={handleEdit}
      className={style["title-edit"]}
      bordered={false}
      />
  }
  return <PageHeader
  title={<Typography.Title>{props.title}</Typography.Title>}
  tags={<CardSetTags value={props.targetSets} />}
  extra={[
    <Dropdown
      key="menu"
      disabled={props.me.isGuest()}
      overlay={<Menu onClick={handleMenuClick} className={style["page-menu"]}>
        <Menu.Item key="edit">編集</Menu.Item>
        <Menu.Divider />
        <Menu.Item key="delete" danger={true}>削除</Menu.Item>
      </Menu>}
      placement="bottomRight"
      trigger={[ "click" ]}
      >
      <Button
        style={{
          border: 'none',
          padding: 0,
          backgroundColor: "transparent",
        }}
      >
        <EllipsisOutlined
          style={{
            fontSize: 24,
            verticalAlign: 'top',
          }}
        />
      </Button>
    </Dropdown>
  ]} />
};

export default PageTitle;
