@echo off

set backupfile=backup.bin
docker-compose exec db pg_dump -Fc -f %backupfile% -U postgres postgres
docker cp go-postgres:/%backupfile% .
