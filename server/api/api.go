package api

import (
	"fmt"
	"os"
	"time"

	"example.com/m/common"
	"example.com/m/model"
	jwt "github.com/appleboy/gin-jwt"
	"github.com/gin-gonic/gin"
	"github.com/go-gorp/gorp"
	"github.com/line/line-bot-sdk-go/linebot"
	"golang.org/x/crypto/bcrypt"
)

// RegisterAPI はAPIの登録をします
func RegisterAPI(comp *common.Component) {
	dbmap := &gorp.DbMap{Db: comp.DB, Dialect: gorp.PostgresDialect{}}

	key := "mtgleague" + os.Getenv("JWT_SECRET")
	authMiddleware := &jwt.GinJWTMiddleware{
		Realm:       "mtgleague",
		Key:         []byte(key),
		Timeout:     time.Hour * 24 * 30,
		SendCookie:  true,
		TokenLookup: "header:Authorization,cookie:JWTToken",
		Authenticator: func(c *gin.Context) (interface{}, error) {
			var user model.User
			if err := c.ShouldBindJSON(&user); err != nil {
				return "", jwt.ErrMissingLoginValues
			}

			password, err := dbmap.SelectStr("SELECT password FROM users WHERE name=$1", user.Name)
			if err != nil {
				return nil, jwt.ErrFailedAuthentication
			}
			if password == "" {
				// ダミー
				password = "$2a$10$abcdefghijklmnabcdefghijklmnabcdefghijklmnabcdefghijk"
			}
			err = bcrypt.CompareHashAndPassword([]byte(password), []byte(user.Password))
			if err != nil {
				return nil, jwt.ErrFailedAuthentication
			}
			return &user, nil
		},
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if v, ok := data.(*model.User); ok {
				return jwt.MapClaims{
					"id": v.Name,
				}
			}
			return jwt.MapClaims{}
		},
	}

	channelSecret := os.Getenv("LINE_CHANNEL_SECRET")
	channelAccessToken := os.Getenv("LINE_CHANNEL_ACCESS_TOKEN")
	bot, err := linebot.New(channelSecret, channelAccessToken)
	if err != nil {
		panic(fmt.Sprintf("cannot create linebot: %v\n", err))
	}

	apiGroup := comp.Router.Group("/api")
	apiAuth := comp.Router.Group("/api")
	apiAuth.Use(authMiddleware.MiddlewareFunc())

	dlGroup := comp.Router.Group("/download")
	dlGroup.Use(authMiddleware.MiddlewareFunc())

	context := Context{
		API:            apiGroup,
		APIAuth:        apiAuth,
		Download:       dlGroup,
		DBmap:          dbmap,
		AuthMiddleware: authMiddleware,
		Component:      comp,
		LineBot:        bot,
	}

	RegisterGlobalAPI(&context)
	RegisterUsersAPI(&context)
	RegisterContentsAPI(&context)
	RegisterEventsAPI(&context)
	RegisterCardsAPI(&context)
	RegisterDecksAPI(&context)
	RegisterLineAPI(&context)
	RegisterDownloadAPI(&context)
	RegisterPageAPI(&context)
}
