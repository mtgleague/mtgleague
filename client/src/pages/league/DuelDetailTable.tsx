import React from 'react';
import { EventData, DuelDetail } from 'model/Event';
import { Table, Collapse } from 'antd';
import style from 'assets/League.module.css';
import { SameWeek } from './util';
import { User } from 'model/User';
import moment from 'moment';

const { Panel } = Collapse;

interface DuelData {
  user: string,
  isHighlight: boolean,
  data: { date: moment.Moment, opponent: string, value: boolean }[],
  point: number,
}

const CreateDuelData: (user: string, me: string)=>DuelData = (user, me) => ({
  user,
  isHighlight: user === me,
  data: [],
  point: 0,
});

const DuelDetailTable = React.memo((props: { me: User, events: EventData[] }) => {

  const colums: any[] = [];

  const data = new Map<string, DuelData>();
  props.events.filter(e => e.active && e.type === "duel").forEach(e => {
    const d = e.detail as DuelDetail;
    const item1 = data.get(d.user1) || CreateDuelData(d.user1, props.me.name);
    const item2 = data.get(d.user2) || CreateDuelData(d.user2, props.me.name);
    !data.has(d.user1) && data.set(d.user1, item1);
    !data.has(d.user2) && data.set(d.user2, item2);

    item1.data.push({
      date: e.date,
      opponent: d.user2,
      value: d.result === 0,
    });
    item2.data.push({
      date: e.date,
      opponent: d.user1,
      value: d.result === 1,
    });
    // ソートにしか使わないのでここで勝ちに重みをつける
    item1.point += d.result ? -1 : 101;
    item2.point += d.result ? 101 : -1;
  });

  const sortedData = Array.from(data.values()).sort((a, b) => b.point - a.point);
  colums.push({
    dataIndex: "user",
    key: "user",
    fixed: "left",
  });
  for (const d of sortedData) {
    colums.push({
      title: d.user,
      dataIndex: "data",
      key: d.user,
      align: "center",
      render: (data: any[]) => {
        const targets = data.filter(x => x.opponent === d.user);
        // 対戦なし
        if (targets.length === 0) return <div data-label={d.user}>-</div>;
        const now = moment();
        const thisWeekTargets = targets.filter(x => SameWeek(x.date, now));
        return <div data-label={d.user} className={thisWeekTargets.length >= 3 ? style["thisweek-max"] : undefined}>
        <span className={style["thisweek-detail"]}>
          {thisWeekTargets.filter(x => x.value).length} - {thisWeekTargets.filter(x => !x.value).length}
        </span>
        <br />
        <span>
          ({targets.filter(x => x.value).length} - {targets.filter(x => !x.value).length})
        </span>
        </div>;
      },
    });
  }

  return <Collapse>
    <Panel header="詳細を表示" key="1">
      <Table
        bordered
        columns={colums}
        dataSource={sortedData}
        size="small"
        pagination={false}
        tableLayout="fixed"
        className="duel-detail-table"
        rowClassName={(record, _) => record.isHighlight ? style["my-detail-row"] : ""}
        >
      </Table>
    </Panel>
  </Collapse>
});

export default DuelDetailTable;
