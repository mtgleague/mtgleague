
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE events ADD active boolean;
UPDATE events SET active = TRUE;
ALTER TABLE events ALTER active SET NOT NULL;
ALTER TABLE events ALTER active SET DEFAULT TRUE;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE events DROP COLUMN active;
