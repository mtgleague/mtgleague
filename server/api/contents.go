package api

import (
	"net/http"
	"strconv"
	"strings"
	"time"

	"example.com/m/model"
	"github.com/gin-gonic/gin"
)

func fileAPI(context *Context) {

	context.APIAuth.GET("/files/:id", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		id := helper.getID()

		var file model.File
		helper.selectOne(&file, "SELECT * FROM files WHERE id=$1", id)
		if helper.err != nil {
			return
		}

		c.Header("Content-Disposition", "inline; filename=\""+file.Name+"\"")
		c.Data(http.StatusOK, file.MIMEType, file.Data)
	})
	context.APIAuth.GET("/files/:id/info", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		id := helper.getID()

		var file model.File
		helper.selectOne(&file, "SELECT * FROM files WHERE id=$1", id)
		if helper.err != nil {
			return
		}

		c.JSON(http.StatusOK, file)
	})
	context.APIAuth.DELETE("/files/:id", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		id := helper.getID()

		helper.verifyUser()
		if helper.err != nil {
			return
		}

		var file model.File
		helper.begin()
		helper.selectOne(&file, "SELECT * FROM files WHERE id=$1", id)
		helper.delete(&file)
		helper.commit()
		if helper.err != nil {
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"status": "OK",
		})
	})

	context.APIAuth.GET("/contents/:id/files", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		id := helper.getID()

		var content model.Content
		var fileData []model.File
		helper.begin()
		helper.selectOne(&content, "SELECT * FROM contents WHERE id=$1", id)
		helper.selectAny(&fileData, "SELECT * FROM files WHERE cid=$1", id)
		helper.commit()
		if helper.err != nil {
			return
		}
		c.JSON(http.StatusOK, fileData)
	})

	context.APIAuth.POST("/contents/:id/files", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		id := helper.getID()

		helper.verifyUser()
		if helper.err != nil {
			return
		}

		fh, err := c.FormFile("file")
		if err != nil {
			setBadRequestResult(c, "invalid file.")
			return
		}

		fileData := model.FileNew{
			Name:      fh.Filename,
			ContentID: id,
		}

		{
			file, err := fh.Open()
			if err != nil {
				setServerError(c, err)
				return
			}
			buff := make([]byte, fh.Size)
			_, err = file.Read(buff)
			if err != nil {
				setServerError(c, err)
				file.Close()
				return
			}
			fileData.Data = buff
			fileData.MIMEType = http.DetectContentType(buff)
			err = file.Close()
			if err != nil {
				setServerError(c, err)
				return
			}
		}
		var content model.Content
		helper.begin()
		helper.selectOne(&content, "SELECT * FROM contents WHERE id=$1", id)
		helper.insert(&fileData)
		FileID := helper.getLastValue()
		helper.commit()
		if helper.err != nil {
			return
		}
		result := model.File{
			ID:      FileID,
			FileNew: fileData,
		}
		c.JSON(http.StatusOK, result)
	})

}

func commentAPI(context *Context) {

	context.APIAuth.GET("/comments/:id", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		id := helper.getID()

		var comment model.Comment
		helper.selectOne(&comment, "SELECT * FROM comments WHERE id=$1", id)
		if helper.err != nil {
			return
		}

		c.JSON(http.StatusOK, comment)
	})

	context.APIAuth.GET("/contents/:id/comments", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		id := helper.getID()

		var content model.Content
		var commentData []model.Comment
		helper.begin()
		helper.selectOne(&content, "SELECT * FROM contents WHERE id=$1", id)
		helper.selectAny(&commentData, "SELECT * FROM comments WHERE cid=$1 ORDER BY date", id)
		helper.commit()
		if helper.err != nil {
			return
		}
		c.JSON(http.StatusOK, commentData)
	})

	context.APIAuth.POST("/contents/:id/comments", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}

		helper.verifyUser()
		if helper.err != nil {
			return
		}
		cid := helper.getID()

		var comment model.CommentNew
		err := c.ShouldBindJSON(&comment)
		if err != nil {
			setBadRequestResult(c, err.Error())
			return
		}
		comment.ContentID = cid
		comment.Author = helper.getMyName()
		comment.Date.Time = time.Now()
		comment.Parent = nil

		var content model.Content
		helper.begin()
		helper.selectOne(&content, "SELECT * FROM contents WHERE id=$1", cid)
		helper.insert(&comment)
		id := helper.getLastValue()
		helper.commit()
		if helper.err != nil {
			return
		}
		result := model.Comment{
			ID:         id,
			CommentNew: comment,
		}
		c.JSON(http.StatusOK, result)
	})

	context.APIAuth.PUT("/comments/:id", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		id := helper.getID()

		var inputCommentData model.Comment
		err := c.ShouldBindJSON(&inputCommentData)
		if err != nil {
			setBadRequestResult(c, err.Error())
			return
		}

		var commentData model.Comment
		helper.begin()
		helper.selectOne(&commentData, "SELECT * FROM comments WHERE id=$1", id)
		helper.verifyPerson(commentData.Author)
		commentData.Text = inputCommentData.Text
		commentData.Date.Time = time.Now()
		commentData.Edit = true
		helper.update(&commentData)
		helper.commit()
		if helper.err != nil {
			return
		}
		c.JSON(http.StatusOK, commentData)
	})

	context.APIAuth.DELETE("/comments/:id", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		id := helper.getID()

		var commentData model.Comment
		helper.begin()
		helper.selectOne(&commentData, "SELECT * FROM comments WHERE id=$1", id)
		helper.verifyPerson(commentData.Author)
		helper.delete(&commentData)
		helper.commit()
		if helper.err != nil {
			return
		}
		c.JSON(http.StatusOK, commentData)
	})

}

// RegisterContentsAPI は`/contents`以下のAPIを登録します
func RegisterContentsAPI(context *Context) {
	context.DBmap.AddTableWithName(model.Content{}, "contents")
	context.DBmap.AddTableWithName(model.ContentNew{}, "contents")
	context.DBmap.AddTableWithName(model.File{}, "files")
	context.DBmap.AddTableWithName(model.FileNew{}, "files")
	context.DBmap.AddTableWithName(model.Comment{}, "comments")
	context.DBmap.AddTableWithName(model.CommentNew{}, "comments")

	context.APIAuth.GET("/contents", func(c *gin.Context) {
		var result []model.Content
		_, err := context.DBmap.Select(&result, "SELECT * FROM contents")
		if err != nil {
			setServerError(c, err)
			return
		}
		c.JSON(http.StatusOK, result)
	})
	context.APIAuth.GET("/contents/:id", func(c *gin.Context) {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			setBadRequestResult(c, "invalid id.")
			return
		}
		var result model.Content
		err = context.DBmap.SelectOne(&result, "SELECT * FROM contents WHERE id=$1", id)
		if err != nil {
			setServerError(c, err)
			return
		}
		c.JSON(http.StatusOK, result)
	})
	context.APIAuth.POST("/contents", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}

		helper.verifyUser()
		if helper.err != nil {
			return
		}

		var content model.ContentNew
		err := c.BindJSON(&content)
		if err != nil {
			setBadRequestResult(c, err.Error())
			return
		}
		if len(content.Title) == 0 {
			setBadRequestResult(c, "title is empty.")
			return
		}
		helper.begin()
		helper.insert(&content)
		id := helper.getLastValue()
		helper.commit()
		if helper.err != nil {
			return
		}

		c.JSON(http.StatusOK, model.Content{
			ID:         id,
			ContentNew: content,
		})
	})
	context.APIAuth.PUT("/contents/:id", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}

		helper.verifyUser()
		if helper.err != nil {
			return
		}
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			setBadRequestResult(c, "invalid id.")
			return
		}
		var content model.Content
		err = c.ShouldBindJSON(&content)
		if err != nil {
			setBadRequestResult(c, err.Error())
			return
		}
		if len(content.Title) == 0 {
			setBadRequestResult(c, "title is empty.")
			return
		}
		content.ID = id
		_, err = context.DBmap.Update(&content)
		if err != nil {
			setServerError(c, err)
			return
		}
		c.JSON(http.StatusOK, content)
	})
	context.APIAuth.DELETE("/contents/:id", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}

		helper.verifyUser()
		if helper.err != nil {
			return
		}
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			setBadRequestResult(c, "invalid id.")
			return
		}
		helper.begin()
		var content model.Content
		helper.selectOne(&content, "SELECT * FROM contents WHERE id=$1", id)
		var event []model.Event
		helper.selectAny(&event, "SELECT * FROM events WHERE cid=$1", id)
		var duelQuery []string
		var systemQuery []string
		for _, e := range event {
			switch e.Type {
			case "duel":
				duelQuery = append(duelQuery, "id="+strconv.Itoa(e.EventID))
				break
			case "system":
				systemQuery = append(systemQuery, "id="+strconv.Itoa(e.EventID))
				break
			}
		}
		if len(duelQuery) > 0 {
			helper.exec("DELETE FROM duel_events WHERE " + strings.Join(duelQuery, " OR "))
		}
		if len(systemQuery) > 0 {
			helper.exec("DELETE FROM system_events WHERE " + strings.Join(systemQuery, " OR "))
		}
		helper.exec("DELETE FROM events WHERE cid=$1", id)
		helper.exec("DELETE FROM files WHERE cid=$1", id)
		helper.exec("DELETE FROM comments WHERE cid=$1", id)
		helper.exec("DELETE FROM contents WHERE id=$1", id)
		helper.commit()
		if helper.err != nil {
			return
		}
		c.JSON(http.StatusOK, content)
	})

	fileAPI(context)
	commentAPI(context)
}
