package api

import (
	"net/http"
	"strings"
	"time"

	"example.com/m/model"
	"github.com/gin-gonic/gin"
)

// RegisterGlobalAPI は全体に関わるAPIを登録します
func RegisterGlobalAPI(context *Context) {
	context.DBmap.AddTableWithName(model.GlobalData{}, "global_data")

	context.APIAuth.GET("/places", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		var result model.Places
		var values []model.GlobalData

		helper.selectAny(&values, "SELECT * FROM global_data WHERE key='place_active' or key='place_list'")
		if helper.err != nil {
			return
		}

		for _, v := range values {
			switch v.Key {
			case "place_active":
				result.Active = v.Value
				result.UpdatedAt = v.Date
				break
			case "place_list":
				result.PlaceList = strings.Split(v.Value, ",")
				break
			}
		}
		c.JSON(http.StatusOK, result)
	})

	context.APIAuth.GET("/places/active", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}

		var v model.GlobalData

		helper.selectOne(&v, "SELECT * FROM global_data WHERE key='place_active'")
		if helper.err != nil {
			return
		}

		result := model.PlaceActive{
			Active:    v.Value,
			UpdatedAt: v.Date,
		}
		c.JSON(http.StatusOK, result)
	})

	context.APIAuth.POST("/places/active", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
			comp:    context.Component,
		}
		var input model.PlaceActive

		err := c.ShouldBindJSON(&input)
		if err != nil {
			setBadRequestResult(c, "")
			return
		}
		input.UpdatedAt.Time = time.Now()

		value := model.GlobalData{
			Key:   "place_active",
			Date:  input.UpdatedAt,
			Value: input.Active,
		}
		helper.exec(
			"UPDATE global_data SET date=$1, value=$2 WHERE key='place_active'",
			value.Date,
			value.Value,
		)
		if helper.err != nil {
			return
		}
		helper.broadcast("change_place", input)
		c.JSON(http.StatusOK, input)
	})

	context.APIAuth.GET("/places/list", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}

		var v model.GlobalData
		helper.selectOne(&v, "SELECT * FROM global_data WHERE key='place_list'")
		if helper.err != nil {
			return
		}
		result := strings.Split(v.Value, ",")

		c.JSON(http.StatusOK, result)
	})

	context.APIAuth.POST("/places/list", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		var input model.PlaceList

		err := c.ShouldBindJSON(&input)
		if err != nil {
			setBadRequestResult(c, "")
			return
		}

		value := model.GlobalData{
			Key: "place_list",
			Date: model.LocalDateTime{
				Time: time.Now(),
			},
			Value: strings.Join(input, ","),
		}
		helper.insert(&value)
		if helper.err != nil {
			return
		}

		c.JSON(http.StatusOK, input)
	})
}
