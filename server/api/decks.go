package api

import (
	"net/http"
	"strconv"
	"time"

	"example.com/m/model"
	"github.com/gin-gonic/gin"
)

// RegisterDecksAPI は`/decks`以下のAPIを登録します
func RegisterDecksAPI(context *Context) {
	context.DBmap.AddTableWithName(model.DeckNew{}, "decks")
	context.DBmap.AddTableWithName(model.Deck{}, "decks").SetKeys(true, "id")

	context.APIAuth.GET("/decks", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}

		helper.verifyUser()
		var decks []model.Deck
		if helper.isRoleAdmin() {
			helper.selectAny(&decks, "SELECT * FROM decks ORDER BY created_at DESC")
		} else {
			helper.selectAny(
				&decks,
				"SELECT * FROM decks WHERE author=$1 OR access_level='public' ORDER BY created_at DESC",
				helper.getMyName(),
			)
		}
		if helper.err != nil {
			return
		}
		c.JSON(http.StatusOK, decks)
	})
	context.APIAuth.GET("/decks/:key", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		helper.verifyUser()
		key := helper.getParamStr("key")
		id, err := strconv.Atoi(key)
		var deck model.Deck
		if err != nil {
			helper.selectOne(&deck, "SELECT * FROM decks WHERE key=$1", key)
		} else {
			helper.selectOne(&deck, "SELECT * FROM decks WHERE id=$1", id)
		}
		if helper.err != nil {
			return
		}
		if !helper.isRoleAdmin() && helper.getMyName() != deck.Author {
			canAccess := true
			if key == deck.Key {
				if deck.AccessLevel == "private" {
					canAccess = false
				}
			} else {
				if deck.AccessLevel != "public" {
					canAccess = false
				}
			}
			if !canAccess {
				helper.setForbidden("アクセス権がありません")
				return
			}
		}
		c.JSON(http.StatusOK, deck)
	})
	context.APIAuth.POST("/decks", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}

		helper.verifyUser()
		var deck model.DeckNew
		err := c.ShouldBindJSON(&deck)
		if err != nil {
			setBadRequestResult(c, err.Error())
			return
		}
		deck.Author = helper.getMyName()
		deck.CreatedAt = model.LocalDateTime{
			Time: time.Now(),
		}
		deck.UpdatedAt = deck.CreatedAt

		helper.begin()
		helper.insert(&deck)
		id := helper.getLastValue()
		key := helper.selectStr("SELECT key FROM decks WHERE id=$1", id)
		helper.commit()
		if helper.err != nil {
			return
		}
		c.JSON(http.StatusOK, model.Deck{
			DeckNew:   deck,
			ID:        id,
			Key:       key,
			Cards:     model.CardList{},
			SideCards: model.CardList{},
		})
	})
	context.APIAuth.PUT("/decks/:id", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}

		helper.verifyUser()
		id := helper.getID()

		var deckEdit model.DeckEdit
		err := c.ShouldBindJSON(&deckEdit)
		if err != nil {
			setBadRequestResult(c, err.Error())
			return
		}

		helper.begin()
		var deck model.Deck
		helper.selectOne(&deck, "SELECT * FROM decks WHERE id=$1", id)
		helper.verifyPerson(deck.Author)
		deck.UpdatedAt.Time = time.Now()
		deck.Name = deckEdit.Name
		deck.Note = deckEdit.Note
		deck.Cards = deckEdit.Cards
		deck.SideCards = deckEdit.SideCards
		deck.UseSets = deckEdit.UseSets
		deck.AccessLevel = deckEdit.AccessLevel
		helper.update(&deck)
		helper.commit()
		if helper.err != nil {
			return
		}
		c.JSON(http.StatusOK, deck)
	})
	context.APIAuth.DELETE("/decks/:id", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}

		id := helper.getID()

		helper.begin()
		var deck model.Deck
		helper.selectOne(&deck, "SELECT * FROM decks WHERE id=$1", id)
		helper.verifyPerson(deck.Author)
		helper.delete(&deck)
		helper.commit()
		if helper.err != nil {
			return
		}
		c.JSON(http.StatusOK, gin.H{
			"status": "OK",
		})
	})

}
