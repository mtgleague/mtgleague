const cheerio = require("cheerio");
const fs = require("fs");
const request = require('request');

function requestAsync(url, opt = {}) {
    return new Promise((resolve, reject) => {
        request(url, opt, (err, res, body) => {
            if (!err && res.statusCode == 200) {
                resolve(body);
            } else {
                reject(err);
            }
        });
    });
}
const sleep = msec => new Promise(resolve => setTimeout(resolve, msec));
async function requestPage(url) {
    const body = await requestAsync(url);
    await sleep(1000);
    return cheerio.load(body);
}

async function getCard(url) {
    console.log(`request ${url}...`);
    const $ = await requestPage(url);
    let results = [];

    $(".card-detail").each((i, e) => {
        let result = {};
        const $e = $(e);
    
        const CostRegex = /\/img_sys\/cardImages\/common\/([^\/]+).png/;
    
        const cardPath = $e.find(".card-img img").attr("src");
        const path = /cardImages\/(.+)\/(.+)\/cardimage.png/.exec(cardPath);
        const cardName = `${path[1]}_${path[2]}`;
    
        request(cardPath, {encoding: null}, (err, res, body) => {
            fs.writeFileSync(
                `./download/${cardName}.png`,
                body,
                'binary');
        });
    
        while (true) {
            const $img = $e.find(".card-info img");
            if ($img.length == 0) break;
            $img.eq(0).replaceWith(`(${CostRegex.exec($img.eq(0).attr("src"))[1]})`);
        }
        const convertLandName = t => t
            .replace("《Plains》", "《平地》")
            .replace("《Island》", "《島》")
            .replace("《Swamp》", "《沼》")
            .replace("《Mountain》", "《山》")
            .replace("《Forest》", "《森》");
        const convertLandType = t => t
            .replace("Basic Land — Plains", "基本土地 ― - 平地")
            .replace("Basic Land — Island", "基本土地 ― - 島")
            .replace("Basic Land — Swamp", "基本土地 ― - 沼")
            .replace("Basic Land — Mountain", "基本土地 ― - 山")
            .replace("Basic Land — Forest", "基本土地 ― - 森");
        result.title = convertLandName($e.find("h2").text());
        result.cardName = cardName;
        result.power = $e.find(".power-toughness").text();
        result.cost = Array.from($e.find(".icon-list li").map((i, e) => $(e).text()));
        result.type = convertLandType($e.find(".type").text());
        let $text = $e.find(".text");
    
        result.textJP = $text.eq(0).text();
        const hasFlavor = $text.length > 2 || result.textJP.length == 0;
        result.flavorText = hasFlavor ? $text.eq(1).text() : "";
        result.textEN = result.textJP.length > 0 ? $text.eq($text.length - 1).text() : "";

        results.push(result);
    })    
    fs.writeFileSync(`./download/${results[0].cardName}.json`, JSON.stringify(results));
    console.log(`complete ${results[0].title}.`);
}

async function main() {
    const $ = await requestPage("https://mtg-jp.com/products/card-gallery/0000278/");
    const list = Array.from($(".card-list a"))
        .filter(e => !$(e).attr("href").match(/^javascript/))
        .filter(e => {
            const m = $(e).find("img").data("src").match(/cardImages\/(.+)\/(.+)\/cardimage.png/);
            if (m) {
                const cardName = `${m[1]}_${m[2]}`;
                return !(fs.existsSync(`./download/${cardName}.png`) &&
                    fs.existsSync(`./download/${cardName}.json`));
            }
            return false;
        })
        .map(e => "https://mtg-jp.com" + $(e).attr("href"));
    let i = 1;
    for (let item of list) {
        console.log(`${i++}/${list.length}`);
        await getCard(item);
    }
    console.log("done.");
}

main();

//(async () => {
//    await getCard("https://mtg-jp.com/products/card-gallery/0000184/472967/");
//})();
