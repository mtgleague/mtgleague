import React, { useEffect, useState } from 'react';
import { Link, RouteChildrenProps, useHistory } from 'react-router-dom';
import {
  Typography,
  PageHeader,
  Table
} from 'antd';
import API from 'api/API';
import { AccessLevel, Deck } from 'model/Deck';
import { User } from 'model/User';
import AddButton from './AddButton';
import CardSetTags from './CardSetTags';
import AccessLevelIcon from './AccessLevelIcon';
import { CardSet } from 'model/Card';
import Enumerable from 'linq';
import { ColumnsType } from 'antd/lib/table';

const PageTitle = (props: {
  title: string,
  onCreate: (name: string) => void,
}) => {
  const extra: React.ReactNode[] = [
    <AddButton key={1} onCreate={props.onCreate} />,
  ];

  return <PageHeader
    title={<Typography.Title>{props.title}</Typography.Title>}
    extra={extra}
    />
};

interface Props extends RouteChildrenProps<{ id: string }> {
  me: User;
  isEdit?: boolean;
}

const Decklist = (props: Props) => {

  const [ decklist, setDecklist ] = useState<Deck[]>([]);
  const [ cardSets, setCardSets ] = useState<CardSet[]>([]);
  const history = useHistory();

  useEffect(() => {
    API.GET("/card_sets", (cardSets: CardSet[]) => {
      setCardSets(cardSets);
    });
    API.GET("/decks", (decklist: Deck[]) => {
      setDecklist(decklist);
    });
  }, []);

  const handleNewDeck = (name: string) => {
    API.POST("/decks", {
      name
    }, deck => {
      setDecklist(oldDecklist => {
        const decklist = oldDecklist.slice();
        decklist.push(deck);
        return decklist;
      })
      history.push("/deck/" + deck.key);
    });
  };

  const columns: ColumnsType<Deck> = [
    {
      title: 'デッキ名',
      dataIndex: 'name',
      key: 'id',
      render: (text: string, record: Deck) =>
          <Link to={`/deck/${record.key}`}>{text}</Link>,
    },
    {
      title: '',
      dataIndex: 'access_level',
      key: 'id',
      render: (v: AccessLevel) => v !== 'public' ? <AccessLevelIcon showTooltip level={v} /> : "",
    },
    {
      title: '作成者',
      dataIndex: 'author',
      key: 'id',
      filters: Enumerable.from(decklist)
        .select(d => d.author)
        .distinct()
        .select(author => ({
          text: author,
          value: author,
        }))
        .toArray(),
      onFilter: (value, record) => record.author === value,
    },
    {
      title: 'カードセット',
      dataIndex: 'use_sets',
      key: 'id',
      render: (text: string) => <CardSetTags cardSets={cardSets} value={text} />,
      filters: Enumerable.from(decklist)
        .selectMany(d => d.use_sets?.split(",") || [])
        .distinct()
        .select(set => ({
          text: `[${set}]${cardSets.find(s => s.code === set)?.name}`,
          value: set,
        }))
        .toArray(),
      onFilter: (value, record) => record.use_sets?.split(",").includes(value as string),
    },
    {
      title: '',
      dataIndex: '',
      key: 'id',
      render: (_: string, record: Deck) => <div>
        {props.me.name === record.author &&
          <Link to={`/deck/${record.key}/edit`}>Edit</Link>
        }
      </div>
    },
  ];
  return <>
    <PageTitle
      title={"デッキリスト"}
      onCreate={handleNewDeck}
      />
    <Table
      dataSource={decklist}
      columns={columns}
      rowKey="id"
      />
  </>;
}

export default Decklist;
