import { Card } from 'model/Card';
import BitFlags from 'util/BitFlags';

export type Action = (card: Card) => void;
export type Actions = { [key: string]: Action };

export interface CardData
{
  card: Card;
  count: number;
}

export enum SortKind
{
  CardNo,
  Type,
  Cost,
}

export enum ColorKind
{
  White,
  Blue,
  Black,
  Red,
  Green,
  Colorless,

  Term,
}

export enum TypeKind
{
  Creature,
  Enchantment,
  Artifact,
  Instant,
  Sorcery,
  Planeswalker,
  Battle,
  Land,

  Term,
}

export enum RarityKind
{
  Land,
  Common,
  Uncommon,
  Rare,
  MythicRare,

  Term,
}

export interface CardFilter
{
  sort: SortKind;
  color: BitFlags<ColorKind>;
  type: BitFlags<TypeKind>;
  rarity: BitFlags<RarityKind>;
}

export type ColorSymbol = ("W"|"U"|"B"|"R"|"G");
export interface CostDetail
{
  value: number;
  color: ColorSymbol[];
}

export const SortKindName = [
  "カード番号",
  "種類",
  "コスト",
];

export const TypeKindName = [
  "クリーチャー",
  "エンチャント",
  "アーティファクト",
  "インスタント",
  "ソーサリー",
  "プレインズウォーカー",
  "バトル",
  "土地",
];

export const RarityKindName = [
  "基本土地",
  "コモン",
  "アンコモン",
  "レア",
  "神話レア",
];

export function cardTypeToOrder(type: string) {
  if (type.match("クリーチャー")) return 0;
  if (type.match(/アーティファクト|エンチャント/)) return 1;
  if (type.match(/プレインズウォーカー/)) return 2;
  if (type.match(/バトル/)) return 3;
  if (type.match(/ソーサリー|インスタント/)) return 4;
  if (type.match(/土地/)) return 10;
  return 100;
}

export function costToOrder(cost: string, type: string) {
  let value = 0;
  if (cost) {
    cost.split(",").forEach(c => {
      const m = c.match(/\((\d+)\)/);
      if (m) {
        value += Number(m[1]);
      } else if (c !== "(X)") {
        value++;
      }
    });
  } else if (type.match(/土地/)) {
    value = 100;
  }
  return value;
}

export function parseCost(cost: string) {
  const result: CostDetail = {
    value: 0,
    color: [],
  };
  if (cost) {
    cost.split(",").forEach(c => {
      const m = c.match(/\((.+)\)/);
      if (m) {
        const v = m[1];
        if (v.match(/^\d+$/)) {
          result.value += Number(v);
        } else if (v === "X") {
        } else {
          result.value++;
          if (v.includes("W")) result.color.push("W");
          if (v.includes("U")) result.color.push("U");
          if (v.includes("B")) result.color.push("B");
          if (v.includes("R")) result.color.push("R");
          if (v.includes("G")) result.color.push("G");
        }
      }
    });
  }
  return result;
}
