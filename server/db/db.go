package db

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"time"

	"bitbucket.org/liamstask/goose/lib/goose"
	"github.com/go-gorp/gorp"
)

func ConnectToDB() *sql.DB {
	dburl := os.Getenv("DATABASE_URL")
	db, err := sql.Open("postgres", dburl)
	if err != nil {
		log.Fatal(err)
	}
	// 接続待ち
	for {
		err := db.Ping()
		if err != nil {
			log.Println(err)
			time.Sleep(10)
			continue
		}
		break
	}
	return db
}

func Migration(db *sql.DB) {
	conf := &goose.DBConf{
		MigrationsDir: filepath.Join("db", "migrations"),
		Driver: goose.DBDriver{
			Name:    "postgres",
			OpenStr: ":memory:",
			Import:  "github.com/lib/pq",
			Dialect: &goose.PostgresDialect{},
		},
		PgSchema: "",
	}

	target, err := goose.GetMostRecentDBVersion(conf.MigrationsDir)
	if err != nil {
		panic(fmt.Sprintf("cannot get recent db version with goose: %v\n", err))
	}

	// カードをDBからファイルに出す
	{
		current, err := goose.EnsureDBVersion(conf, db)
		if err != nil {
			panic(fmt.Sprintf("cannot get current db version with goose: %v\n", err))
		}
		if current < 20230128164340 {
			if _, err := os.Stat("./image/cards"); err != nil {
				panic("not found cards dir")
			}
			dbmap := &gorp.DbMap{Db: db, Dialect: gorp.PostgresDialect{}}
			type CardImageInfo struct {
				Key   string `db:"key"`
				Set   string `db:"set"`
				Image []byte `db:"image"`
			}
			var cards []CardImageInfo
			dbmap.Select(&cards, "SELECT key, set, image FROM cards")
			for _, card := range cards {
				setPath := "./image/cards/" + card.Set
				path := setPath + "/" + card.Key + ".png"
				if _, err := os.Stat(setPath); err != nil {
					fmt.Println("create dir " + setPath)
					os.Mkdir(setPath, 0775)
				}
				if _, err := os.Stat(path); err != nil {
					fmt.Println("save " + path)
					ioutil.WriteFile(path, card.Image, 0664)
				}
			}
		}
	}

	err = goose.RunMigrationsOnDb(conf, conf.MigrationsDir, target, db)
	if err != nil {
		panic(fmt.Sprintf("cannot run migration with goose: %v\n", err))
	}
}
