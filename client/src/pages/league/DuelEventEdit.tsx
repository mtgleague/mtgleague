import React, { Component, useCallback, useEffect, useState } from 'react';
import { Button, AutoComplete, Radio, DatePicker, Tabs, Select, Popconfirm, Input, } from 'antd';
import { EventData, SystemDetail, DuelDetail, SystemEventKind } from 'model/Event';
import { User } from 'model/User';
import { RadioChangeEvent } from 'antd/lib/radio';
import moment from 'moment';
import API from 'api/API';
import { Card } from 'model/Card';
import Enumerable from 'linq';
import { isMobile } from 'react-device-detect';

interface DefaultProps {
  data: EventData;
  mode: "edit" | "add";
}

interface Props extends DefaultProps {
  me?: User;
  contentId?: number;
  users: User[];
  cards?: Card[];
  onAdd?: (data: EventData)=>void;
  onEdit?: (data: EventData)=>void;
  onDelete?: (data: EventData)=>void;
  onCancel?: ()=>void;
}

interface State {
  data: EventData;
}

interface DuelState {
  date: moment.Moment,
  contentId: number,
  user1: string,
  user2: string,
  result: 0 | 1,
  text: string,
}
interface SystemState {
  date: moment.Moment,
  contentId: number,
  username: string,
  kind: SystemEventKind,
}

interface UserEditProps {
  users: User[];
  value: string;
  onChange: (value: string)=>void;
}

class UserEdit extends Component<UserEditProps, any> {

  constructor(props: UserEditProps) {
    super(props);
    this.state = {
      value: props.value,
      options: [],
    }
    this.handleUserSearch = this.handleUserSearch.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidUpdate(prevProps: UserEditProps) {
    const newState: any = {};
    let changeState = false;
    if (JSON.stringify(prevProps.users) !== JSON.stringify(this.props.users)) {
      newState.options = this.props.users.map((u: User) => ({ value: u.name }));
      changeState = true;
    }
    if (prevProps.value !== this.props.value) {
      newState.value = this.props.value;
      changeState = true;
    }
    if (changeState) {
      this.setState(newState);
    }
  }

  handleUserSearch(text: string) {
    this.setState({
      options: this.props.users
        .filter(u => u.name.includes(text))
        .map(u => ({ value: u.name })),
    });
  }

  handleChange(value: string) {
    this.setState({ value });
    this.props.onChange(value);
  }

  render() {
    return <AutoComplete
      value={this.state.value}
        options={this.state.options}
        style={{ width: 100 }}
        onChange={this.handleChange}
        onSearch={this.handleUserSearch}
        placeholder="username"
      />
  }
}

class ResultEdit extends Component<{ value: 0 | 1, onChange: (value: 0 | 1)=>void }, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      value: props.value,
    }
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidUpdate(prevProps: any) {
    if (this.props.value !== prevProps.value) {
      this.setState({ value: this.props.value });
    }
  }

  handleChange(e: RadioChangeEvent) {
    const value = e.target.value;
    this.setState({ value });
    this.props.onChange(value);
  }

  render() {
    return <Radio.Group
      onChange={this.handleChange}
      value={this.state.value}
      optionType="button"
      options={[
        { label: 'o - x', value: 0 },
        { label: 'x - o', value: 1 },
      ]}
      >
    </Radio.Group>
  }
}

const TextEdit = (props: {
  cards: Card[],
  value: string,
  onChange: (value: string)=>void
}) => {

  const [ value, setValue ] = useState(props.value);
  const [ options, setOptions ] = useState<{ value: string, card: Card }[]>([]);

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  const handleChange = (value: string, option: any) => {
    if (option.value) {
      setValue(v => {
        const i = find(v);
        let str = v.substr(0, i) + `《${value}》`;
        props.onChange(str);
        return str;
      });
    } else {
      setValue(value);
      props.onChange(value);
    }
  };
  const find = (value: string) => {
    let i = value.indexOf("#");
    if (i !== -1) return i;
    i = value.indexOf("＃");
    return i;
  };
  const handleSearch = (value: string) => {
    const i = find(value);
    if (i === -1) {
      setOptions([]);
      return;
    }
    const word = value.substr(i + 1);
    const newOptions = Enumerable.from(props.cards)
      .distinct(c => c.name)
      .where(c => c.name.includes(word) || c.name_kana.includes(word) || c.text.includes(word))
      .select(c => ({ value: c.name, card: c }))
      .orderBy(c => (c.card.name.includes(word) ? "a" : c.card.name_kana.includes(word) ? "b" : "c") + c.value)
      .toArray();
    setOptions(newOptions);
  };
  const handleSelect = useCallback((value: string) => {
    setOptions([]);
  }, []);

  return (props.cards.length > 0 ? <AutoComplete
    onSearch={handleSearch}
    onSelect={handleSelect}
    value={value}
    style={{ width: isMobile ? "100%" : 600 }}
    onChange={handleChange}
    placeholder="text"
    options={
      [
        {
          label: "#でカード名を補完できます",
          options: options.map(e => ({
            value: e.value,
            label: <>
              <div style={{ display: "flex" }}>
                <img alt={e.value} src={`/api/cards/${e.card.key}/image`} height={64} />
                <div style={{ margin: 2 }}>
                  <div style={{ opacity: .6, fontSize: "80%" }}>{e.card.cost} {e.card.cardtype}</div>
                  <div>{e.value}</div>
                </div>
              </div>
            </>
          }
          ))
        }
      ]
    }
  >
    {isMobile ? <Input.TextArea /> : <Input />}
  </AutoComplete> : (
    isMobile ? <Input.TextArea
      value={value}
      style={{ width: "100%" }}
      onChange={e => {
        setValue(e.target.value);
        props.onChange(e.target.value);
      }}
      placeholder="text"
    /> : <Input
      value={value}
      style={{ width: 600 }}
      onChange={e => {
        setValue(e.target.value);
        props.onChange(e.target.value);
      }}
      placeholder="text"
  />));
};

class KindEdit extends Component<{
  value: SystemEventKind,
  onChange: (value: SystemEventKind)=>void,
  canDistribute: boolean
}, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      value: props.value,
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(value: SystemEventKind) {
    this.setState({ value });
    this.props.onChange(value);
  }

  componentDidUpdate(prevProps: any) {
    if (this.props.value !== prevProps.value) {
      this.setState({ value: this.props.value });
    }
  }

  render() {
    return <Select
      onChange={this.handleChange}
      value={this.state.value}
      style={{ width: 120 }}
      >
        <Select.Option value="add">追加</Select.Option>
        <Select.Option value="reset">リセット</Select.Option>
        {this.props.canDistribute && <Select.Option value="distribution">購入権配布</Select.Option>}
    </Select>
  }
}

const SubmitButton = (props: {
  mode: "add" | "edit",
  onSbumit?: ()=>void,
  onCancel?: ()=>void,
  onDelete?: ()=>void,
}) => {

  return props.mode === "add" ?
    <Button type="primary" onClick={props.onSbumit}>登録</Button> :
    <>
      <Button onClick={props.onCancel}>キャンセル</Button>
      <Popconfirm
        placement="bottom"
        title="削除しますか？"
        onConfirm={props.onDelete}
        okText="削除"
        okType="danger"
        cancelText="キャンセル"
      >
        <Button danger>削除</Button>
      </Popconfirm>
      <Button onClick={props.onSbumit} type="primary">更新</Button>
    </>
}

class DuelEventEdit extends Component<Props, DuelState> {
  public static defaultProps: DefaultProps = {
    data: {
      author: "", // not use
      date: moment(),
      id: 0, // not use
      content_id: 0,
      type: "duel",
      detail: {
        user1: "",
        user2: "",
        result: 0,
        text: "",
      },
      active: true,
    },
    mode: "add",
  };
  constructor(props: Props) {
    super(props);
    const d = props.data.detail as DuelDetail;
    this.state = {
      date: props.data.date,
      contentId: props.data.content_id || props.contentId || 0,
      user1: d.user1,
      user2: d.user2,
      result: d.result,
      text: d.text,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  componentDidUpdate(prevProps: Props) {
    if (this.props.data !== prevProps.data) {
      const d = this.props.data.detail as DuelDetail;
      this.setState({
        date: this.props.data.date,
        user1: d.user1,
        user2: d.user2,
        result: d.result,
        text: d.text,
      });
    }
  }

  handleSubmit() {
    const data = this.state;
    console.log(data);
    switch (this.props.mode) {
      case "add":
        API.POST("/events", {
          date: data.date.format(),
          type: "duel",
          content_id: data.contentId,
          user1: data.user1,
          user2: data.user2,
          result: data.result,
          text: data.text,
        }, e => {
          this.props.onAdd && this.props.onAdd({ ...e, date: moment(e.date) });
          this.setState({ text: "" });
        });
        break;
      case "edit":
        API.PUT(`/events/${this.props.data.id}`, {
          date: data.date.format(),
          user1: data.user1,
          user2: data.user2,
          result: data.result,
          text: data.text,
        }, e => {
          this.props.onEdit && this.props.onEdit({ ...e, date: moment(e.date) });
        });
        break;
    }
  }
  handleDelete() {
    API.DELETE(`/events/${this.props.data.id}`, _ => {
      this.props.onDelete && this.props.onDelete(this.props.data);
    });
  }
  render() {

    return <>
      <DatePicker
        value={this.state.date}
        onChange={v => v && this.setState({ date: v })}
      />
      <UserEdit
        users={this.props.users}
        value={this.state.user1}
        onChange={user1 => this.setState({ user1 })}
        />
      <ResultEdit
        value={this.state.result}
        onChange={result => this.setState({ result })}
        />
      <UserEdit
        users={this.props.users}
        value={this.state.user2}
        onChange={user2 => this.setState({ user2 })}
        />
      <TextEdit
        cards={this.props.cards || []}
        value={this.state.text}
        onChange={text => this.setState({ text })}
        />
      <br />
      <SubmitButton
        mode={this.props.mode}
        onSbumit={this.handleSubmit}
        onCancel={this.props.onCancel}
        onDelete={this.handleDelete}
        />
    </>;
  }
}

class SystemEventEdit extends Component<Props, SystemState> {
  public static defaultProps: DefaultProps = {
    data: {
      author: "", // not use
      date: moment(),
      id: 0, // not use
      content_id: 0,
      type: "system",
      detail: {
        username: "",
        kind: "add",
      },
      active: true,
    },
    mode: "add",
  };
  constructor(props: Props) {
    super(props);
    const d = props.data.detail as SystemDetail;
    this.state = {
      date: props.data.date,
      contentId: props.data.content_id || props.contentId || 0,
      username: d.username || props.me?.name || "",
      kind: d.kind,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  componentDidUpdate(prevProps: Props) {
    if (this.props.data !== prevProps.data) {
      const d = this.props.data.detail as SystemDetail;
      this.setState({
        date: this.props.data.date,
        username: d.username || "",
        kind: d.kind,
      });
    }
  }

  handleSubmit() {
    const data = this.state;
    console.log(data);
    switch (this.props.mode) {
      case "add":
        API.POST("/events", {
          date: this.state.date.format(),
          type: "system",
          content_id: this.state.contentId,
          username: this.state.username,
          kind: this.state.kind,
        }, e => {
          this.props.onAdd && this.props.onAdd({ ...e, date: moment(e.date) });
        });
        break;
      case "edit":
        API.PUT(`/events/${this.props.data.id}`, {
          date: this.state.date.format(),
          username: this.state.username,
          kind: this.state.kind,
        }, e => {
          this.props.onEdit && this.props.onEdit({ ...e, date: moment(e.date) });
        });
        break;
    }
  }
  handleDelete() {
    API.DELETE(`/events/${this.props.data.id}`, _ => {
      this.props.onDelete && this.props.onDelete(this.props.data);
    });
  }
  render() {

    return <>
      <DatePicker
        value={this.state.date}
        onChange={v => v && this.setState({ date: v })}
      />
      <UserEdit
        users={this.props.users}
        value={this.state.username}
        onChange={username => this.setState({ username })}
        />
      <KindEdit
        value={this.state.kind}
        onChange={kind => this.setState({ kind })}
        canDistribute={this.props.me?.isOrganizer() || false}
        />
      <br />
      <SubmitButton
        mode={this.props.mode}
        onSbumit={this.handleSubmit}
        onCancel={this.props.onCancel}
        onDelete={this.handleDelete}
        />
    </>;
  }
}


class EventEdit extends Component<Props, State> {
  public static defaultProps: DefaultProps = {
    data: {
      author: "", // not use
      date: moment(),
      id: 0, // not use
      content_id: 0,
      type: "duel",
      detail: {
        username: "",
        kind: "add",
      },
      active: true,
    },
    mode: "add",
  };

  render = () => <>
    {this.props.mode === "add" ?
      <Tabs defaultActiveKey={this.props.data.type}>
        <Tabs.TabPane tab="対戦記録" key="duel">
          <DuelEventEdit
            me={this.props.me}
            onAdd={this.props.onAdd}
            users={this.props.users}
            cards={this.props.cards}
            contentId={this.props.contentId}
          />
        </Tabs.TabPane>
        <Tabs.TabPane tab="パック追加" key="system">
          <SystemEventEdit 
            me={this.props.me}
            onAdd={this.props.onAdd}
            users={this.props.users}
            contentId={this.props.contentId}
          />
        </Tabs.TabPane>
      </Tabs> : 
      this.props.data.type === "duel" ? 
        <DuelEventEdit {...this.props} /> :
        <SystemEventEdit {...this.props} />
    }
  </>
};
export default EventEdit;
