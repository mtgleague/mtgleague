import React, { Component, useState } from "react";
import { CommentData, FileData } from "model/Content";
import { Comment, Typography, Button, Tooltip, Popconfirm } from "antd";
import MarkdownContent from "./MarkdownContent";
import API from "api/API";
import moment from "moment";
import SaveButton from "./SaveButton";
import style from 'assets/League.module.css';
import { User } from "model/User";

const MDComment = (props: {
  me: User,
  value: CommentData,
  contentId: number,
  onUploadFiles: (files: FileData[]) => void,
  onChange: (value: CommentData) => void,
  onDelete: (value: CommentData) => void,
}) => {
  const date = moment(props.value.date);
  const [isEdit, setIsEdit] = useState(false);
  const [value, setValue] = useState(props.value.text);
  let originalValue = props.value.text;
  const canEdit = props.me.isPerson(props.value.author);
  const handleEdit = () => setIsEdit(true);
  const handleDelete = () => {
    API.DELETE(`/comments/${props.value.id}`, _ => {
      props.onDelete(props.value);
    });
  };
  const handleSave = () => {
    API.PUT(`/comments/${props.value.id}`, {
      text: value,
    }, e => {
      setIsEdit(false);
      originalValue = e.text;
      setValue(e.text);
      props.onChange(e);
    });
  };
  const handleCancel = () => {
    setValue(originalValue);
    setIsEdit(false);
  };

  return <Comment
    content={<>
      <MarkdownContent
        content={value}
        contentId={props.contentId}
        isEdit={isEdit}
        onUploadFiles={props.onUploadFiles}
        onChange={v => setValue(v)}
        height="5em"
        toolbar={false}
      />
      {props.value.edit && <span style={{
        position: "absolute",
        right: 0,
        top: 0,
        opacity: 0.3,
      }}>編集済み</span>}
      {isEdit && (<>
        <SaveButton
          canSave={value !== "" && value !== originalValue}
          onCancel={handleCancel}
          onSave={handleSave}
          />
      </>)}
    </>}
    actions={!canEdit || isEdit ? [] : [
      <span onClick={handleEdit}>編集</span>,
      <Popconfirm
        placement="top"
        title="コメントを削除しますか？"
        onConfirm={handleDelete}
        okText="削除"
        okType="danger"
        cancelText="キャンセル"
        >
        <span>削除</span>
      </Popconfirm>,
    ]}
    author={<span className={style["comment-author"]}>{props.value.author}</span>}
    datetime={(
      <Tooltip title={date.format('YYYY-MM-DD HH:mm:ss')}>
        <span>{date.fromNow()}</span>
      </Tooltip>
      )}
    />
}

class Comments extends Component<
  {
    me: User,
    contentId: number,
    onUploadFiles: (files: FileData[]) => void,
  },
  {
    comments: CommentData[],
    sendingValue: string,
    sendingEdit: boolean,
  }
  > {

  constructor(props: any) {
    super(props);
    this.state = {
      comments: [],
      sendingValue: "",
      sendingEdit: false,
    };
    this.handleChangeSending = this.handleChangeSending.bind(this);
    this.handleSubmitSending = this.handleSubmitSending.bind(this);
    this.handleEditComment = this.handleEditComment.bind(this);
    this.handleDeleteComment = this.handleDeleteComment.bind(this);
  }

  componentDidMount() {
    this.loadComments();
  }

  componentDidUpdate(prevProps: any) {
    if (prevProps.contentId !== this.props.contentId) {
      this.loadComments();
    }
  }

  loadComments() {
    API.GET(`/contents/${this.props.contentId}/comments`, comments => {
      this.setState({ comments });
    })
  }
  handleChangeSending(value: string) {
    this.setState({ sendingValue: value });
  }
  handleSubmitSending() {
    API.POST(`/contents/${this.props.contentId}/comments`, {
      text: this.state.sendingValue,
    }, e => {
      this.setState(s => {
        const comments = s.comments.slice();
        comments.push(e);
        return { comments, sendingValue: "", sendingEdit: false };
      })
    });
  }
  handleEditComment(value: CommentData) {
    this.setState(s => {
      const comments = s.comments.slice();
      const index = comments.findIndex(c => c.id === value.id);
      if (index === -1) return null;
      comments[index] = value;
      return { comments };
    });
  }
  handleDeleteComment(value: CommentData) {
    this.setState(s => {
      const comments = s.comments.slice();
      const index = comments.findIndex(c => c.id === value.id);
      if (index === -1) return null;
      comments.splice(index, 1);
      return { comments };
    })
  }

  render = () => {
    return <div>
      <Typography.Title level={3}>コメント</Typography.Title>
      <div style={{ margin: "0 2em" }}>
        {this.state.comments.map(item => (
          <MDComment
            me={this.props.me}
            key={item.id}
            value={item}
            contentId={this.props.contentId}
            onUploadFiles={this.props.onUploadFiles}
            onChange={this.handleEditComment}
            onDelete={this.handleDeleteComment}
            />
        ))}
        {this.state.sendingEdit ? <>
          <MarkdownContent
            contentId={this.props.contentId}
            onUploadFiles={this.props.onUploadFiles}
            content={this.state.sendingValue}
            onChange={this.handleChangeSending}
            height="5em"
            toolbar={false}
            isEdit={true}
            />
          <SaveButton
            canSave={this.state.sendingValue !== ""}
            saveText="コメントする"
            onSave={this.handleSubmitSending}
            onCancel={() => this.setState({ sendingEdit: false })}
            />
        </> : <>
          {!this.props.me.isGuest() && <Button
            style={{ 
              width: "100%",
              height: "5em",
              textAlign: "left",
              cursor: "text",
            }}
            onClick={() => this.setState({ sendingEdit: true })}
            type="ghost"
            >
            コメントする...
          </Button>}
        </>}
      </div>
    </div>
  }
}

export default Comments;
