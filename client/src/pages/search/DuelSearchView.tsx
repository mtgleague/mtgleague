import React, { useCallback, useEffect, useState } from "react";
import { Input, PageHeader, Pagination, Space, Typography } from "antd";
import API from "api/API";
import queryString from 'query-string';
import { useHistory, useLocation } from "react-router";
import { EventData, DuelDetail } from "model/Event";
import leagueStyle from 'assets/League.module.css';

const { Search } = Input;

const SearchResult = (props: { event: EventData }) => {
  const eventData = props.event;
  const duelData = props.event.detail as DuelDetail;
  const r1 = duelData.result === 0 ? "o" : "x";
  const r2 = duelData.result === 1 ? "o" : "x";

  return (
    <div>
      <div>
        <a href={`/?id=${eventData.content_id}`}>{eventData.date.toLocaleString()}</a>
        &nbsp;
        {duelData.user1}&nbsp;
        <span className={leagueStyle["win-lose"]}>{r1}</span>
        &nbsp;-&nbsp;
        <span className={leagueStyle["win-lose"]}>{r2}</span>
        &nbsp;{duelData.user2}
      </div>
      <div>
        {duelData.text}
      </div>
    </div>
  )
};

const DuelSearchView = (props: any) => {
  const [results, setResults] = useState<EventData[]>([]);
  const [searching, setSearching] = useState(false);
  const [errorText, setErrorText] = useState("");
  const history = useHistory();
  const location = useLocation();
  const param = queryString.parse(location.search);
  const text = param.text as string;
  const page = Number(param.page) || 1;
  console.log(page);

  useEffect(() => {
    if (text) {
      setSearching(true);
      setErrorText("");
      API.GET(`/events/search?text=${encodeURIComponent(text)}`, res => {
        setResults(res);
        setSearching(false);
      }, err => {
        setErrorText("検索に失敗しました。クエリが正しいか確認してください。");
        // エラー時は検索を解除するだけ
        setSearching(false);
      });
    } else {
      setResults([]);
    }
  }, [text]);
  const handleSearch = useCallback((value: string) => {
    history.push(`?text=${encodeURIComponent(value)}`);
  }, [history]);
  const handleChangePage = useCallback((page: number) => {
    history.push(`?text=${encodeURIComponent(text)}&page=${page}`);
  }, [history, text]);

  const PageMax = 100;
  const Pager = () => {
    return results.length > PageMax ? <Pagination
      current={page}
      onChange={handleChangePage}
      total={results.length}
      pageSize={PageMax}
      showSizeChanger={false}
      /> : null;
  };

  return <div>
    <PageHeader title={<Typography.Title>履歴検索</Typography.Title>} />
    <Typography.Paragraph>
      過去の対戦を検索できます。<br />
      以下のクエリが利用可能です。
      <pre>{
        `user:<username> 指定のユーザーを含む
since:<2022-1-1> 指定日付以降
until:<2022-1-1> 指定日付以前`
      }
      </pre>
    </Typography.Paragraph>
    <Search
      enterButton
      onSearch={handleSearch}
      defaultValue={text}
      loading={searching}
      allowClear
      />
    {!searching && <>
      {text && (<div>「{text}」の検索 {results.length}件</div>)}
      {errorText ? <div style={{ color: "red" }}>{errorText}</div> : <>
        <Space direction="vertical" size="middle" style={{ display: 'flex', marginBottom: "3em" }}>
        <Pager />
        {results.slice((page - 1) * PageMax, page * PageMax).map(e => (
          <SearchResult key={e.id} event={e} />
        ))}
        <Pager />
        </Space>
      </>}
    </>}
  </div>
}

export default DuelSearchView;
