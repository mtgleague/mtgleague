
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE users(
    name text not null unique,
    password text not null
);
CREATE TABLE contents(
    id serial primary key,
    title text not null,
    start_date date,
    end_date date,
    target_sets text,
    text text not null
);
CREATE TABLE system_events(
    id serial primary key,
    username text not null,
    kind text not null
);
CREATE TABLE duel_events(
    id serial primary key,
    user1 text not null,
    user2 text not null,
    result smallint,
    text text
);
CREATE TABLE events(
    id serial primary key,
    date date,
    author text not null,
    type text not null,
    eid integer not null,
    cid integer not null
);

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE events;
DROP TABLE duel_events;
DROP TABLE system_events;
DROP TABLE contents;
DROP TABLE users;
