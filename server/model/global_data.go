package model

// GlobalData は全体に関する情報を表す型
type GlobalData struct {
	Date  LocalDateTime `json:"-" db:"date"`
	Key   string        `json:"-" db:"key"`
	Value string        `json:"-" db:"value"`
}

// Places は活動場所に関する情報の型
type Places struct {
	PlaceList `json:"list"`
	PlaceActive
}

// PlaceActive は活動場所の情報の型
type PlaceActive struct {
	Active    string        `json:"active"`
	UpdatedAt LocalDateTime `json:"updated_at"`
}

// PlaceList は活動場所の種類の型
type PlaceList []string
