package model

// Card はカードの型
type Card struct {
	Key         string  `json:"key" db:"key"`
	No          int     `json:"no" db:"no"`
	Name        string  `json:"name" db:"name"`
	NameKana    string  `json:"name_kana" db:"name_kana"`
	Set         string  `json:"set" db:"set"`
	Cost        string  `json:"cost" db:"cost"`
	Power       *string `json:"power" db:"power"`
	Toughness   *string `json:"toughness" db:"toughness"`
	Loyalty     *string `json:"loyalty" db:"loyalty"`
	SuperType   *string `json:"supertype" db:"supertype"`
	CardType    string  `json:"cardtype" db:"cardtype"`
	SubType     *string `json:"subtype" db:"subtype"`
	Rarity      string  `json:"rarity" db:"rarity"`
	Text        string  `json:"text" db:"text"`
	FlavorText  string  `json:"flavor_text" db:"flavor_text"`
	BackSideKey *string `json:"back_side_key" db:"back_side_key"`
	OtherKeys   *string `json:"other_keys" db:"other_keys"`
	ExtraImgUrl *string `json:"extra_img_url" db:"extra_img_url"`
}

// CardSetForImage はカードのセットを調べるための型
type CardSetForImage struct {
	Key string `db:"key"`
	Set string `db:"set"`
}

// CardSet はセットの型
type CardSet struct {
	Code        string    `json:"code" db:"code"`
	Name        string    `json:"name" db:"name"`
	NameEn      string    `json:"name_en" db:"name_en"`
	ReleaseNote string    `json:"release_note" db:"release_note"`
	StartDate   LocalDate `json:"start_date" db:"start_date"`
}

type CardWithSet struct {
	SetName string `json:"set_name" db:"set_name"`
	Card
}

// CardResult はカード検索の結果の型
type CardResult struct {
	Cards []CardWithSet `json:"cards"`
	Total int           `json:"total"`
}
