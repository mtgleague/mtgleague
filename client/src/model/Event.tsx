

export interface DuelDetail {
    user1: string,
    user2: string,
    result: 0 | 1,
    text: string,
}

export type SystemEventKind = "add" | "reset" | "distribution";
export interface SystemDetail {
    username: string,
    kind: SystemEventKind,
}

export interface EventData {
    id: number,
    date: moment.Moment,
    author: string,
    type: "duel" | "system",
    content_id: number,
    detail: DuelDetail | SystemDetail,
    active: boolean,
}
