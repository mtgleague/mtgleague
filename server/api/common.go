package api

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"example.com/m/common"
	"example.com/m/ws"
	jwt "github.com/appleboy/gin-jwt"
	"github.com/gin-gonic/gin"
	"github.com/go-gorp/gorp"
	"github.com/line/line-bot-sdk-go/linebot"
)

// Context は api 以下に受け渡すものを保持します
type Context struct {
	API            *gin.RouterGroup
	APIAuth        *gin.RouterGroup
	Download       *gin.RouterGroup
	DBmap          *gorp.DbMap
	AuthMiddleware *jwt.GinJWTMiddleware
	Component      *common.Component
	LineBot        *linebot.Client
}

func setNotFoundResult(c *gin.Context, message string) {
	log.Print(message)
	c.JSON(http.StatusNotFound, gin.H{
		"code":    http.StatusNotFound,
		"message": message,
	})
}

func setBadRequestResult(c *gin.Context, message string) {
	log.Print(message)
	c.JSON(http.StatusBadRequest, gin.H{
		"code":    http.StatusBadRequest,
		"message": message,
	})
}

func setForbidden(c *gin.Context, message string) {
	log.Print(message)
	c.JSON(http.StatusForbidden, gin.H{
		"code":    http.StatusForbidden,
		"message": message,
	})
}

func setUnauthorized(c *gin.Context, message string) {
	log.Print(message)
	c.JSON(http.StatusUnauthorized, gin.H{
		"code":    http.StatusUnauthorized,
		"message": message,
	})
}

func setServerError(c *gin.Context, err error) {
	log.Print(err)
	c.JSON(500, gin.H{
		"code":    500,
		"message": err.Error(),
	})
}

type apiHelper struct {
	context     *gin.Context
	err         error
	dbmap       *gorp.DbMap
	transaction *gorp.Transaction
	myRole      string
	comp        *common.Component
}

// apiError はapiHelper内ででたエラーを表す型
type apiError struct {
	message string
}

func (e *apiError) Error() string {
	return e.message
}

func (a *apiHelper) setBadRequestResult(message string) {
	if a.err != nil {
		return
	}
	setBadRequestResult(a.context, message)
	a.err = &apiError{
		message: message,
	}
}

func (a *apiHelper) setForbidden(message string) {
	if a.err != nil {
		return
	}
	setForbidden(a.context, message)
	a.err = &apiError{
		message: message,
	}
}

func (a *apiHelper) setUnauthorized(message string) {
	if a.err != nil {
		return
	}
	setUnauthorized(a.context, message)
	a.err = &apiError{
		message: message,
	}
}

func (a *apiHelper) getParamInt(name string) int {
	if a.err != nil {
		return 0
	}
	c := a.context
	id, err := strconv.Atoi(c.Param(name))
	if err != nil {
		a.err = err
		setBadRequestResult(c, "invalid param "+name+".")
		a.rollback()
		return 0
	}
	return id
}

func (a *apiHelper) getParamStr(name string) string {
	if a.err != nil {
		return ""
	}
	c := a.context
	str := c.Param(name)
	return str
}

func (a *apiHelper) getID() int {
	return a.getParamInt("id")
}

func (a *apiHelper) getMyName() string {
	if a.err != nil {
		return ""
	}
	clamis := jwt.ExtractClaims(a.context)
	myName := clamis["id"].(string)
	return myName
}

func (a *apiHelper) getMyRole() string {
	if a.err != nil {
		return ""
	}
	if len(a.myRole) == 0 {
		myName := a.getMyName()
		myRole, err := a.dbmap.SelectStr("SELECT role FROM users WHERE name=$1", myName)
		if err != nil {
			return ""
		}
		a.myRole = myRole
	}
	return a.myRole
}

func (a *apiHelper) verifyPerson(name string) bool {
	if a.err != nil {
		return false
	}
	if name == a.getMyName() {
		return true
	}
	if a.isRoleAdmin() {
		return true
	}
	a.rollback()
	a.setForbidden("not authorized user.")
	return false
}

func (a *apiHelper) verifyUser() bool {
	if a.err != nil {
		return false
	}
	if !a.isRoleGuest() {
		return true
	}
	a.rollback()
	a.setForbidden("not authorized user.")
	return false
}

func (a *apiHelper) verifyOrganizer() bool {
	if a.err != nil {
		return false
	}
	if a.isRoleOrganizer() {
		return true
	}
	a.rollback()
	a.setForbidden("not authorized user.")
	return false
}

func (a *apiHelper) verifyAppDeveloper() bool {
	if a.err != nil {
		return false
	}
	if a.isRoleAppDeveloper() {
		return true
	}
	a.rollback()
	a.setForbidden("not authorized user.")
	return false
}

func (a *apiHelper) verifyAdmin() bool {
	if a.err != nil {
		return false
	}
	if a.isRoleAdmin() {
		return true
	}
	a.rollback()
	a.setForbidden("not authorized user.")
	return false
}

func (a *apiHelper) isRoleAdmin() bool {
	if a.err != nil {
		return false
	}
	if a.getMyRole() == "admin" {
		return true
	}
	return false
}

func (a *apiHelper) isRoleOrganizer() bool {
	if a.err != nil {
		return false
	}
	if a.getMyRole() == "organizer" || a.isRoleAdmin() {
		return true
	}
	return false
}

func (a *apiHelper) isRoleAppDeveloper() bool {
	if a.err != nil {
		return false
	}
	if a.getMyRole() == "appdeveloper" || a.isRoleAdmin() {
		return true
	}
	return false
}

func (a *apiHelper) isRoleGuest() bool {
	if a.err != nil {
		return false
	}
	if a.getMyRole() == "guest" {
		return true
	}
	return false
}

func (a *apiHelper) begin() {
	if a.err != nil {
		return
	}
	c := a.context
	t, err := a.dbmap.Begin()
	if err != nil {
		a.err = err
		setServerError(c, err)
		return
	}
	a.transaction = t
}

func (a *apiHelper) commit() {
	if a.err != nil {
		return
	}
	c := a.context
	err := a.transaction.Commit()
	if err != nil {
		a.err = err
		setServerError(c, err)
		a.rollback()
		return
	}
	a.transaction = nil
}

func (a *apiHelper) rollback() {
	if a.transaction != nil {
		log.Print("rollback")
		a.transaction.Rollback()
		a.transaction = nil
	}
}

func (a *apiHelper) selectAny(holder interface{}, query string, args ...interface{}) {
	if a.err != nil {
		return
	}
	c := a.context
	_, err := a.dbmap.Select(holder, query, args...)
	if err != nil {
		a.err = err
		setServerError(c, err)
		a.rollback()
		return
	}
}

func (a *apiHelper) selectOne(holder interface{}, query string, args ...interface{}) {
	if a.err != nil {
		return
	}
	c := a.context
	err := a.dbmap.SelectOne(holder, query, args...)
	if err != nil {
		a.err = err
		setServerError(c, err)
		a.rollback()
		return
	}
}

func (a *apiHelper) selectInt(query string, args ...interface{}) int {
	if a.err != nil {
		return 0
	}
	c := a.context
	r, err := a.dbmap.SelectInt(query, args...)
	if err != nil {
		a.err = err
		setServerError(c, err)
		a.rollback()
		return 0
	}
	return int(r)
}

func (a *apiHelper) selectStr(query string, args ...interface{}) string {
	if a.err != nil {
		return ""
	}
	c := a.context
	r, err := a.dbmap.SelectStr(query, args...)
	if err != nil {
		a.err = err
		setServerError(c, err)
		a.rollback()
		return ""
	}
	return r
}

func (a *apiHelper) getLastValue() int {
	if a.err != nil {
		return 0
	}
	c := a.context
	id, err := a.dbmap.SelectInt("SELECT LASTVAL()")
	if err != nil {
		a.err = err
		setServerError(c, err)
		a.rollback()
		return 0
	}
	return int(id)
}

func (a *apiHelper) insert(list ...interface{}) {
	if a.err != nil {
		return
	}
	err := a.dbmap.Insert(list...)
	if err != nil {
		a.err = err
		setServerError(a.context, err)
		a.rollback()
		return
	}
}

func (a *apiHelper) update(list ...interface{}) {
	if a.err != nil {
		return
	}
	_, err := a.dbmap.Update(list...)
	if err != nil {
		a.err = err
		setServerError(a.context, err)
		a.rollback()
		return
	}
}

func (a *apiHelper) delete(list ...interface{}) {
	if a.err != nil {
		return
	}
	_, err := a.dbmap.Delete(list...)
	if err != nil {
		a.err = err
		setServerError(a.context, err)
		a.rollback()
		return
	}
}

func (a *apiHelper) exec(query string, list ...interface{}) {
	if a.err != nil {
		return
	}
	_, err := a.dbmap.Exec(query, list...)
	if err != nil {
		a.err = err
		setServerError(a.context, err)
		a.rollback()
		return
	}
}

func (a *apiHelper) broadcast(key string, obj interface{}) {
	if a.err != nil {
		return
	}
	s := ws.SocketHelper{
		Comp: a.comp,
	}
	msg, _ := json.Marshal(gin.H{
		"key":   key,
		"value": obj,
	})
	s.Broadcast(msg)
}

func (a *apiHelper) broadcastOthers(key string, obj interface{}) {
	if a.err != nil {
		return
	}
	s := ws.SocketHelper{
		Comp: a.comp,
	}
	msg, _ := json.Marshal(gin.H{
		"key":   key,
		"value": obj,
	})
	s.BroadcastOthers(msg, a.context)
}
