import React, { Component } from 'react';
import { RouteChildrenProps } from 'react-router-dom';
import { Button, notification, Form, DatePicker, Input, Typography, PageHeader, Select } from 'antd';
import API from 'api/API';
import moment from 'moment';
import { Content } from 'model/Content';
import { CardSet } from 'model/Card';

interface Props extends RouteChildrenProps {
  onAdd: (item: Content) => void;
}
interface State {
  cardSets: CardSet[];
}

class NewLeague extends Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      cardSets: [],
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    API.GET("/card_sets", cardSets => this.setState({ cardSets }));
  }

  handleSubmit(value: any) {
    API.POST("/contents", {
      title: value.title,
      start_date: moment(value.range[0]).format(),
      end_date: moment(value.range[1]).format(),
      target_sets: (value.target_sets || []).join(","),
      week_max: 3,
      text: "",
    }, e => {
      notification.success({ message: "作成しました" });
      this.props.onAdd(e);
    });
  }
  render = () => {

    return <>
      <PageHeader title={<Typography.Title>新規ページ</Typography.Title>} />
      <Form
        onFinish={this.handleSubmit}
        >
        <Form.Item
          name="title"
          label="タイトル"
          rules={[{ required: true, message: 'タイトルを設定してください' }]}
          >
          <Input />
        </Form.Item>
        <Form.Item
          name="range"
          label="期間"
          rules={[{ required: true, message: '期間を設定してください' }]}
          >
          <DatePicker.RangePicker />
        </Form.Item>
        <Form.Item
          name="target_sets"
          label="対象カードセット"
          >
          <Select mode="tags">
            {this.state.cardSets.map(s => <Select.Option key={s.code} value={s.code}>{s.name}({s.code})</Select.Option>)}
          </Select>
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">作成</Button>
        </Form.Item>
      </Form>
    </>
  };
}

export default NewLeague;
