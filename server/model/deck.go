package model

import (
	"database/sql/driver"
	"encoding/json"
)

// Deck はデッキの型
type Deck struct {
	ID  int    `json:"id" db:"id"`
	Key string `json:"key" db:"key"`
	DeckNew
	Note        string   `json:"note" db:"note"`
	Cards       CardList `json:"cards" db:"cards"`
	SideCards   CardList `json:"side_cards" db:"side_cards"`
	UseSets     string   `json:"use_sets" db:"use_sets"`
	AccessLevel string   `json:"access_level" db:"access_level"`
}

// DeckNew はデッキを新規追加時の型
type DeckNew struct {
	Name      string        `json:"name" db:"name"`
	Author    string        `json:"author" db:"author"`
	CreatedAt LocalDateTime `json:"created_at" db:"created_at"`
	UpdatedAt LocalDateTime `json:"updated_at" db:"updated_at"`
}

// DeckEdit はデッキ編集時パラメータの型
type DeckEdit struct {
	Name        string   `json:"name"`
	Note        string   `json:"note"`
	Cards       CardList `json:"cards"`
	SideCards   CardList `json:"side_cards"`
	UseSets     string   `json:"use_sets"`
	AccessLevel string   `json:"access_level"`
}

// CardList はカードの集まり
type CardList []CardCount

// CardCount はカードの枚数を表す型
type CardCount struct {
	Key string `json:"key"`
	Num int    `json:"num"`
}

// Value はDBに保存する値に変換
func (l CardList) Value() (driver.Value, error) {
	return json.Marshal(l)
}

// Scan はDBから読み込んだ値をCardListに変換
func (l *CardList) Scan(value interface{}) error {
	bytes := value.([]byte)
	return json.Unmarshal(bytes, l)
}
