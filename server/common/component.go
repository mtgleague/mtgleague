package common

import (
	"database/sql"
	"sync"

	"github.com/gin-gonic/gin"
	"gopkg.in/olahol/melody.v1"
)

type Component struct {
	Router   *gin.Engine
	Melody   *melody.Melody
	DB       *sql.DB
	Mutex    *sync.Mutex
	Volatile map[string]string
}
