import React, { useRef, useEffect, useCallback, forwardRef, ComponentType } from 'react';
import { throttle } from 'lodash';
import { ListChildComponentProps, VariableSizeList } from 'react-window';

const getScrollPosition = (axis: "x" | "y") => {
  if (axis === "x") {
    return window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft || 0;
  } else {
    return window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
  }
}

interface ComponentProps
{
  ref: React.MutableRefObject<VariableSizeList>,
  outerRef: React.MutableRefObject<any>,
  style: any,
  onScroll: ({ scrollLeft, scrollTop, scrollOffset, scrollUpdateWasRequested }: any) => void;
}

interface Param
{
  children: (props: ComponentProps) => any;
  throttleTime: number;
}

const ReactWindowScroller = forwardRef<VariableSizeList, Param>(({
  children,
  throttleTime = 10,
}, refParam) => {
  const outerRef = useRef<any>();
  const ref = refParam as React.MutableRefObject<VariableSizeList>;

  useEffect(() => {
    const handleWindowScroll = throttle(() => {
      const { offsetTop = 0 } = outerRef.current || {}
      const scrollTop = getScrollPosition('y') - offsetTop
      ref.current && ref.current.scrollTo(scrollTop)
    }, throttleTime)
    const handleWIndowResize = throttle(() => {
      ref.current && ref.current.resetAfterIndex(0);
    }, throttleTime)

    window.addEventListener('scroll', handleWindowScroll)
    window.addEventListener('resize', handleWIndowResize)
    return () => {
      handleWindowScroll.cancel()
      window.removeEventListener('scroll', handleWindowScroll)
      handleWIndowResize.cancel()
      window.removeEventListener('resize', handleWIndowResize)
    }
  }, [ref, throttleTime]);

  const onScroll = useCallback(
    ({ scrollLeft, scrollTop, scrollOffset, scrollUpdateWasRequested }) => {
      if (!scrollUpdateWasRequested) return
      const top = getScrollPosition('y')
      const left = getScrollPosition('x')
      const { offsetTop, offsetLeft } = outerRef.current || {};

      scrollOffset += Math.min(top, offsetTop)
      scrollTop += Math.min(top, offsetTop)
      scrollLeft += Math.min(left, offsetLeft)

      if (scrollOffset !== top) window.scrollTo(0, scrollOffset)
    },
    []
  )
  const style = {
    width: '100%',
    height: '100%',
    display: 'inline-block',
    overflow: "visible"
  };

  return children({
    ref,
    outerRef,
    style,
    onScroll
  })
});

const VirtualList = React.memo(forwardRef((props: {
  itemCount: number,
  itemSize: (index: number, outerRef?: any, calcAreaRef?: any) => number,
  children: ComponentType<ListChildComponentProps>,
}, ref: any) => {
  const calcAreaRef = useRef<any>();

  return <>
    <div ref={calcAreaRef} />
    <ReactWindowScroller ref={ref} throttleTime={10}>
      {({ ref, outerRef, style, onScroll }: ComponentProps) => {
        // 初回の描画でcalcAreaRefがnullの状態できてしまうので遅延して再計算する
        if (!calcAreaRef.current) {
          setTimeout(() => ref.current.resetAfterIndex(0), 100);
        }
        return <VariableSizeList
          ref={ref}
          outerRef={outerRef}
          overscanCount={10}
          style={style}
          onScroll={onScroll}
          itemCount={props.itemCount}
          itemSize={index => props.itemSize(index, outerRef, calcAreaRef)}
          width="100%"
          height={window.innerHeight}
          children={props.children}
          />
      }
    }
    </ReactWindowScroller>
  </>;
}));

export default VirtualList;
