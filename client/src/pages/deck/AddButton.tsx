import React, { useState } from 'react';
import {
  Button,
  Modal,
  Input} from 'antd';
import {
  PlusOutlined} from '@ant-design/icons';
import style from 'assets/Decklist.module.css';


const AddButton = (props: { onCreate: (name: string)=>void }) => {

  const [visible, setVisible] = useState(false);
  const [value, setValue] = useState("");
  const onOK = () => {
    props.onCreate(value);
    setVisible(false);
  };
  const onCancel = () => {
    setVisible(false);
  };
  const onOpen = () => {
    setValue("");
    setVisible(true);
  };
  
  return <>
    <Button
      className={style["new-deck-button"]}
      onClick={onOpen}
      icon={<PlusOutlined />}
      >
        新しいデッキ
      </Button>
    <Modal
      visible={visible}
      onOk={onOK}
      okButtonProps={{
        disabled: !value,
      }}
      onCancel={onCancel}
      okText="作成"
      title="新しいデッキ"
      centered
      >
        名前：<Input value={value} onChange={e => setValue(e.target.value)} />
      </Modal>
  </>;
};

export default AddButton;
