package main

import (
	"path"
	"path/filepath"
	"sync"

	"example.com/m/api"
	"example.com/m/common"
	"example.com/m/db"
	"example.com/m/ws"
	"gopkg.in/olahol/melody.v1"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
)

func main() {

	godotenv.Load()

	var comp common.Component
	comp.Router = gin.Default()
	comp.Melody = melody.New()
	comp.DB = db.ConnectToDB()
	comp.Mutex = new(sync.Mutex)
	comp.Volatile = make(map[string]string)

	db.Migration(comp.DB)

	// 基本はすべてindex.htmlに飛ばす
	folderPath := "./web"
	comp.Router.NoRoute(func(c *gin.Context) {
		_, file := path.Split(c.Request.RequestURI)
		ext := filepath.Ext(file)
		// ディレクトリアクセス（ファイル名がない）かパスクエリ（拡張子がない）
		if file == "" || ext == "" {
			c.File(folderPath + "/index.html")
		} else {
			c.File(folderPath + c.Request.RequestURI)
		}
	})

	// WebSocket
	ws.RegisterSocket(&comp)

	// API登録
	api.RegisterAPI(&comp)

	comp.Router.Run()
}
