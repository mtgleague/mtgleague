import React, { Component } from 'react';
import 'assets/App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Main from 'pages/Main';
import Login from 'pages/Login';
import Signup from 'pages/Signup';

class App extends Component {

  render = () => (
    <BrowserRouter>
      <Switch>
        <Route exact path={`/login`} component={Login} />
        <Route exact path={`/signup`} component={Signup} />
        <Route path={`/`} component={Main} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
