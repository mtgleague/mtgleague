import React, { Component } from 'react';
import { Form, Input, Button, Alert } from 'antd';
import { Store } from 'antd/lib/form/interface';
import { RouteChildrenProps } from 'react-router-dom';
import { ValidateStatus } from 'antd/lib/form/FormItem';
import queryString from 'query-string';
import API from 'api/API';

interface Props extends RouteChildrenProps {
}
interface State {
  result: string,
  validUser: ValidateStatus,
  validPassword: ValidateStatus,
}

const layout = {
  labelCol: { span: 9 },
  wrapperCol: { span: 8 },
};
const tailLayout = {
  wrapperCol: { offset: 9, span: 12 },
};

class Signup extends Component<Props, State> {
  code: string | undefined;

  constructor(props: Props) {
    super(props);
    this.state = {
      result: "",
      validUser: "error",
      validPassword: "error",
    };
    const param = queryString.parse(this.props.location.search);
    if (typeof(param.code) === "string") {
      this.code = param.code;
    }

    this.onValueChange = this.onValueChange.bind(this);
    this.onFinish = this.onFinish.bind(this);
  }

  onValueChange(changedValues: Store, values: Store) {
    if (changedValues.username) {
      this.setState({
        validUser: changedValues.username !== "" ? "success" : "error",
      });
    }
    if (changedValues.password || changedValues.password2) {
      this.setState({
        validPassword: values.password !== "" && values.password === values.password2 ? "success" : "error",
      });
    }
  }

  onFinish(values: Store) {
    (async () => {
      const req = await fetch("/api/users", {
        method: "POST",
        body: JSON.stringify({
          name: values.username,
          password: values.password,
          code: this.code,
        })
      });
      const data = await req.json();
      if (data.code !== undefined && data.code !== 200) {
        this.setState({ result: data.message });
        return;
      }
      await API.POST("/users/logout", {});
      console.log("signup.");
      this.props.history.push("/login");
    })();
  }

  render = () => (
    <div style={{
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      height: "100vh",
    }}>
      <div
        style={{
        width: "50%"
      }}>
        {this.state.result && <Alert
          message="ユーザー登録に失敗しました"
          description={this.state.result}
          type="error"
          closable
          style={{
            margin: 10
          }}
        />}
        <Form
          {...layout}
          onFinish={this.onFinish}
          onValuesChange={this.onValueChange}
        >
          <Form.Item {...tailLayout}>
            <h2>ユーザー登録</h2>
          </Form.Item>

          <Form.Item
            label="ユーザー名"
            name="username"
            rules={[{ required: true, message: 'ユーザー名を入力してください' }]}
            validateStatus={this.state.validUser}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="パスワード"
            name="password"
            rules={[{ required: true, message: 'パスワードを入力してください' }]}
            hasFeedback
            validateStatus={this.state.validPassword}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            label="パスワード(確認)"
            name="password2"
            rules={[{ required: true, message: '同じパスワードを入力してください' }]}
            hasFeedback
            validateStatus={this.state.validPassword}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit" disabled={this.state.validUser !== "success" || this.state.validPassword !== "success"}>
              登録
          </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}

export default Signup;
