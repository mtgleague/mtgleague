
export type AccessLevel = "public"|"internal"|"private";
export interface Deck {
  id: number;
  key: string;
  name: string;
  memo: string;
  author: string;
  created_at: moment.Moment;
  updated_at: moment.Moment;
  cards: CardNum[];
  side_cards: CardNum[];
  use_sets: string;
  access_level: AccessLevel;
}

export interface CardNum {
  key: string;
  num: number;
}
