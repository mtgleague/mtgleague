import React, { useCallback } from 'react';
import { Button, Timeline, Typography, Tooltip, Modal } from 'antd';
import { 
  PlusCircleOutlined,
  ReloadOutlined,
  EditOutlined,
  StarOutlined,
} from '@ant-design/icons';
import style from 'assets/League.module.css';
import { EventData, SystemDetail, DuelDetail } from 'model/Event';
import { SameWeekYear } from './util';
import EventEdit from './DuelEventEdit';
import { User } from 'model/User';
import moment from 'moment';
import VirtualList from 'pages/VirtualList';
import { VariableSizeList } from 'react-window';
import { Card } from 'model/Card';

const { Paragraph, Link } = Typography;

const EditButton = (props: any) => {
  return <Tooltip placement="top" title="Edit">
    <Button
      type="link"
      onClick={props.onClick}
      style={{
        marginLeft: 5,
        padding: 0,
        height: 22,
      }}>
      <EditOutlined />
    </Button>
  </Tooltip>
}

const EditModal = (props: { 
  me: User,
  visible: boolean,
  data: EventData,
  users: User[],
  cards?: Card[],
  onClose: ()=>void,
  onChangeEvent: (type: string, data: EventData)=>void,
}) => {

  const handleEdit = useCallback(e => {
    props.onChangeEvent("edit", e);
    props.onClose();
  }, [props]);
  const handleDelete = useCallback(e => {
      props.onChangeEvent("delete", e);
      props.onClose();
  }, [props]);

  return <Modal
    title="履歴編集"
    visible={props.visible}
    footer={null}
    width={640}
    onCancel={props.onClose}
  >
    <EventEdit
      me={props.me}
      users={props.users}
      mode="edit"
      data={props.data}
      cards={props.cards}
      onEdit={handleEdit}
      onDelete={handleDelete}
      onCancel={props.onClose}
    />
  </Modal>
};

export type ChangeEventCallback = (type: string, e: EventData)=>void;

const SystemEvent = (props: {
  me: User,
  data: EventData,
  users: User[],
  onChangeEvent: ChangeEventCallback,
  onEditStart: (data: EventData) => void
}) => {
  const e = props.data;
  const d = e.detail as SystemDetail;
  const canEdit = props.me.isPerson(d.username) || props.me.isPerson(e.author);
  const message = d.kind === "add" ? "パックを追加" : d.kind === "reset" ? "リセット" : "購入権を配布";

  return <>
    <span id={`event-item-${e.id}`}>
      <span className={style["system-event"]}>
        [{e.date.format("YYYY-MM-DD")}]&nbsp;
        {`${d.username}が${message}しました`}
      </span>
      {canEdit && <EditButton onClick={() => props.onEditStart(props.data)} />}
    </span>
  </>
};

const LinkedText = (props: { text: string }) => {
  let text = props.text;
  const output: JSX.Element[] = [];
  while (text !== "") {
    const m = text.match(/《(.+?)》/);
    if (m && m.index !== undefined) {
      output.push(<span key={output.length}>{text.substr(0, m.index)}</span>);
      const url=`http://mtgwiki.com/index.php?search=${m[1]}`;
      output.push(<Link href={url} key={output.length} target="_blank">{m[0]}</Link>);
      text = text.substr(m.index + m[0].length);
    } else {
      output.push(<span key={output.length}>{text}</span>);
      break;
    }
  }
  return <>{output}</>;
}
const DuelEvent = (props: {
  me: User,
  data: EventData,
  users: User[],
  onChangeEvent: ChangeEventCallback,
  onEditStart: (data: EventData) => void
}) => {
  const d = props.data.detail as DuelDetail;
  const date = props.data.date.format("YYYY-MM-DD");
  const r1 = d.result === 0 ? "o" : "x";
  const r2 = d.result === 1 ? "o" : "x";
  const copyText = `${d.user1} :${r1}: - :${r2}: ${d.user2} ${d.text}`;
  const canEdit = props.me.name === props.data.author || !props.me.isGuest();

  return <>
    <span className={style["duel-event"]} id={`event-item-${props.data.id}`}>
      <Paragraph
        style={{ display: "inline" }}
        copyable={{ text: copyText }}
        delete={!props.data.active}
        >
        [{date}]&nbsp;
        {d.user1}&nbsp;
        <span className={style["win-lose"]}>{r1}</span>
        &nbsp;-&nbsp;
        <span className={style["win-lose"]}>{r2}</span>
        &nbsp;{d.user2} <LinkedText text={d.text} />
      </Paragraph>
      {canEdit && <EditButton onClick={() => props.onEditStart(props.data)} />}
    </span>
  </>
};

const SystemIcon = (d: SystemDetail) => {
  switch (d.kind) {
    case "add": return <PlusCircleOutlined />;
    case "reset": return <ReloadOutlined />;
    case "distribution": return <StarOutlined />;
  }
}

const EventItem = ((props: {
  me: User,
  data: EventData,
  color: string,
  users: User[],
  addClass: string,
  onChangeEvent: ChangeEventCallback,
  onEditStart: (data: EventData) => void
}) => {
  const e = props.data;
  return e.type === "duel" ?
    <Timeline.Item key={e.id} color={props.color} className={props.addClass}>
      <DuelEvent
        me={props.me}
        data={e}
        users={props.users}
        onChangeEvent={props.onChangeEvent}
        onEditStart={props.onEditStart}
        />
    </Timeline.Item> :
    <Timeline.Item
      key={e.id}
      color="gray"
      dot={SystemIcon(e.detail as SystemDetail)}
      className={props.addClass}
    >
      <SystemEvent
        me={props.me}
        data={e}
        users={props.users}
        onChangeEvent={props.onChangeEvent}
        onEditStart={props.onEditStart}
        />
    </Timeline.Item>
});

interface Props
{
  me: User,
  events: EventData[],
  users: User[],
  cards?: Card[],
  onChangeEvent: ChangeEventCallback,
  myEventOnly: boolean,
};

class DuelEventHistory extends React.PureComponent<Props, any> {
  list: React.RefObject<VariableSizeList>;

  constructor(props: Props) {
    super(props);
    this.state = {
      isOldEventShow: false,
      isVisibleEdit: false,
      editData: {} as EventData,
      viewEvents: this.filterViewEvents(false),
    }
    this.handleOldEventShow = this.handleOldEventShow.bind(this);
    this.calcSize = this.calcSize.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.list = React.createRef();
  }

  filterViewEvents(isOldEventShow: boolean) {
    let events = this.props.events;
    if (this.props.myEventOnly) {
      events = events.filter(e => {
        if (e.type === "duel") {
          const d = e.detail as DuelDetail;
          return d.user1 === this.props.me.name || d.user2 === this.props.me.name;
        } else {
          const d = e.detail as SystemDetail;
          return d.username === this.props.me.name;
        }
      });
    }
    const now = moment();
    const nowYear = now.year(), nowWeek = now.isoWeek();
    let viewEvents = events.map(e => ({ val: e, isNew: SameWeekYear(e.date, nowYear, nowWeek) }));
    if (!isOldEventShow) {
      viewEvents = viewEvents.filter(e => e.isNew);
    }
    return viewEvents;
  }

  componentDidUpdate(oldProps: Props) {
    if (this.props.myEventOnly !== oldProps.myEventOnly ||
        this.props.events !== oldProps.events) {
      this.setState((s: any) => ({ viewEvents: this.filterViewEvents(s.isOldEventShow) }));
      this.list.current?.resetAfterIndex(0);
    }
  }
  handleOldEventShow() {
    this.setState((s: any) => ({
      isOldEventShow: !s.isOldEventShow,
      viewEvents: this.filterViewEvents(!s.isOldEventShow)
    }));
  }

  renderItem({ index, style }: any) {
    const commonParam = {
      me: this.props.me,
      users: this.props.users,
      onChangeEvent: this.props.onChangeEvent,
      onEditStart: (data: EventData) => this.setState({ isVisibleEdit: true, editData: data }),
    };
    const e = this.state.viewEvents[index];
    const addClass = index === this.state.viewEvents.length - 1 ? "ant-timeline-item-last" : "";
    return <div key={e.val.id} style={style}>
      <EventItem
        addClass={addClass}
        {...commonParam}
        data={e.val}
        color={e.isNew ? "blue" : "green" }
        />
    </div>
  }
  calcSize(index: number, outerRef: any, calcAreaRef: any) {
    const elem = calcAreaRef.current;
    const e = this.state.viewEvents[index];
    if (!e || e.val.type === "system") return 31;
    if (!elem) return 31;
    const d = e.val.detail as DuelDetail;
    
    elem.style.lineHeight = "22px";
    const { clientWidth } = outerRef.current || {};
    // -26: 左のマージン、39: 編集・コピーボタン
    elem.style.width = `${clientWidth - 26}px`;
    elem.innerHTML =
      `<span>[0000-00-00] ${d.user1} x - x ${d.user2} ${d.text}</span>`
      + "<span style='width: 14px;margin-left: 4px;display:inline-block;'></span>"
      + "<span style='width: 16px;margin-left: 5px;display:inline-block;'></span>";
    elem.style.display = "block";
    const height = elem.clientHeight + 9;
    elem.style.display = "none";
    return height;
  }

  render() {

    return <>
    <Timeline mode="left">
      <Timeline.Item color="green" className={this.state.viewEvents.length ? "" : "ant-timeline-item-last"}>
        <Button type="link" onClick={this.handleOldEventShow}>{this.state.isOldEventShow ? "▲隠す" : "▼表示"}</Button>
      </Timeline.Item>
      <VirtualList
        ref={this.list}
        itemCount={this.state.viewEvents.length}
        itemSize={this.calcSize}
        >
          {this.renderItem}
      </VirtualList>
    </Timeline>
    <EditModal
      me={this.props.me}
      visible={this.state.isVisibleEdit}
      onClose={() => this.setState({ isVisibleEdit: false })}
      onChangeEvent={this.props.onChangeEvent}
      users={this.props.users}
      cards={this.props.cards}
      data={this.state.editData}
      />
    </>
  }
}

export default DuelEventHistory;
