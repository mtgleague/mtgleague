package model

import "database/sql"

// User は新規作成時のuserの型
type User struct {
	Name     string `form:"name" json:"name" db:"name"`
	Password string `form:"password" json:"password" db:"password"`
	Role     string `json:"role" db:"role"`
}

// ViewUser はすべてのユーザーが見えるuserの型
type ViewUser struct {
	Name string `form:"name" json:"name" db:"name"`
}

// UserFull は権限のあるユーザーが見えるuserの型
type UserFull struct {
	Name        string         `json:"name" db:"name"`
	Role        string         `json:"role" db:"role"`
	LineID      sql.NullString `json:"-" db:"line_uid"`
	ConnectLine bool           `json:"connect_line_id" db:"-"`
}

// UserLine はLINE連携時に使う型
type UserLine struct {
	Name   string         `db:"name"`
	LineID sql.NullString `db:"line_uid"`
}
