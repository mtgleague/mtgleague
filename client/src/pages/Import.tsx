import React, { Component } from 'react';
import { RouteChildrenProps } from 'react-router-dom';
import { Button, notification, Form, Input, InputNumber, PageHeader, Typography } from 'antd';
import queryString from 'query-string';
import API from 'api/API';
import moment from 'moment';

const { TextArea } = Input;

interface Props extends RouteChildrenProps {
}
interface State {
}

class Import extends Component<Props, State> {
  content_id: string;

  constructor(props: Props) {
    super(props);
    const param = queryString.parse(props.location.search);
    if (typeof(param.id) === "string") {
      this.content_id = param.id;
    } else {
      this.content_id = "";
    }
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(value: any) {
    console.log(value);
    const lines: string[] = value.text.split("\n");
    const events: object[] = [];
    const errorLines: string[] = [];
    const cid: number = Number(value.content_id);

    lines.forEach(line => {
      if (!line.trim()) {
        return;
      }
      let m = line.match(/^- \[(?<date>\d{4}-\d\d-\d\d)\] (?<u1>[a-z]+)\s*(?<d1>:[ox]:)\s*-\s*(?<d2>:[ox]:)\s*(?<u2>[a-z]+)\s*(?<data>.+)/);
      if (m && m.groups) {
        if (m.groups["d1"] === m.groups["d2"]) {
          errorLines.push(line);
          return;
        }
        events.push({
          content_id: cid,
          type: "duel",
          date: moment(m.groups["date"]),
          user1: m.groups["u1"],
          user2: m.groups["u2"],
          result: m.groups["d1"] === ":o:" && m.groups["d2"] === ":x:" ? 0 : 1,
          text: m.groups["data"],
        });
        return;
      }
      m = line.match(/^- system\((?<kind>.+)\):\[(?<date>\d{4}-\d\d-\d\d)\] (?<u>[a-z]+)/);
      if (m && m.groups) {
        events.push({
          content_id: cid,
          type: "system",
          date: moment(m.groups["date"]),
          username: m.groups["u"],
          kind: m.groups["kind"],
        });
        return;
      }
      errorLines.push(line);
    });
    (async () => {
      for (let e of events) {
        await API.POST("/events", e)
      }
      notification.success({
        message: "すべてインポートしました",
      });
    })();
    if (errorLines.length > 0) {
      notification.error({
        message: "一部のインポートに失敗しました",
        description: errorLines.join("\n"),
      });
      console.error(errorLines);
    }
  }
  render = () => {

    return <>
      <PageHeader title={<Typography.Title>インポート</Typography.Title>} />
      <Form
        onFinish={this.handleSubmit}
        >
        <Form.Item
          name="content_id"
          label="コンテンツID"
          rules={[{ required: true }]}
          initialValue={this.content_id}
          >
          <InputNumber />
        </Form.Item>
        <Form.Item
          name="text"
          label="データ"
          rules={[{ required: true }]}
          >
          <TextArea rows={10} />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">インポート</Button>
        </Form.Item>
      </Form>
    </>
  };
}

export default Import;
