package api

import (
	"crypto/rand"
	"database/sql"
	"encoding/base64"
	"log"
	"math"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	"example.com/m/model"
	"github.com/gin-gonic/gin"
	"github.com/line/line-bot-sdk-go/linebot"
)

// RegisterLineAPI はLINEに関わるAPIを登録します
func RegisterLineAPI(context *Context) {
	context.DBmap.AddTableWithName(model.UserLine{}, "users").SetKeys(false, "name")

	bot := context.LineBot
	apiGroup := context.API.Group("/line")
	apiGroupAuth := context.APIAuth.Group("/line")

	apiGroupAuth.POST("/link", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		helper.verifyUser()
		if helper.err != nil {
			return
		}
		// 4. nonceを生成してユーザーをLINEプラットフォームにリダイレクトする
		nonce := make([]byte, 64)
		if _, err := rand.Read(nonce); err != nil {
			setServerError(c, err)
			return
		}
		nonceStr := base64.StdEncoding.EncodeToString(nonce)
		key := newNonceKey(nonceStr)
		log.Println("create nonce:", key)

		// 揮発データとしてnonceとユーザーを紐付けて保存
		context.Component.Mutex.Lock()
		context.Component.Volatile[key] = helper.getMyName()
		context.Component.Mutex.Unlock()

		c.JSON(http.StatusOK, gin.H{
			"nonce": nonceStr,
		})
	})
	apiGroupAuth.DELETE("/link", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		helper.verifyUser()
		if helper.err != nil {
			return
		}

		name := helper.getMyName()

		var user model.UserLine
		helper.selectOne(&user, "SELECT name, line_uid FROM users WHERE name=$1", name)
		if user.LineID.Valid {
			user.LineID.Scan(nil)
			helper.update(&user)
		}
		if helper.err != nil {
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"status": "ok",
		})
	})

	apiGroup.Any("/hook", func(c *gin.Context) {
		events, err := bot.ParseRequest(c.Request)
		if err != nil {
			log.Println("parse error:", err)
			return
		}
		for _, e := range events {
			log.Println(e)
			err = nil
			switch e.Type {
			case linebot.EventTypeFollow:
				err = onLineAPIFollow(context, c, bot, e)
			case linebot.EventTypeMessage:
				err = onLineAPIMessage(
					&apiHelper{
						context: c,
						dbmap:   context.DBmap,
					},
					bot,
					e,
				)
			case linebot.EventTypeAccountLink:
				err = onLineAPIAccountLink(context, c, bot, e)
			case linebot.EventTypePostback:
				err = onLineAPIPostback(
					&apiHelper{
						context: c,
						dbmap:   context.DBmap,
					},
					bot,
					e,
				)
			}
			if err != nil {
				log.Println(err)
			}
		}
	})

	if os.Getenv("DEVELOP_MODE") == "1" {
		apiGroup.Any("/test", func(c *gin.Context) {
			helper := &apiHelper{
				context: c,
				dbmap:   context.DBmap,
			}
			w, _ := c.GetQuery("word")
			uid := os.Getenv("LINE_TEST_USER_ID")
			helper.begin()
			var card model.Card
			helper.selectOne(&card, "SELECT * FROM cards WHERE name=$1 LIMIT 1", w)
			helper.commit()
			if helper.err != nil {
				log.Println(helper.err)
				return
			}
			msg, err := newCardDetailMessage(card)
			// msg, err := newCardSearchResultMessage(
			// 	helper,
			// 	w,
			// )
			// msg, err := newRestCountMessage(
			// 	helper,
			// 	uid,
			// )
			if err != nil {
				log.Println(err)
				return
			}
			_, err = bot.PushMessage(uid, msg).Do()
			if err != nil {
				log.Println(err)
			}
		})
	}
}

func onLineAPIFollow(context *Context, c *gin.Context, bot *linebot.Client, e *linebot.Event) error {
	// 1. 連携トークンを発行する
	token, err := bot.IssueLinkToken(e.Source.UserID).Do()
	if err != nil {
		return err
	}

	// 2. ユーザーを連携URLにリダイレクトする
	msg := newAccountLinkMessage(
		c.Request.Host,
		token.LinkToken,
	)
	_, err = bot.ReplyMessage(e.ReplyToken, msg).Do()
	if err != nil {
		return err
	}

	return nil
}

func onLineAPIAccountLink(context *Context, c *gin.Context, bot *linebot.Client, e *linebot.Event) error {
	helper := apiHelper{
		context: c,
		dbmap:   context.DBmap,
	}

	nonce := e.AccountLink.Nonce
	key := newNonceKey(nonce)

	context.Component.Mutex.Lock()
	name, ok := context.Component.Volatile[key]
	context.Component.Mutex.Unlock()

	log.Println("link user", name)
	log.Println("nonce", key)

	if ok {
		// 5. アカウントを連携する
		if e.AccountLink.Result == linebot.AccountLinkResultOK {
			helper.selectStr("SELECT name FROM users WHERE name=$1", name)
			user := model.UserLine{
				Name: name,
				LineID: sql.NullString{
					String: e.Source.UserID,
					Valid:  true,
				},
			}
			helper.update(&user)
		}
		// もういらないので消す
		context.Component.Mutex.Lock()
		delete(context.Component.Volatile, key)
		context.Component.Mutex.Unlock()
	}
	if helper.err != nil {
		return helper.err
	}

	return nil
}

func onLineAPIMessage(helper *apiHelper, bot *linebot.Client, e *linebot.Event) error {

	switch message := e.Message.(type) {
	case *linebot.TextMessage:
		log.Println("message:" + message.Text)
		if message.Text == "連携" {
			// 1. 連携トークンを発行する
			token, err := bot.IssueLinkToken(e.Source.UserID).Do()
			if err != nil {
				return err
			}

			// 2. ユーザーを連携URLにリダイレクトする
			msg := newAccountLinkMessage(
				helper.context.Request.Host,
				token.LinkToken,
			)
			_, err = bot.PushMessage(e.Source.UserID, msg).Do()
			if err != nil {
				return err
			}
			return nil
		}
		if message.Text == "対戦可能数" {
			msg, err := newRestCountMessage(
				helper,
				e.Source.UserID,
			)
			if err != nil {
				return err
			}
			_, err = bot.ReplyMessage(e.ReplyToken, msg).Do()
			if err != nil {
				return err
			}
			return nil
		}
		if message.Text == "カード検索ヘルプ" {
			msg := linebot.NewTextMessage(
				"以下の投稿をここにすると簡易検索ができます\n" +
					"\n" +
					"カード名 <検索ワード>\n（ひらがなでも可）\n" +
					"レアリティ <神話レアやコモンなど>\n" +
					"タイプ <クリーチャーやソーサリーなど>\n" +
					"テキスト <検索ワード>\n" +
					"カードセット <アルファベットのコードまたは日本語>\n" +
					"\n" +
					"複数を組み合わせて指定することも可能です\n" +
					"例）レアリティ アンコモン タイプ クリーチャー",
			)
			_, err := bot.ReplyMessage(e.ReplyToken, msg).Do()
			if err != nil {
				return err
			}
			return nil
		}

		if strings.HasPrefix(message.Text, "カード名") ||
			strings.HasPrefix(message.Text, "タイプ") ||
			strings.HasPrefix(message.Text, "テキスト") ||
			strings.HasPrefix(message.Text, "レアリティ") ||
			strings.HasPrefix(message.Text, "カードセット") ||
			false {
			msg, err := newCardSearchResultMessage(
				helper,
				message.Text,
			)
			if err != nil {
				return err
			}
			_, err = bot.ReplyMessage(e.ReplyToken, msg).Do()
			if err != nil {
				return err
			}
			return nil
		}

		helper.begin()
		var card model.Card
		helper.selectOne(&card, "SELECT * FROM cards WHERE name=$1 LIMIT 1", message.Text)
		helper.commit()
		if helper.err == nil {
			msg, err := newCardDetailMessage(card)
			if err != nil {
				return err
			}
			_, err = bot.ReplyMessage(e.ReplyToken, msg).Do()
			if err != nil {
				return err
			}
			return nil
		}
	}
	return nil
}

func onLineAPIPostback(helper *apiHelper, bot *linebot.Client, e *linebot.Event) error {
	data := e.Postback.Data

	if strings.HasPrefix(data, "card_detail") {
		w := strings.Split(data, " ")
		if len(w) == 2 {
			helper.begin()
			var card model.Card
			helper.selectOne(&card, "SELECT * FROM cards WHERE key=$1", w[1])
			helper.commit()
			if helper.err != nil {
				return helper.err
			}
			msg, err := newCardDetailMessage(card)
			if err != nil {
				return err
			}
			_, err = bot.ReplyMessage(e.ReplyToken, msg).Do()
			if err != nil {
				return err
			}
			return nil
		}
	}
	return nil
}

// アカウント連携リンクメッセージを作成する
func newAccountLinkMessage(host string, linkToken string) *linebot.TemplateMessage {
	url := "https://" + host + "/integration/line?linkToken=" + linkToken
	action := linebot.NewURIAction("連携する", url)
	button := linebot.NewButtonsTemplate("", "アカウント連携", "リンクを開くとギャザ部アカウントと連携します", action)
	msg := linebot.NewTemplateMessage("アカウント連携", button)

	return msg
}

// 残り対戦可能数のメッセージを作成する
func newRestCountMessage(helper *apiHelper, userID string) (*linebot.FlexMessage, error) {

	var user model.UserLine
	helper.begin()
	helper.selectOne(&user, "SELECT name, line_uid FROM users WHERE line_uid=$1", userID)
	var content struct {
		ID      int `db:"id"`
		WeekMax int `db:"week_Max"`
	}
	helper.selectOne(&content, "SELECT id, week_max FROM contents WHERE start_date <= CURRENT_DATE AND end_date >= CURRENT_DATE ORDER BY start_date DESC LIMIT 1")

	var duelEvents []model.DuelEvent
	query := "SELECT events.id, type, cid, date, author, user1, user2, result, text, active " +
		"FROM events INNER JOIN duel_events ON events.eid = duel_events.id AND type = 'duel' AND cid = $1"
	helper.selectAny(&duelEvents, query, content.ID)
	helper.commit()
	if helper.err != nil {
		if content.ID == 0 {
			container := &linebot.BubbleContainer{
				Type: linebot.FlexContainerTypeBubble,
				Body: &linebot.BoxComponent{
					Layout: linebot.FlexBoxLayoutTypeVertical,
					Contents: []linebot.FlexComponent{
						&linebot.TextComponent{
							Text:   "残り対戦可能数",
							Size:   "xl",
							Weight: "bold",
						},
						&linebot.SeparatorComponent{
							Margin: "sm",
						},
						&linebot.TextComponent{
							Text:  "リーグ期間外",
							Size:  "sm",
							Color: "#666666",
						},
					},
				},
			}
			msg := linebot.NewFlexMessage("残り対戦可能数", container)
			return msg, nil
		}
		return nil, helper.err
	}

	activeUser := map[string]int{}
	_, nowWeek := time.Now().ISOWeek()

	for _, e := range duelEvents {
		var v int
		var ok bool

		_, w := e.Date.ISOWeek()
		if e.User1 == user.Name {
			v, ok = activeUser[e.User2]
			if !ok {
				v = 0
			}
			if w == nowWeek {
				v++
			}
			activeUser[e.User2] = v
		} else if e.User2 == user.Name {
			v, ok = activeUser[e.User1]
			if !ok {
				v = 0
			}
			if w == nowWeek {
				v++
			}
			activeUser[e.User1] = v
		} else {
			v, ok = activeUser[e.User1]
			if !ok {
				v = 0
			}
			activeUser[e.User1] = v
			v, ok = activeUser[e.User2]
			if !ok {
				v = 0
			}
			activeUser[e.User2] = v
		}
	}
	// 対戦数でソートする
	groups := map[int][]string{}
	for key, val := range activeUser {
		name := key
		rest := int(math.Max(float64(content.WeekMax-val), 0))
		v, ok := groups[rest]
		if !ok {
			v = []string{}
		}
		groups[rest] = append(v, name)
	}
	type Entry struct {
		rest  int
		names []string
	}
	var entries []Entry
	for key, val := range groups {
		entries = append(entries, Entry{
			rest:  key,
			names: val,
		})
	}
	sort.Slice(entries, func(i, j int) bool {
		return entries[i].rest > entries[j].rest
	})

	msgContents := []linebot.FlexComponent{
		&linebot.SeparatorComponent{
			Margin: "sm",
		},
	}
	for _, v := range entries {
		rest := v.rest
		names := v.names
		sort.Strings(names)
		one := 1
		five := 5
		msgContents = append(msgContents, &linebot.BoxComponent{
			Layout:  linebot.FlexBoxLayoutTypeBaseline,
			Spacing: "sm",
			Margin:  "sm",
			Contents: []linebot.FlexComponent{
				&linebot.TextComponent{
					Text:  strconv.Itoa(rest) + "戦",
					Size:  "sm",
					Color: "#aaaaaa",
					Flex:  &one,
				},
				&linebot.TextComponent{
					Text:  strings.Join(names, ", "),
					Wrap:  true,
					Size:  "sm",
					Color: "#666666",
					Flex:  &five,
				},
			},
		})
		msgContents = append(msgContents, &linebot.SeparatorComponent{
			Margin: "sm",
		})
	}

	container := &linebot.BubbleContainer{
		Type: linebot.FlexContainerTypeBubble,
		Body: &linebot.BoxComponent{
			Layout: linebot.FlexBoxLayoutTypeVertical,
			Contents: []linebot.FlexComponent{
				&linebot.TextComponent{
					Text:   "残り対戦可能数",
					Size:   "xl",
					Weight: "bold",
				},
				&linebot.BoxComponent{
					Layout:   linebot.FlexBoxLayoutTypeVertical,
					Contents: msgContents,
				},
			},
		},
	}
	msg := linebot.NewFlexMessage("残り対戦可能数", container)

	return msg, nil
}

// カード検索結果メッセージを作成する
func newCardSearchResultMessage(helper *apiHelper, search string) (linebot.SendingMessage, error) {
	var cards []struct {
		Key  string `db:"key"`
		Name string `db:"name"`
	}

	pattern := regexp.MustCompile(`(カード名|タイプ|テキスト|レアリティ|カードセット)[\s　]+([^\s　]+)`)
	res := pattern.FindAllStringSubmatch(search, -1)
	var words []interface{}
	var userQuery []string
	i := 1
	for _, r := range res {
		s := strconv.Itoa(i)
		switch r[1] {
		case "カード名":
			words = append(words, "%"+r[2]+"%")
			userQuery = append(userQuery, "(cards.name like $"+s+" OR name_kana like $"+s+")")
			i++
		case "タイプ":
			words = append(words, "%"+r[2]+"%")
			userQuery = append(userQuery, "cards.cardtype like $"+s+"")
			i++
		case "テキスト":
			words = append(words, "%"+r[2]+"%")
			userQuery = append(userQuery, "cards.text like $"+s+"")
			i++
		case "レアリティ":
			words = append(words, r[2])
			userQuery = append(userQuery, "cards.rarity = $"+s+"")
			i++
		case "カードセット":
			words = append(words, "%"+r[2]+"%")
			userQuery = append(userQuery, "(card_sets.code like $"+s+" OR card_sets.name like $"+s+")")
			i++
		}
	}
	if len(words) > 0 {
		helper.begin()
		query := "SELECT key, cards.name FROM " +
			"cards INNER JOIN card_sets ON cards.set = card_sets.code " +
			"WHERE " +
			strings.Join(userQuery, " AND ") + " AND cards.extra_img_url IS NULL " +
			"ORDER BY start_date DESC LIMIT 10"
		helper.selectAny(&cards, query, words...)
		helper.commit()
	}
	if helper.err != nil {
		return nil, helper.err
	}
	if len(cards) == 0 {
		msg := linebot.NewTextMessage("カードが見つかりませんでした")
		return msg, nil
	}
	contents := []*linebot.ImageCarouselColumn{}
	root := os.Getenv("ROOT_URL")
	for _, c := range cards {
		url := root + "/api/cards/" + c.Key + "/image"
		// LINE的にラベルは12文字制限があるのでそれ以下にする
		contents = append(contents, linebot.NewImageCarouselColumn(
			url,
			linebot.NewPostbackAction(string([]rune(c.Name)[:12]), "card_detail "+c.Key, "", ""),
		))
	}
	msg := linebot.NewTemplateMessage(
		"カード検索結果",
		linebot.NewImageCarouselTemplate(contents...),
	)
	return msg, nil
}

// カード詳細メッセージを作成する
func newCardDetailMessage(card model.Card) (linebot.SendingMessage, error) {
	cardType := card.CardType
	if card.SuperType != nil {
		cardType = *card.SuperType + cardType
	}
	if card.SubType != nil {
		cardType += " - " + *card.SubType
	}
	text := card.Text
	if card.Power != nil {
		text += "\n\n[" + *card.Power + "/" + *card.Toughness + "]"
	}
	if card.Loyalty != nil {
		text += "\n\n[" + *card.Loyalty + "]"
	}

	root := os.Getenv("ROOT_URL")
	container := &linebot.BubbleContainer{
		Type: linebot.FlexContainerTypeBubble,
		Header: &linebot.BoxComponent{
			Layout: linebot.FlexBoxLayoutTypeVertical,
			Contents: []linebot.FlexComponent{
				&linebot.TextComponent{
					Text: card.Name + " " + card.Cost,
				},
			},
		},
		Hero: &linebot.ImageComponent{
			URL: root + "/api/cards/" + card.Key + "/image",
		},
		Body: &linebot.BoxComponent{
			Layout: linebot.FlexBoxLayoutTypeVertical,
			Contents: []linebot.FlexComponent{
				&linebot.TextComponent{
					Text:  cardType,
					Size:  "sm",
					Color: "#666666",
				},
				&linebot.SeparatorComponent{
					Margin: "sm",
				},
				&linebot.TextComponent{
					Text: text,
					Size: "sm",
					Wrap: true,
				},
			},
		},
		Footer: &linebot.BoxComponent{
			Layout: linebot.FlexBoxLayoutTypeVertical,
			Contents: []linebot.FlexComponent{
				&linebot.ButtonComponent{
					Style: linebot.FlexButtonStyleTypeLink,
					Action: &linebot.URIAction{
						Label: "M:TG Wikiで検索",
						URI:   "http://mtgwiki.com/wiki/index.php?search=" + url.QueryEscape(card.Name),
					},
				},
			},
		},
	}
	msg := linebot.NewFlexMessage("カード詳細", container)

	return msg, nil
}

func newNonceKey(nonce string) string {
	return "line-nonce-" + nonce
}
