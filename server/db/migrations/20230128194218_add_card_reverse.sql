
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE cards ADD back_side_key text;


-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE cards DROP COLUMN back_side_key;

