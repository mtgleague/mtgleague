
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE decks ADD access_level text NOT NULL DEFAULT 'private';
UPDATE decks SET access_level = 'public' where is_public = 'true';
UPDATE decks SET access_level = 'private' where is_public = 'false';
ALTER TABLE decks DROP COLUMN is_public;

CREATE FUNCTION random_text() RETURNS text language sql as $$
  SELECT string_agg (substr('abcdefghijklmnopqrstuvwxyz0123456789', ceil (RANDOM() * 36)::integer, 1), '')
  FROM generate_series(1, 12)
$$;
ALTER TABLE decks ADD key text NOT NULL DEFAULT random_text();
CREATE UNIQUE INDEX key_index on decks(key);


-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE decks DROP COLUMN key;
DROP FUNCTION random_text();

ALTER TABLE decks ADD is_public boolean not null default false;
UPDATE decks SET is_public = 'true' where access_level = 'public';
UPDATE decks SET is_public = 'false' where access_level <> 'public';
ALTER TABLE decks DROP COLUMN access_level;

