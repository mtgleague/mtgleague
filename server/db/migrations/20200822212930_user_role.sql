
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE users ADD role text;
UPDATE users SET role = 'user';
ALTER TABLE users ALTER role SET NOT NULL;
ALTER TABLE users ALTER role SET DEFAULT 'user';

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE users DROP COLUMN role;
