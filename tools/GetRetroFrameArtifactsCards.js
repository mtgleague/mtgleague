const cheerio = require("cheerio");
const fs = require("fs");
const request = require('request');

const TargetNames = [
"石臼/Millstone",
"液鋼の塗膜/Liquimetal Coating",
"彩色の星/Chromatic Star",
"ジェイラム秘本/Jalum Tome",
"自己組立機械/Self-Assembler",
"清純のタリスマン/Pristine Talisman",
"象牙の塔/Ivory Tower",
"魂標ランタン/Soul-Guide Lantern",
"胆液の水源/Ichor Wellspring",
"鋳造所の検査官/Foundry Inspector",
"つややかな雄鹿/Burnished Hart",
"羽ばたき飛行機械/Ornithopter",
"バネ葉の太鼓/Springleaf Drum",
"速足のブーツ/Swiftfoot Boots",
"武勇の印章/Sigil of Valor",
"骨の鋸/Bone Saw",
"他所のフラスコ/Elsewhere Flask",
"ミシュラのガラクタ/Mishra's Bauble",
"アシュノッドの供犠台/Ashnod's Altar",
"アメジストのとげ/Thorn of Amethyst",
"失われし夢の井戸/Well of Lost Dreams",
"金粉の水蓮/Gilded Lotus",
"空虚への扉/Door to Nothingness",
"屑鉄さらい/Scrap Trawler",
"雲の鍵/Cloud Key",
"号泣の石/Keening Stone",
"鼓舞する彫像/Inspiring Statuary",
"ゴブリンの放火砲/Goblin Charbelcher",
"彩色の灯籠/Chromatic Lantern",
"再鍛の黒き剣/Blackblade Reforged",
"磁石のゴーレム/Lodestone Golem",
"弱者の剣/Sword of the Meek",
"順応する自動機械/Adaptive Automaton",
"精神の病を這うもの/Psychosis Crawler",
"精神迷わせの秘本/Mazemind Tome",
"先駆のゴーレム/Precursor Golem",
"ファイレクシアの破棄者/Phyrexian Revoker",
"吠えたける鉱山/Howling Mine",
"防御の光網/Defense Grid",
"彫り込み鋼/Sculpting Steel",
"巻き戻しの時計/Unwinding Clock",
"街の鍵/Key to the City",
"滅消の杭/Quietus Spike",
"旅行者の凧/Journeyer's Kite",
"類似の金床/Semblance Anvil",
"ルーン唱えの長槍/Runechanter's Pike",
"流転の護符/Quicksilver Amulet",
"霊体のヤギ角/Astral Cornucopia",
"威圧の杖/Staff of Domination",
"隔離するタイタン/Sundering Titan",
"かごの中の太陽/Caged Sun",
"危険な櫃/Perilous Vault",
"狂気の祭壇/Altar of Dementia",
"神秘の炉/Mystic Forge",
"催眠の宝珠/Mesmeric Orb",
"精神の眼/Mind's Eye",
"多勢の兜/Helm of the Host",
"ドラゴン・エンジン、レイモス/Ramos, Dragon Engine",
"白金の天使/Platinum Angel",
"ファイレクシアの処理装置/Phyrexian Processor",
"モックス・アンバー/Mox Amber",
"霊気貯蔵器/Aetherflux Reservoir",
"ワームとぐろエンジン/Wurmcoil Engine",
].map(s => s.split("/"));
const SetName = "BRR";

const parseCardlist = sets => {
    const path = `./download/${sets}.txt`;
    if (!fs.existsSync(path)) return "";
    // console.log("load " + sets);
    const text = fs.readFileSync(path).toString();
    // return text.split("\n\n\n");
    return text;
};
const cardlistText = parseCardlist(SetName);

function requestAsync(url, opt = {}) {
    return new Promise((resolve, reject) => {
        request(url, opt, (err, res, body) => {
            if (!err && res.statusCode == 200) {
                resolve(body);
            } else {
                reject(err);
            }
        });
    });
}
async function requestPage(url) {
    const body = await requestAsync(url);
    return cheerio.load(body);
}

async function getCard(id, imgObj) {
    const title = imgObj.attr("alt");
    console.log(`save ${title}...`);

    const cardPath = imgObj.attr("src");
    const cardName = `${SetName}_${("0000" + id).slice(-4)}`;

    // if (fs.existsSync(`./download/${cardName}.json`)) {
    //     return JSON.parse(fs.readFileSync(`./download/${cardName}.json`));
    // }

    if (!fs.existsSync(`./download/${cardName}.png`)) {
        request(cardPath, {encoding: null}, (err, res, body) => {
            fs.writeFileSync(
                `./download/${cardName}.png`,
                body,
                'binary');
        });
    }

    const name = TargetNames.find(name => name[0] === title);
    const $wikiPage = await requestPage(`http://mtgwiki.com/wiki/${encodeURIComponent(name[0])}/${name[1]}`);

    const text = $wikiPage(".card").text();
    const cost = text.match(/(\(.+?\))+/);
    let costs = cost[0].replace(/\)\(/g, "),(");
    costs = costs.split(",")
        .map(s => s
            .replace("白", "W")
            .replace("青", "U")
            .replace("黒", "B")
            .replace("赤", "R")
            .replace("緑", "G")
            .replace("Ｘ", "X")
            .replace(/[０-９]/g, n => String.fromCharCode(n.charCodeAt(0) - 0xFEE0))
            )
        .join(",");
    // アーティファクトだけ
    const type = text.match(/(?<super>伝説の)?アーティファクト\s*(?<c>クリーチャー)?(?:\s*—\s*(?<sub>[^\(\)]+\(.+?\)(?: [^\(\)]+\(.+?\))*))?/);
    const textJP = $wikiPage(".card p").html().split("<br />").map(e => $wikiPage(`<div>${e}</div>`).text());

    const results = [];
    const reality = $wikiPage.text().match(/カード個別評価：兄弟戦争旧枠版アーティファクト - (アンコモン|レア|神話レア)/)[1];
    const isCreature = type.groups["c"] && type.groups["sub"];

    const pt = isCreature ? text.match(/(?<p>[\d*+X]+)\/(?<t>[\d*+X]+)$/) : null;
    const res = {
        key: cardName,
        no: id,
        name: title,
        name_kana: "",
        set: SetName,
        cost: costs,
        power: pt?.groups["p"],
        toughness: pt?.groups["t"],
        loyalty: null,
        supertype: type.groups["super"] || null,
        cardtype: "アーティファクト" + (isCreature ? "・クリーチャー" : ""),
        subtype: type.groups["sub"]?.replace(/\(.+?\)/g, "").replace(/ /g, "・") || null,
        reality,
        text: textJP.join("\n"),
        flavor_text: "",
    };

    m = cardlistText.match(`日本語名：${title}（(.+?)）`);
    if (m) {
        res.name_kana = m[1];
    } else {
        throw "parse kana error";
    }

    console.log(res);

    results.push(res);

    fs.writeFileSync(`./download/${cardName}.json`, JSON.stringify(results));
    console.log(`complete ${title}.`);
    return results;
}

async function main() {
    const $ = await requestPage("https://magic.wizards.com/ja/news/card-image-gallery/the-brothers-war-variants");

    // Webから取得した時点ではimgタグで記述されている（その後jsで書き換えているっぽい）
    const list0 = Array.from($(".resizing-cig img"))
        .filter(e => TargetNames.some(name => name[0] === $(e).attr("alt")));
    const list = [];
    for (let item of list0) {
        if (!list.some(i => $(i).attr("alt") === $(item).attr("alt"))) {
            list.push(item);
        }
    }
    let i = 1;
    const results = [];
    for (let item of list) {
        const obj = await getCard(i, $(item));
        i++;
        results.push(obj);
    }
    fs.writeFileSync(`./listdata/${SetName}.json`, JSON.stringify(results));

    console.log("done.");
}

main();

//(async () => {
//    await getCard("https://mtg-jp.com/products/card-gallery/0000184/472967/");
//})();
