package ws

import (
	"log"

	"example.com/m/common"
	"github.com/gin-gonic/gin"
	"gopkg.in/olahol/melody.v1"
)

// RegisterSocket はソケットに関するコールバックを設定する
func RegisterSocket(comp *common.Component) {

	comp.Router.GET("/ws", func(c *gin.Context) {
		ck, _ := c.Cookie("JWTToken")
		comp.Melody.HandleRequestWithKeys(
			c.Writer,
			c.Request,
			gin.H{
				"Token": ck,
			},
		)
	})

	comp.Melody.HandleConnect(func(s *melody.Session) {
		log.Println("connect")
	})
	comp.Melody.HandleDisconnect(func(s *melody.Session) {
		log.Println("disconnect")
	})
	comp.Melody.HandleMessage(func(s *melody.Session, msg []byte) {
	})
}

type SocketHelper struct {
	Comp *common.Component
}

func (s *SocketHelper) Broadcast(msg []byte) {
	s.Comp.Mutex.Lock()
	s.Comp.Melody.Broadcast(msg)
	s.Comp.Mutex.Unlock()
}

func (s *SocketHelper) BroadcastOthers(msg []byte, c *gin.Context) {
	s.Comp.Mutex.Lock()
	s.Comp.Melody.BroadcastFilter(msg, func(s *melody.Session) bool {
		v, _ := c.Cookie("JWTToken")
		token, _ := s.Get("Token")
		return token.(string) != v
	})
	s.Comp.Mutex.Unlock()
}
