
type UserRole = "guest" | "user" | "organizer" | "appdeveloper" | "admin";
export interface User {
  name: string;
  role: UserRole;
  connect_line_id: boolean;
  isGuest: () => boolean;
  isAdmin: () => boolean;
  isOrganizer: () => boolean;
  isAppDeveloper: () => boolean;
  isPerson: (name: string) => boolean;
}

export function createUser(user: User | { name: string, role: UserRole, connect_line_id?: boolean }) {
  return {
    ...user,
    connect_line_id: user.connect_line_id || false,
    isGuest() { return this.role === "guest"; },
    isAdmin() { return this.role === "admin"; },
    isOrganizer() { return this.role === "organizer" || this.role === "admin"; },
    isAppDeveloper() { return this.role === "appdeveloper" || this.role === "admin"; },
    isPerson(name: string) {
      return this.name === name || this.role === "admin";
    },
  };
}
