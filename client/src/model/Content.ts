
export interface Content {
  id: number;
  title: string;
  start_date: string;
  end_date: string;
  target_sets: string;
  text: string;
}

export interface FileData {
  id: number;
  name: string;
  mime_type: string;
  content_id: number;
}

export interface CommentData {
  id: number;
  author: string;
  date: string;
  text: string;
  content_id: number;
  edit: boolean;
}
