import React, { Component } from 'react';
import SimpleMDE from 'react-simplemde-editor';
import { FileData } from 'model/Content';
import API from 'api/API';


interface MarkdownEditorProps {
  contentId?: number;
  value: string;
  onUploadFiles?: (files: FileData[]) => void;
  onChange: (value: string) => void;
  height?: string;
  toolbar?: boolean;
}

class MarkdownEditor extends Component<MarkdownEditorProps, any> {

  constructor(props: MarkdownEditorProps) {
    super(props);
    this.handleFileDrop = this.handleFileDrop.bind(this);
    this.handlePaste = this.handlePaste.bind(this);
  }

  uploadFileAndInsertMD(instance: CodeMirror.Editor, files: FileList) {
    if (!this.props.contentId) {
      return;
    }
    (async () => {
      const addedFiles: FileData[] = [];
      for (let f of Array.from(files)) {
        const formData = new FormData();
        formData.append("file", f);
        await API.POSTF(`/contents/${this.props.contentId}/files`, formData, e => {
          if (e.mime_type.match(/^image\//)) {
            instance.replaceSelection(`![${e.name}](/api/files/${e.id})`);
          } else {
            instance.replaceSelection(`[${e.name}](/api/files/${e.id})`);
          }
          addedFiles.push(e);
        });
      }
      if (this.props.onUploadFiles) {
        this.props.onUploadFiles(addedFiles);
      }
    })()
  }

  handleFileDrop(instance: CodeMirror.Editor, event: DragEvent) {
    const files = event.dataTransfer?.files;
    if (!files?.length) {
      return;
    }
    this.uploadFileAndInsertMD(instance, files);
  }
  handlePaste(instance: CodeMirror.Editor, event: ClipboardEvent) {
    const files = event.clipboardData?.files;
    if (!files?.length) {
      return;
    }
    this.uploadFileAndInsertMD(instance, files);
  }

  render() {
    // eventsの型は全部の要素を要求してくるのでanyにすることで特定のイベントだけをセットする
    const SimpleMDEEvents: any = {
      drop: this.handleFileDrop,
      paste: this.handlePaste,
    };

    return <SimpleMDE
      onChange={this.props.onChange}
      value={this.props.value}
      options={{
        spellChecker: false,
        minHeight: this.props.height,
        maxHeight: this.props.height || "70vh",
        toolbar: this.props.toolbar,
      }}
      events={SimpleMDEEvents}
    />
  }
}

export default MarkdownEditor;
