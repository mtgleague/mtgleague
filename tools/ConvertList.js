const fs = require("fs");

const list = fs.readdirSync("./download").filter(f => f.endsWith(".json"));

const parseCardlist = sets => {
    const path = `./download/${sets}.txt`;
    if (!fs.existsSync(path)) return [];
    // console.log("load " + sets);
    const text = fs.readFileSync(path).toString();
    return text.split("\n\n\n");
};
const parseDicData = sets => {
    let path = `./download/E_${sets}_JT_MS_J_2.txt`;
    if (!fs.existsSync(path)) {
        path = `./download/${sets}__J.txt`;
        if (!fs.existsSync(path)) {
            return {};
        }
    }
    const text = fs.readFileSync(path).toString();
    const res = {};
    text.split("\n").forEach(line => {
        if (!line) return;
        const v = line.split("\t");
        const val = v[0].slice(1).replace(/、/g, "");
        const key = v[1].slice(1, -1);
        if (!res[key] || res[key].length < val.length) {
            res[key] = val;
        }
    });
    return res;
};

let result = {};
let cardlistText = {};
const dicData = {};

list.forEach(file => {
    let objs = JSON.parse(fs.readFileSync(`./download/${file}`));
    if (!Array.isArray(objs)) objs = [ objs ];
    const sets = file.split("_")[0];
    const resobj = [];

    if (!cardlistText[sets]) {
        cardlistText[sets] = parseCardlist(sets);
        if (!cardlistText[sets].length) console.log(`card set ${sets} is empty.`);
        dicData[sets] = parseDicData(sets);
    }
    if (!cardlistText[sets].length) return;

    for (const obj of objs) {
        try {
            let cardname = obj.title.replace(/（.+?）|《|》/g, "");
            if (cardname.includes("//")) {
                // 分割カード
                cardname = cardname.split("//")[resobj.length].trim();
            }
            const res = {
                key: obj.cardName,
                no: null,
                name: cardname,
                name_kana: "",
                set: sets,
                cost: obj.cost.join(","),
                power: null,
                toughness: null,
                loyalty: null,
                supertype: null,
                cardtype: "",
                subtype: null,
                rarity: null,
                text: obj.textJP,
                flavor_text: obj.flavorText,
            };
            if (resobj.length == 0) {
                const cardText = cardlistText[sets].find(s => s.match(new RegExp(`日本語名：${cardname}(（.+）)?\n`)));
                if (!cardText) {
                    throw "not found";
                }
                let m;
                m = cardText.match(/^(\d+)\./);
                if (m) {
                    res.no = parseInt(m[1]);
                } else {
                    throw "parse no error";
                }
                res.rarity = cardText.match(/^　稀少度：(.+)/m)[1];
            } else {
                res.no = resobj[0].no;
                res.rarity = resobj[0].rarity;
            }
            // m = cardText.match(`日本語名：${cardname}（(.+?)）`);
            // if (m) {
            //     res.name_kana = m[1];
            // } else 
            if (dicData[sets] && dicData[sets][cardname]) {
                res.name_kana = dicData[sets][cardname];
            } else {
                throw "parse kana error";
            }
            const pt = obj.power;
            if (pt) {
                if (pt.includes("/")) {
                    const ptarr = pt.split("/");
                    res.power = ptarr[0].trim();
                    res.toughness = ptarr[1].trim();
                } else {
                    res.loyalty = pt.trim();
                }
            }
            m = obj.type.match(/^(?<super>(?:基本|伝説の|ワールド|氷雪|持続)+)?(?<type>[^― ]+)(\s*([― -]+)\s*(?<subtype>.+))?/);
            if (m) {
                m.groups["super"] && (res.supertype = m.groups["super"]);
                res.cardtype = m.groups["type"];
                m.groups["subtype"] && (res.subtype = m.groups["subtype"]);
            } else {
                throw "parse type error";
            }
            if (res.cardtype.match(/^[a-zA-Z]+$/)) {
                throw "not jp card";
            }
    
            resobj.push(res);
        } catch (e) {
            console.log(e + " " + obj.cardName  + " " + obj.title);
            return;
        }
    }
    if (!result[sets]) result[sets] = [];
    result[sets].push(resobj);
});

!fs.existsSync("./listdata") && fs.mkdirSync("./listdata");
for (const sets in result) {
    fs.writeFileSync(`./listdata/${sets}.json`, JSON.stringify(result[sets]));
}
