package api

import (
	"log"
	"net/http"
	"sort"
	"strconv"
	"strings"

	"example.com/m/model"
	jwt "github.com/appleboy/gin-jwt"
	"github.com/gin-gonic/gin"
)

// RegisterEventsAPI は`/events`以下のAPIを登録します
func RegisterEventsAPI(context *Context) {
	type eventNewActive struct {
		model.EventNew
		Active bool `db:"active"`
	}
	context.DBmap.AddTableWithName(model.DuelEventNew{}, "duel_events")
	context.DBmap.AddTableWithName(model.DuelEventUpdate{}, "duel_events")
	context.DBmap.AddTableWithName(model.SystemEventNew{}, "system_events")
	context.DBmap.AddTableWithName(model.SystemEventUpdate{}, "system_events")
	context.DBmap.AddTableWithName(model.EventNew{}, "events")
	context.DBmap.AddTableWithName(model.Event{}, "events")
	context.DBmap.AddTableWithName(eventNewActive{}, "events")

	context.APIAuth.GET("/events", func(c *gin.Context) {
		cidStr, _ := c.GetQuery("content_id")
		var systemEvents []model.SystemEvent
		var duelEvents []model.DuelEvent
		var query string

		if len(cidStr) > 0 {
			cid, err := strconv.Atoi(cidStr)
			if err != nil {
				setBadRequestResult(c, "invalid id.")
				return
			}
			query = "SELECT events.id, type, cid, date, author, username, kind, active " +
				"FROM events INNER JOIN system_events ON events.eid = system_events.id AND type = 'system' AND cid = $1"
			_, err = context.DBmap.Select(&systemEvents, query, cid)
			if err != nil {
				setServerError(c, err)
				return
			}
			query = "SELECT events.id, type, cid, date, author, user1, user2, result, text, active " +
				"FROM events INNER JOIN duel_events ON events.eid = duel_events.id AND type = 'duel' AND cid = $1"
			_, err = context.DBmap.Select(&duelEvents, query, cid)
			if err != nil {
				setServerError(c, err)
				return
			}
		} else {
			query = "SELECT events.id, type, cid, date, author, username, kind, active " +
				"FROM events INNER JOIN system_events ON events.eid = system_events.id AND type = 'system'"
			_, err := context.DBmap.Select(&systemEvents, query)
			if err != nil {
				setServerError(c, err)
				return
			}
			query = "SELECT events.id, type, cid, date, author, user1, user2, result, text, active " +
				"FROM events INNER JOIN duel_events ON events.eid = duel_events.id AND type = 'duel'"
			_, err = context.DBmap.Select(&duelEvents, query)
			if err != nil {
				setServerError(c, err)
				return
			}
		}
		events := make([]model.Event, len(systemEvents)+len(duelEvents))
		i := 0
		for _, v := range systemEvents {
			events[i] = model.Event{
				ID:        v.ID,
				EventNew:  v.EventNew,
				EventData: v.SystemEventNew,
				Active:    v.Active,
			}
			i++
		}
		for _, v := range duelEvents {
			events[i] = model.Event{
				ID:        v.ID,
				EventNew:  v.EventNew,
				EventData: v.DuelEventNew,
				Active:    v.Active,
			}
			i++
		}
		sort.SliceStable(events, func(i int, j int) bool {
			return events[i].ID < events[j].ID
		})
		c.JSON(http.StatusOK, events)
	})

	context.APIAuth.POST("/events", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
			comp:    context.Component,
		}

		helper.verifyUser()
		if helper.err != nil {
			return
		}

		var event model.EventAll
		if err := c.ShouldBindJSON(&event); err != nil {
			setBadRequestResult(c, err.Error())
			return
		}
		clamis := jwt.ExtractClaims(c)
		event.Author = clamis["id"].(string)

		switch event.Type {
		case "duel":
			duelEvent := event.DuelEventNew
			if len(duelEvent.User1) == 0 || len(duelEvent.User2) == 0 || len(duelEvent.Text) == 0 {
				setBadRequestResult(c, "requied parameter is empty.")
				return
			}
			if duelEvent.User1 == duelEvent.User2 {
				setBadRequestResult(c, "same users duel.")
				return
			}
			if duelEvent.Result != 0 && duelEvent.Result != 1 {
				setBadRequestResult(c, "invalid result.")
				return
			}
			log.Print(duelEvent)
			helper.begin()
			max := helper.selectInt("SELECT week_max FROM contents WHERE id=$1", event.ContentID)
			isActive := true
			// 同一週に一定数以上で無効フラグを立てる
			{
				query := "SELECT count(*) " +
					"FROM events INNER JOIN duel_events ON " +
					"events.eid = duel_events.id AND type = 'duel' AND cid = $1 " +
					"AND ((user1 = $2 AND user2 = $3) OR (user1 = $3 AND user2 = $2)) " +
					"AND EXTRACT(WEEK FROM date) = $4"

				_, w := event.Date.ISOWeek()
				count := helper.selectInt(
					query,
					event.ContentID,
					duelEvent.User1,
					duelEvent.User2,
					w,
				)
				if count >= max {
					isActive = false
				}
			}

			helper.insert(&duelEvent)
			id := helper.getLastValue()
			var commonEvent eventNewActive
			commonEvent.EventNew = event.EventNew
			commonEvent.EventID = id
			commonEvent.Active = isActive
			helper.insert(&commonEvent)
			id = helper.getLastValue()
			helper.commit()
			if helper.err != nil {
				return
			}
			result := model.Event{
				ID:        int(id),
				EventNew:  event.EventNew,
				EventData: duelEvent,
				Active:    isActive,
			}
			helper.broadcast("add_event", result)
			c.JSON(http.StatusOK, result)
			break
		case "system":
			systemEvent := event.SystemEventNew
			if len(systemEvent.UserName) == 0 {
				setBadRequestResult(c, "requied parameter is empty.")
				return
			}
			isValidKind := false
			if systemEvent.Kind == "add" || systemEvent.Kind == "reset" {
				isValidKind = true
			}
			if systemEvent.Kind == "distribution" && helper.isRoleOrganizer() {
				isValidKind = true
			}
			if !isValidKind {
				setBadRequestResult(c, "invalid kind.")
				return
			}
			helper.begin()
			helper.insert(&systemEvent)
			id := helper.getLastValue()
			commonEvent := event.EventNew
			commonEvent.EventID = int(id)
			helper.insert(&commonEvent)
			id = helper.getLastValue()
			helper.commit()
			if helper.err != nil {
				return
			}
			result := model.Event{
				ID:        int(id),
				EventNew:  event.EventNew,
				EventData: systemEvent,
				Active:    true,
			}
			helper.broadcast("add_event", result)
			c.JSON(http.StatusOK, result)

		default:
			setBadRequestResult(c, "unknown event type.")
			return
		}
	})

	context.APIAuth.PUT("/events/:id", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
			comp:    context.Component,
		}

		helper.verifyUser()
		if helper.err != nil {
			return
		}

		id := helper.getID()

		var event model.EventAll
		if err := c.ShouldBindJSON(&event); err != nil {
			setBadRequestResult(c, err.Error())
			return
		}
		event.EventID = id
		var result model.Event
		helper.begin()
		helper.selectOne(&result, "SELECT * from events WHERE id=$1", id)

		// 日付を上書きして更新
		result.Date = event.Date
		helper.update(&result)

		switch result.Type {
		case "duel":
			helper.update(&model.DuelEventUpdate{
				ID:           result.EventID,
				DuelEventNew: event.DuelEventNew,
			})
			result.EventData = event.DuelEventNew
			break
		case "system":
			helper.update(&model.SystemEventUpdate{
				ID:             result.EventID,
				SystemEventNew: event.SystemEventNew,
			})
			result.EventData = event.SystemEventNew
			break
		default:
			helper.setBadRequestResult("unknown event type.")
			return
		}
		helper.commit()
		// エラーが出てたら終わり
		if helper.err != nil {
			return
		}
		helper.broadcast("edit_event", result)
		c.JSON(http.StatusOK, result)
	})

	context.APIAuth.DELETE("/events/:id", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
			comp:    context.Component,
		}

		helper.verifyUser()
		if helper.err != nil {
			return
		}

		id := helper.getID()

		var result model.Event
		helper.begin()
		helper.selectOne(&result, "SELECT * from events WHERE id=$1", id)

		switch result.Type {
		case "duel":
			helper.delete(&model.DuelEventUpdate{
				ID: result.EventID,
			})
			break
		case "system":
			helper.delete(&model.SystemEventUpdate{
				ID: result.EventID,
			})
			break
		default:
			helper.setBadRequestResult("unknown event type.")
			return
		}
		helper.delete(&result)
		helper.commit()
		// エラーが出てたら終わり
		if helper.err != nil {
			return
		}
		helper.broadcast("delete_event", result)
		c.JSON(http.StatusOK, gin.H{
			"status": "OK",
		})
	})

	context.APIAuth.GET("/events/search", func(c *gin.Context) {
		queryText, _ := c.GetQuery("text")
		var duelEvents []model.DuelEvent
		var query string

		if len(queryText) > 0 {
			texts := strings.Split(queryText, " ")
			query = "SELECT events.id, type, cid, date, author, user1, user2, result, text, active " +
				"FROM events INNER JOIN duel_events ON events.eid = duel_events.id AND type = 'duel'"
			queryArgs := make([]interface{}, len(texts))
			for i, v := range texts {
				x := "$" + strconv.Itoa(i+1)
				if strings.HasPrefix(v, "user:") {
					query += " AND (user1 = " + x + " OR user2 = " + x + ")"
					queryArgs[i] = v[5:]
				} else if strings.HasPrefix(v, "since:") {
					query += " AND date >= " + x
					queryArgs[i] = v[6:]
				} else if strings.HasPrefix(v, "until:") {
					query += " AND date <= " + x
					queryArgs[i] = v[6:]
				} else {
					query += " AND text LIKE " + x
					queryArgs[i] = "%" + v + "%"
				}
			}

			_, err := context.DBmap.Select(&duelEvents, query, queryArgs...)
			if err != nil {
				setServerError(c, err)
				return
			}
		}
		events := make([]model.Event, len(duelEvents))
		i := 0
		for _, v := range duelEvents {
			events[i] = model.Event{
				ID:        v.ID,
				EventNew:  v.EventNew,
				EventData: v.DuelEventNew,
				Active:    v.Active,
			}
			i++
		}
		// 日付で降順にソート
		sort.SliceStable(events, func(i int, j int) bool {
			return events[i].Date.Unix() > events[j].Date.Unix()
		})
		c.JSON(http.StatusOK, events)
	})

}
