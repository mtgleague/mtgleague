import React from 'react';
import { Button, notification, Typography, PageHeader, Descriptions, Image, Modal } from 'antd';
import API from 'api/API';
import queryString from 'query-string';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

const { confirm } = Modal;

const IntegrationService = (props: any) => {

  if (props.id === "line") {
    const param = queryString.parse(props.location.search);
    const token = param.linkToken;
    if (token) {
      API.POST("/line/link", {
        linkToken: token
      }, e => {
        // エスケープしないと+が化ける
        const nonce = encodeURIComponent(e.nonce);
        // 4. nonceを生成してユーザーをLINEプラットフォームにリダイレクトする
        const url = `https://access.line.me/dialog/bot/accountLink?linkToken=${token}&nonce=${nonce}`;
        window.location.href = url;
      });
      return <div>LINE 連携中・・・</div>;
    }
  }

  const handleUnlinkLine = () => {
    confirm({
      title: "LINE連携を解除しますか？",
      onOk() {
        API.DELETE("/line/link", () => {
          notification.success({
            message: '連携を解除しました',
          });
          props.onUnlink("line");
        });
      },
    });
  };

  return <>
    <PageHeader title={<Typography.Title>外部サービス</Typography.Title>} />
    <Typography.Title level={2}>LINE</Typography.Title>

    <Descriptions title="連携状況" bordered>
      <Descriptions.Item label="ステータス">
        {props.me.connect_line_id
         ? <div>
            連携済み
            <Button style={{ marginLeft: 20 }} type="dashed" onClick={handleUnlinkLine} ghost danger>
              連携解除する
            </Button>
          </div>
         : "未連携"}
      </Descriptions.Item>
    </Descriptions>

    <Typography.Title level={3}>連携方法</Typography.Title>
    <Typography.Paragraph>以下のボタンからLINE友だちを追加します。</Typography.Paragraph>
    <Typography.Paragraph>
      <Button type="link" href="https://lin.ee/TDghWdE">
        <img
          src="https://scdn.line-apps.com/n/line_add_friends/btn/ja.png"
          alt="友だち追加"
          height="36"
        />
      </Button>
    </Typography.Paragraph>
    <Typography.Paragraph>
      追加をすると連携リンクが通知されるので「連携する」を選びます。<br />
      サイトに移動するのでログインします。<br />
      正常に連携できると画面が切り替わります。<br />
      <Image
          src="/img/line-link-1.jpg"
          alt="line-link-1"
          width={200}
          style={{ margin: 10 }}
        />
      <FontAwesomeIcon icon={faArrowRight} />
      <Image
          src="/img/line-link-2.jpg"
          alt="line-link-2"
          width={200}
          style={{ margin: 10 }}
        />
      <FontAwesomeIcon icon={faArrowRight} />
      <Image
          src="/img/line-link-3.jpg"
          alt="line-link-3"
          width={200}
          style={{ margin: 10 }}
        />
    </Typography.Paragraph>
    <Typography.Title level={3}>連携解除方法</Typography.Title>
    <Typography.Paragraph>このページ上部の「連携解除する」のボタンから解除します。</Typography.Paragraph>
    <Image
          src="/img/line-unlink-1.jpg"
          alt="line-unlink-1"
          width={200}
          style={{ margin: 10, border: "gray 1px solid" }}
        />
    <Typography.Title level={3}>再連携方法</Typography.Title>
    <Typography.Paragraph>LINEで「連携」と発言することで連携リンクが通知されます。</Typography.Paragraph>
  </>
}

export default IntegrationService;
