-- UPDATE cards SET back_side_key = NULL WHERE back_side_key <> null;

-- 基本的な両面カード
UPDATE cards AS c1
  SET back_side_key = c2.key
  FROM cards AS c2
  WHERE (c1.text LIKE '%変身%' OR c1.text LIKE '%降霊%' OR c1.text LIKE '%合体させる%' OR c1.text LIKE '%で作製%')
    AND c1."no"=c2."no" AND c1.set=c2.set AND c1.key<>c2.key;

-- 日暮・夜明
UPDATE cards AS c1
  SET back_side_key = c2.key
  FROM cards AS c2
  WHERE c1.text LIKE '%日暮%' AND c2.text LIKE '%夜明%'
    and c1."no"=c2."no" AND c1.set=c2.set AND c1.key<>c2.key;

-- モード持ちの両面カード
UPDATE cards AS c1
  SET back_side_key = c2.key
  FROM cards AS c2
  WHERE c1.back_side_key is null and c1."no"=c2."no" AND c1.set=c2.set AND c1.name<>c2.name
    AND (c1.set = 'ZNR' OR c1.set ='KHM'OR c1.set ='STX' OR c1.set ='MOM');

-- 片面だけ登録されてるものを双方向に設定
UPDATE cards AS c1
  SET back_side_key = c2.key
  FROM cards AS c2
  WHERE c1.back_side_key is null and c1."no"=c2."no" AND c1.set=c2.set AND c1.key = c2.back_side_key;

-- 両面カード確認
SELECT c1.KEY,c1.NO,c1.NAME,c1.set,c1.back_side_key,c2."key" AS okey,c2.name AS oname,c2.back_side_key AS obkey
 FROM cards AS c1 JOIN cards AS c2 ON c1."no"=c2."no" AND c1.set=c2.set AND c1.key<>c2.key AND c1.name<>c2.name
 WHERE c1.back_side_key IS NOT null;

-- 両面カード未設定確認
SELECT c1.KEY,c1.NO,c1.NAME,c1.set,c1.back_side_key,c2."key" AS okey,c2.name AS oname,c2.back_side_key AS obkey
 FROM cards AS c1 JOIN cards AS c2 ON c1."no"=c2."no" AND c1.set=c2.set AND c1.key<>c2.key AND c1.name<>c2.name
 AND ((c1.text LIKE '%変身%'  OR c1.text LIKE '%降霊%' OR c1.text LIKE '%合体させる%' OR c1.text LIKE '%で作製%')
  OR (c1.text LIKE '%日暮%' AND c2.text LIKE '%夜明%'))
 WHERE c1.back_side_key IS null;

-- モード持ちの両面カード確認（要セット指定）
select c1.KEY,c1.NO,c1.NAME,c1.set,c1.back_side_key,c2."key" AS okey,c2.name AS oname,c2.back_side_key AS obkey
  FROM cards AS c1 JOIN cards AS c2
  ON c1.back_side_key is null and c1."no"=c2."no" AND c1.set=c2.set AND c1.name<>c2.name
  AND (c1.set = 'ZNR' OR c1.set ='KHM'OR c1.set ='STX' OR c1.set ='MOM');

-----------------------------------------------------------------
-- 出来事の設定
UPDATE cards AS c1 SET other_keys = c2.key
  FROM cards AS c2
  WHERE c1.other_keys is null and c1."no"=c2."no" AND c1.set=c2.set AND c1.name<>c2.name AND (c2.subtype='出来事' or c1.subtype='出来事');

-- 出来事カード未設定確認
SELECT c1.KEY,c1.NO,c1.NAME,c1.set,c1.other_keys,c2."key" AS okey,c2.name AS oname,c2.other_keys AS ookey
 FROM cards AS c1 JOIN cards AS c2 ON c1."no"=c2."no" AND c1.set=c2.set AND c1.key<>c2.key AND c1.name<>c2.name
 WHERE c1.other_keys is null AND (c2.subtype = '出来事' or c1.subtype = '出来事');

-- 出来事カード確認
SELECT c1.KEY,c1.NO,c1.NAME,c1.set,c1.other_keys,c2."key" AS okey,c2.name AS oname,c2.other_keys AS ookey
 FROM cards AS c1 JOIN cards AS c2 ON c1."no"=c2."no" AND c1.set=c2.set AND c1.key<>c2.key AND c1.name<>c2.name
 WHERE (c2.subtype = '出来事' or c1.subtype = '出来事');

-----------------------------------------------------------------
-- 分割カード設定
UPDATE cards AS c1 SET other_keys = c2.key
  FROM cards AS c2
  WHERE c1.other_keys is null and c1."no"=c2."no" AND c1.set=c2.set AND c1.name<>c2.name
    AND (c1.set = 'RNA' OR c1.set = 'GRN' OR c1.set = 'HOU' OR c1.set='AKH' OR c1.set='MKM');

-- 分割カード未設定確認
SELECT c1.KEY,c1.NO,c1.NAME,c1.set,c1.other_keys,c2."key" AS okey,c2.name AS oname,c2.other_keys AS ookey
 FROM cards AS c1 JOIN cards AS c2 ON c1."no"=c2."no" AND c1.set=c2.set AND c1.key<>c2.key AND c1.name<>c2.name
 WHERE c1.other_keys IS NULL AND (c1.set = 'RNA' OR c1.set='GRN' OR c1.set='HOU' OR c1.set='AKH' OR c1.set='MKM');
