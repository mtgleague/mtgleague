import React, { useEffect, useState } from "react";
import { Popover, Tag } from "antd";
import { CardSet } from "model/Card";
import API from "api/API";

const PopContent = (props: { sets: CardSet[] | null, value: string, children: React.ReactElement }) => {
  const { sets, value } = props;
  if (!sets) return <div>{props.children}</div>
  const set = sets.find(s => s.code === value);
  if (!set) return <Popover content="未登録カードセット">{props.children}</Popover>;

  return <Popover title={set.name} content={
    <ul>
      <li><a href={`http://whisper.wisdom-guild.net/apps/autodic/?set=${set.code}`} target="_blank" rel="noopener noreferrer">カード名辞書</a></li>
      <li><a href={`http://mtgwiki.com/wiki/%E3%82%AB%E3%83%BC%E3%83%89%E5%80%8B%E5%88%A5%E8%A9%95%E4%BE%A1%EF%BC%9A${set.name}`} target="_blank" rel="noopener noreferrer">カード個別評価(M:TG Wiki)</a></li>
      {set.release_note && <li>
        <a href={set.release_note} target="_blank" rel="noopener noreferrer">
          リリースノート{set.release_note.endsWith(".pdf") ? "(PDF)" : ""}
        </a>
      </li>}
    </ul>
  }>{props.children}</Popover>;
};

const CardSetTags = (props: { value: string, cardSets?: CardSet[] }) => {
  const list = props.value ? props.value.split(",") : [];
  const [sets, setSets] = useState<CardSet[] | null>(props.cardSets || null);

  useEffect(() => {
    if (!sets) {
      API.GET("/card_sets", sets => setSets(sets));
    }
  }, [sets]);

  return <div>
    {list.map(t => <PopContent key={t} sets={sets} value={t}>
      <Tag color="purple">{t}</Tag>
    </PopContent>)}
  </div>;
}

export default CardSetTags;
