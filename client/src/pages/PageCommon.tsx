import React, { useCallback, useEffect, useState } from 'react';
import { Button, Dropdown, MenuProps, notification } from "antd";
import { EllipsisOutlined } from '@ant-design/icons';
import API from 'api/API';
import MarkdownContent from './league/MarkdownContent';

export const DropdownMenu = (props: {
  menu: MenuProps
}) => {
  return <Dropdown
  menu={props.menu}
  placement="bottomRight"
  trigger={[ "click" ]}
  >
  <Button
    style={{
      border: 'none',
      padding: 0,
      backgroundColor: "transparent",
    }}
  >
    <EllipsisOutlined
      style={{
        fontSize: 24,
        verticalAlign: 'top',
      }}
    />
  </Button>
</Dropdown>;
}


export const SimpleMarkdownText = (props: {
  isEdit: boolean,
  pageName: string,
  onSaved: () => void,
}) => {
  const [content, setContent] = useState("");

  useEffect(() => {
    API.GET(`/page/${props.pageName}`, res => {
      setContent(res.text);
    });
  }, [props.pageName]);

  const handleSave = useCallback(() => {
    API.PUT(`/page/${props.pageName}`, {
      text: content,
    }, e => {
      notification.success({
        message: "保存しました"
      });
      props.onSaved();
    });
  }, [content, props]);

  return <>
    <MarkdownContent
      content={content}
      isEdit={props.isEdit}
      onChange={setContent}
    />
    {props.isEdit && <Button
      onClick={handleSave}
      type="primary"
      >
      保存
    </Button>}
  </>;
}
