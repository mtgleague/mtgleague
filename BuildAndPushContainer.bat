@echo off

set tag=%~1
if "%tag%"=="" set /p tag=tag?(ex:1.10):

set reg_root=registry.gitlab.com
set image_name=%reg_root%/mtgleague/mtgleague
set build_opt=-t %image_name%

if "%tag%"=="dev" set build_opt=
if not "%tag%"=="" set build_opt=%build_opt% -t %image_name%:%tag%

docker login %reg_root%
docker build %build_opt% .
if not "%tag%"=="dev" docker push %image_name%:latest
if not "%tag%"=="" docker push %image_name%:%tag%

if not "%tag%"=="dev" if not "%tag%"=="" git tag v%tag%
if not "%tag%"=="dev" if not "%tag%"=="" (
  git push
  git push --tags
)

pause
