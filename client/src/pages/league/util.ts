import moment from 'moment';

export const SameWeek = (d1: moment.Moment, d2: moment.Moment) => {
  return d1.year() === d2.year() && d1.isoWeek() === d2.isoWeek();
}

export const SameWeekYear = (d1: moment.Moment, year: number, week: number) => {
  return d1.year() === year && d1.isoWeek() === week;
}
