import React, { Component } from 'react';
import {
  Button,
  Typography,
  List,
  Popconfirm,
} from 'antd';
import {
  DeleteOutlined,
} from '@ant-design/icons';
import { FileData } from 'model/Content';
import API from 'api/API';


class FileListView extends Component<{
  files: FileData[],
  onDelete: (item: FileData) => void,
}, any> {

  constructor(props: any) {
    super(props);
    this.handleFileDelete = this.handleFileDelete.bind(this);
  }

  handleFileDelete(item: FileData) {
    API.DELETE(`/files/${item.id}`, _ => {
      this.props.onDelete(item);
    });
  }

  render = () => {
    const itemStyle = {
      padding: 0,
      margin: 0,
    };
    const buttonStyle = {
      marginLeft: 5,
      padding: 0

    };
    if (this.props.files.length === 0) return <></>;

    return (
      <>
      <span style={{ fontWeight: 600 }}>添付ファイル</span>
      <List
        size="small"
        rowKey="id"
        dataSource={this.props.files}
        grid={{
          column: 4,
        }}
        renderItem={item => (<List.Item style={itemStyle}>
          <Typography.Link href={`/api/files/${item.id}`}>{item.name}</Typography.Link>
          <Popconfirm
              placement="top"
              title="ファイルを削除しますか？"
              onConfirm={() => this.handleFileDelete(item)}
              okText="削除"
              okType="danger"
              cancelText="キャンセル"
            >
            <Button
              type="link"
              danger={true}
              style={buttonStyle}
              >
              <DeleteOutlined />
            </Button>
          </Popconfirm>
        </List.Item>)}
        />
    </>
    )
  }
}
export default FileListView;
