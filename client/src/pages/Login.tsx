import React, { Component } from 'react';
import { Form, Input, Button, Typography } from 'antd';
import { Store } from 'antd/lib/form/interface';
import { RouteChildrenProps } from 'react-router-dom';
import API from 'api/API';
import queryString from 'query-string';

interface Props extends RouteChildrenProps {
}
interface State {
  result: string,
}

const layout = {
  labelCol: { span: 9 },
  wrapperCol: { span: 8 },
  style: {
    width: "100%",
  },
};
const tailLayout = {
  wrapperCol: { offset: 9, span: 12 },
};

class Login extends Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      result: "",
    };
    this.onFinish = this.onFinish.bind(this);
  }

  componentDidMount() {
    // ログイン済みなら対象ページに飛ばす
    // ここでAPI.GETを使うとリダイレクトループするので直接fetchする
    fetch("/api/me")
      .then(e => {
        if (e.ok) {
          this.redirectToTarget();
        }
      });
  }

  onFinish(values: Store) {
    API.POST("/users/login", {
      name: values.username,
      password: values.password,
    }, _ => {
      console.log("login.");
      this.redirectToTarget();
    });
  }

  redirectToTarget() {
    const param = queryString.parse(this.props.location.search);
    const url = param.url as string;
    if (url) {
      this.props.history.push(url);
    } else {
      this.props.history.push("/");
    }
  }

  render = () => (
    <div style={{
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      height: "100vh",
    }}>
      <Form
        {...layout}
        onFinish={this.onFinish}
      >
        <Form.Item {...tailLayout}>
          <Typography.Paragraph>このサイトはログインが必要です</Typography.Paragraph>
        </Form.Item>
        <Form.Item
          label="ユーザー名"
          name="username"
          rules={[{ required: true, message: 'ユーザー名を入力してください' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="パスワード"
          name="password"
          rules={[{ required: true, message: 'パスワードを入力してください' }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            ログイン
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}

export default Login;
