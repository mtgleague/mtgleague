import React, { useState } from 'react';
import {
  Button,
  Modal,
  Input
} from 'antd';
import {
  CopyOutlined
} from '@ant-design/icons';
import style from 'assets/Decklist.module.css';


const CloneButton = (props: { name: string, onClone: (name: string)=>void }) => {

  const [visible, setVisible] = useState(false);
  const [value, setValue] = useState(props.name);
  const onOK = () => {
    props.onClone(value);
    setVisible(false);
  };
  const onCancel = () => {
    setVisible(false);
  };
  const onOpen = () => {
    setValue(props.name);
    setVisible(true);
  };

  return <>
    <Button
      className={style["new-deck-button"]}
      onClick={onOpen}
      icon={<CopyOutlined />}
      >
        複製
      </Button>
    <Modal
      visible={visible}
      onOk={onOK}
      okButtonProps={{
        disabled: !value,
      }}
      onCancel={onCancel}
      okText="複製"
      title="新しいデッキ"
      centered
      >
        名前：<Input value={value} onChange={e => setValue(e.target.value)} />
      </Modal>
  </>;
};

export default CloneButton;
