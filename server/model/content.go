package model

// Content は取得時のコンテントの型
type Content struct {
	ID int `json:"id" db:"id, primarykey, autoincrement"`
	ContentNew
}

// ContentNew は作成時のコンテントの型
type ContentNew struct {
	Title      string    `json:"title" db:"title"`
	StartDate  LocalDate `json:"start_date" db:"start_date"`
	EndDate    LocalDate `json:"end_date" db:"end_date"`
	TargetSets string    `json:"target_sets" db:"target_sets"`
	WeekMax    int       `json:"week_max" db:"week_max"`
	Text       string    `json:"text" db:"text"`
}

// FileNew は作成時のファイルの型
type FileNew struct {
	Name      string `json:"name" db:"name"`
	MIMEType  string `json:"mime_type" db:"type"`
	Data      []byte `json:"-" db:"data"`
	ContentID int    `json:"content_id" db:"cid"`
}

// File は取得時のファイルの型
type File struct {
	ID int `json:"id" db:"id, primarykey, autoincrement"`
	FileNew
}

// CommentNew は作成時のコメントの型
type CommentNew struct {
	Author    string        `json:"author" db:"author"`
	Date      LocalDateTime `json:"date" db:"date"`
	Text      string        `json:"text" db:"text"`
	ContentID int           `json:"content_id" db:"cid"`
	Parent    *int          `json:"parent" db:"parent"`
}

// Comment は取得時のコメントの型
type Comment struct {
	ID   int  `json:"id" db:"id, primarykey, autoincrement"`
	Edit bool `json:"edit" db:"edit"`
	CommentNew
}
