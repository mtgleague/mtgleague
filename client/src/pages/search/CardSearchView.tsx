import React, { useCallback, useEffect, useState } from "react";
import { Input, PageHeader, Pagination, Space, Typography } from "antd";
import API from "api/API";
import queryString from 'query-string';
import { useHistory, useLocation } from "react-router";
import { CardResult } from "model/Card";
import Cardlist from "pages/cardlist/Cardlist";
import { CardCache, CardCacheContext } from "pages/cardlist/CardCacheContext";

const { Search } = Input;

const CardSearchView = (props: any) => {
  const [result, setResult] = useState<CardResult | null>();
  const [searching, setSearching] = useState(false);
  const [errorText, setErrorText] = useState("");
  const [cardCache, setCardCache] = useState<CardCache>({});
  const history = useHistory();
  const location = useLocation();
  const param = queryString.parse(location.search);
  const query = param.q as string;
  const page = Number(param.page) || 1;

  useEffect(() => {
    if (query) {
      setSearching(true);
      setErrorText("");
      const apiQuery: any = {
        text: "",
        page: page,
        limit: 100,
      };
      const check = (word: string, prefix: string, key: string) => {
        if (word.startsWith(prefix)) {
          apiQuery[key] = apiQuery[key] ? apiQuery[key] + " " : "";
          apiQuery[key] += word.substring(prefix.length);
          return true;
        }
        return false;
      };
      query.split(" ").forEach(w => {
        if (false
          || check(w, "set:", "sets")
          || check(w, "cardtype:", "cardtype")
          || check(w, "subtype:", "subtype")
          || check(w, "supertype:", "supertype")
          || check(w, "rarity:", "rarity")
          ) {
        } else {
          if (apiQuery["text"]) {
            apiQuery["text"] += " ";
          }
          apiQuery["text"] += w;
        }
      });
      API.GET(`/cards?${queryString.stringify(apiQuery)}`, res => {
        setResult(res);
        setSearching(false);
      }, err => {
        setErrorText("検索に失敗しました。クエリが正しいか確認してください。");
        // エラー時は検索を解除するだけ
        setSearching(false);
      });
    } else {
      setResult(null);
    }
  }, [query, page]);
  const handleSearch = useCallback((value: string) => {
    history.push(`?q=${encodeURIComponent(value)}`);
  }, [history]);
  const handleChangePage = useCallback((page: number) => {
    history.push(`?q=${encodeURIComponent(query)}&page=${page}`);
  }, [history, query]);

  const PageMax = 100;
  const Pager = () => {
    return result && result.total > PageMax ? <Pagination
      current={page}
      onChange={handleChangePage}
      total={result.total}
      pageSize={PageMax}
      showSizeChanger={false}
      /> : null;
  };

  return <div>
    <PageHeader title={<Typography.Title>カード検索</Typography.Title>} />
    <Typography.Paragraph>
      登録されているカードを検索できます。<br />
      以下のクエリが利用可能です。
      <pre>{
        `<検索ワード> カード名、カードテキストにワードが含まれているカード
set:<setcode> 指定のセットに含まれているカード
cardtype:<クリーチャー|アーティファクト|ソーサリー|インスタント|エンチャント|プレインズウォーカー|バトル|土地> 指定タイプのカード
subtype:<サブタイプ> 指定サブタイプのカード
supertype:<基本|伝説の|氷雪> 指定特殊タイプのカード
rarity:<コモン|アンコモン|レア|神話レア|基本土地> 指定レアリティのカード
`
      }
      </pre>
      検索例
      <pre>{
`例)二段攻撃を持つクリーチャー
cardtype:クリーチャー 二段攻撃

例)レアまたは神話レアのカード
rarity:レア rarity:神話レア
`
        }
      </pre>
    </Typography.Paragraph>
    <Search
      enterButton
      onSearch={handleSearch}
      defaultValue={query}
      loading={searching}
      allowClear
      />
    <CardCacheContext.Provider value={{ cardCache, setCardCache }}>
      {!searching && result && <>
        {query && (<div>「{query}」の検索 {result.total}件</div>)}
        {errorText ? <div style={{ color: "red" }}>{errorText}</div> : <>
          <Space direction="vertical" size="middle" style={{ display: 'flex', marginBottom: "3em" }}>
            <Pager />
            <Cardlist cards={result.cards} hideFilter />
            <Pager />
          </Space>
        </>}
      </>}
    </CardCacheContext.Provider>
  </div>
}

export default CardSearchView;
