import React from 'react';
import { FileData } from 'model/Content';
import MarkdownView from './MarkdownView';
import { EventData } from 'model/Event';
import MarkdownEditor from './MarkdownEditor';
import { User } from 'model/User';
import { Card } from 'model/Card';


const MarkdownContent = (props: {
  me?: User,
  isEdit: boolean,
  contentId?: number,
  content: string,
  events?: EventData[],
  target_cards?: Card[],
  height?: string,
  toolbar?: boolean,
  onUploadFiles?: (files: FileData[]) => void,
  onChange: (value: string) => void,
  onEventChange?: () => void,
}) => {

  if (props.isEdit) {
    return <MarkdownEditor
    contentId={props.contentId}
    onUploadFiles={props.onUploadFiles}
    value={props.content}
    onChange={props.onChange}
    height={props.height}
    toolbar={props.toolbar}
    />
  }
  return <MarkdownView
    me={props.me}
    contentId={props.contentId}
    content={props.content}
    events={props.events}
    target_cards={props.target_cards}
    onEventChange={props.onEventChange}
    />
}

export default MarkdownContent;
