
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE comments(
    id serial primary key,
    author text not null,
    date timestamp not null,
    text text not null,
    edit boolean not null default false,
    parent integer,
    cid integer not null
);

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE comments;
