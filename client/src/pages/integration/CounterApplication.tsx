import React, { useState } from 'react';
import { Typography, PageHeader, Upload, notification, Button, MenuProps } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { DropdownMenu, SimpleMarkdownText } from 'pages/PageCommon';
import { User } from 'model/User';

const items: MenuProps['items'] = [
  {
    key: 'edit',
    label: "編集",
  },
];

const CounterApplication = (props: { me: User }) => {
  const [isEdit, setIsEdit] = useState(false);

  const onClick: MenuProps['onClick'] = ({ key }) => {
    if (key === "edit") {
      setIsEdit(true);
    }
  };

  return <>
    <PageHeader
      title={<Typography.Title>カウンターアプリ</Typography.Title>}
      extra={!isEdit && props.me.isAppDeveloper() && <DropdownMenu
        key="menu"
        menu={{ items, onClick }}
      />} />
    {isEdit && <Upload
      name='bin'
      action='/api/download'
      maxCount={1}
      showUploadList={false}
      accept=".apk"
      onChange={info => {
        if (info.file.status === "done") {
          if (info.file.response.status === "ok") {
            notification.success({ message: `${info.file.name}をアップロードしました` });
          }
        }
      }}
      beforeUpload={(file, list) => {
        if (file.name !== "mtgcounter.apk") {
          notification.error({ message: "ファイル名は mtgcounter.apk である必要があります" });
          return false;
        }
        return true;
      }}
    >
      <Button icon={<UploadOutlined />}>mtgcounter.apk をアップロード</Button>
    </Upload>}
    <SimpleMarkdownText
      pageName="counterapp"
      isEdit={isEdit}
      onSaved={() => setIsEdit(false)}
      />
  </>
}

export default CounterApplication;
