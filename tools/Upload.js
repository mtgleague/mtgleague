const fs = require("fs");
const request = require('request');

require('dotenv').config();

const Url = process.env.URL;
let Token = "";

const sleep = msec => new Promise(resolve => setTimeout(resolve, msec));
const requestAsync = (url, opt = {}) => {
    return new Promise((resolve, reject) => {
        request(url, { ...opt, timeout: 2000 }, (err, res, body) => {
            if (!err) {
                resolve(body);
            } else {
                console.error(err);
                reject(err);
            }
        });
    });
}

const getToken = async () => {
    const res = await requestAsync(`${Url}/api/users/login`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        json: {
            name: process.env.USER,
            password: process.env.PASSWORD,
        },
    });
    Token = res.token;
};

const api = (path, obj) => {
    console.log(`api ${path}`);
    return requestAsync(
        `${Url}/api${path}`,
        {
            method: "POST",
            headers: {
                "Authorization": "Bearer " + Token,
                "Content-Type": "application/json",
            },
            json: obj,
        }
    )
}
const api_file = (path, data) => {
    console.log(`api ${path}`);
    return requestAsync(
        `${Url}/api${path}`,
        {
            method: "POST",
            headers: {
                "Authorization": "Bearer " + Token,
                "Content-Type": "multipart/form-data",
            },
            formData: {
                image: data,
            },
        }
    )
}


const list = fs.readdirSync("./listdata").filter(f => f.endsWith(".json"));
const errorlist = [];
// error.jsonからリトライしたいものだけ抜き出してここに記述
const filter = [
];

(async () => {
    await getToken();

    for (let file of list) {
        const setlist = JSON.parse(fs.readFileSync(`./listdata/${file}`));
        console.log(`load ${file}`);

        for (const card of setlist) {
            if (filter.length > 0) {
                if (!filter.includes(card[0].key)) continue;
            }
            for (const data of card) {
                let retry = false;
                do {
                    retry = false;
                    try {
                        let res = await api("/cards", [data]);
                        if (res.code && res.code !== 200) throw res.message;
                        console.log(res);
                    } catch (e) {
                        if (!retry && e.code == "ETIMEDOUT") {
                            retry = true;
                            console.log("timeout. retry...");
                            await sleep(300 * 20);
                            continue;
                        }
                        errorlist.push({
                            key: card[0].key,
                            err: e
                        });
                    }
                    try {
                        const image = fs.createReadStream(`./download/${data.key}.png`);
                        res = await api_file(`/cards/${data.key}/image`, image);
                        image.close();
                        if (res.code && res.code !== 200) throw res.message;
                        console.log(res);
                    } catch (e) {
                        if (!retry && e.code == "ETIMEDOUT") {
                            retry = true;
                            console.log("timeout. retry...");
                            await sleep(300 * 20);
                            continue;
                        }
                        errorlist.push({
                            key: card[0].key,
                            err: e,
                            img: true
                        });
                    }
                    await sleep(300);
                } while (retry);
            }
        }
    }
    if (errorlist.length > 0) {
        for (const err of errorlist) {
            console.error(err);
        }
        fs.writeFileSync("error.json", JSON.stringify({
            ids: errorlist.map(e => e.key),
            list: errorlist
        }));
    }
})()
