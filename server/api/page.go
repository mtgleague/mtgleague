package api

import (
	"net/http"

	"example.com/m/model"
	"github.com/gin-gonic/gin"
)

// RegisterPageAPI は`/page`以下のAPIを登録します
func RegisterPageAPI(context *Context) {
	context.DBmap.AddTableWithName(model.Page{}, "simple_page").SetKeys(false, "key")

	context.APIAuth.GET("/page/:key", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		key := helper.getParamStr("key")

		var page model.Page
		helper.selectOne(&page, "SELECT * FROM simple_page WHERE key=$1", key)
		if helper.err != nil {
			return
		}
		c.JSON(http.StatusOK, page)
	})

	context.APIAuth.PUT("/page/:key", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		helper.verifyUser()
		if helper.err != nil {
			return
		}
		key := helper.getParamStr("key")

		var page model.Page
		err := c.ShouldBindJSON(&page)
		if err != nil {
			setBadRequestResult(c, err.Error())
			return
		}
		page.Key = key

		_, err = context.DBmap.Update(&page)
		if err != nil {
			setServerError(c, err)
			return
		}
		c.JSON(http.StatusOK, page)
	})
}
