import React from "react";
import {
  LockFilled,
  UnlockFilled,
} from '@ant-design/icons';
import { AccessLevel } from "model/Deck";
import { Tooltip } from "antd";

const PublicStyle = {
  opacity: .4
};
const InternalStyle = {
  color: "green"
};
const PrivateStyle = {
  color: "red"
};

export const PublicIcon = (props: { showTooltip?: boolean }) => {
  if (props.showTooltip) {
    return <Tooltip title="公開">
      <UnlockFilled style={PublicStyle} />
    </Tooltip>;
  }
  return <UnlockFilled style={PublicStyle} />;
};

export const InternalIcon = (props: { showTooltip?: boolean }) => {
  if (props.showTooltip) {
    return <Tooltip title="限定公開">
      <LockFilled style={InternalStyle} />
    </Tooltip>;
  }
  return <LockFilled style={InternalStyle} />;
};

export const PrivateIcon = (props: { showTooltip?: boolean }) => {
  if (props.showTooltip) {
    return <Tooltip title="非公開">
      <LockFilled style={PrivateStyle} />
    </Tooltip>;
  }
  return <LockFilled style={PrivateStyle} />;
};

const AccessLevelIcon = (props: { level: AccessLevel, showTooltip?: boolean }) => {
  switch (props.level) {
    case "private": return <PrivateIcon showTooltip={props.showTooltip} />;
    case "internal": return <InternalIcon showTooltip={props.showTooltip} />;
    case "public": return <PublicIcon showTooltip={props.showTooltip} />;
  }
  return <></>;
};
export default AccessLevelIcon;
