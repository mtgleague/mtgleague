import React from 'react';
import {
  Button,
} from 'antd';
import style from 'assets/League.module.css';


const SaveButton = (props: {
  canSave: boolean,
  saveText?: string,
  onCancel: () => void,
  onSave: () => void,
}) => {
  return <div>
    <Button
      className={style.button}
      onClick={props.onCancel}
      type="dashed">キャンセル</Button>
    <Button
      className={style.button}
      onClick={props.onSave}
      type="primary"
      disabled={!props.canSave}
      >
      {props.saveText || "保存"}
    </Button>
  </div>
}

export default SaveButton;
