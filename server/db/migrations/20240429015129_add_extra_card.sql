
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE cards ADD extra_img_url text;
ALTER TABLE cards RENAME COLUMN reality TO rarity;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE cards DROP COLUMN extra_img_url;
ALTER TABLE cards RENAME COLUMN rarity TO reality;
