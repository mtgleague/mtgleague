package model

// DuelEventNew は作成時の対戦イベントの型
type DuelEventNew struct {
	User1  string `json:"user1" db:"user1"`
	User2  string `json:"user2" db:"user2"`
	Result int16  `json:"result" db:"result"`
	Text   string `json:"text" db:"text"`
}

// DuelEventUpdate は更新時の対戦イベントの型
type DuelEventUpdate struct {
	ID int `json:"id" db:"id, primarykey, autoincrement"`
	DuelEventNew
}

// DuelEvent は取得時の対戦イベントの型
type DuelEvent struct {
	ID     int  `json:"id" db:"id, primarykey, autoincrement"`
	Active bool `json:"active" db:"active"`
	EventNew
	DuelEventNew
}

// SystemEventNew は作成時のシステムイベントの型
type SystemEventNew struct {
	UserName string `json:"username" db:"username"`
	Kind     string `json:"kind" db:"kind"`
}

// SystemEventUpdate は更新時のシステムイベントの型
type SystemEventUpdate struct {
	ID int `json:"id" db:"id, primarykey, autoincrement"`
	SystemEventNew
}

// SystemEvent は取得時のシステムイベントの型
type SystemEvent struct {
	ID     int  `json:"id" db:"id, primarykey, autoincrement"`
	Active bool `json:"active" db:"active"`
	EventNew
	SystemEventNew
}

// EventNew は作成時のイベント共通部分の型
type EventNew struct {
	Date      LocalDate `json:"date" db:"date"`
	Author    string    `json:"author" db:"author"`
	Type      string    `json:"type" db:"type"`
	EventID   int       `json:"-" db:"eid"`
	ContentID int       `json:"content_id" db:"cid"`
}

// Event は取得時のイベント共通部分の型
type Event struct {
	ID int `json:"id" db:"id, primarykey, autoincrement"`
	EventNew
	EventData interface{} `json:"detail" db:"-"`
	Active    bool        `json:"active" db:"active"`
}

// EventAll は作成時の全イベントをまとめて表すための型
type EventAll struct {
	EventNew
	DuelEventNew
	SystemEventNew
}
