package api

import (
	"log"
	"net/http"
	"os"
	"strconv"

	"example.com/m/model"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

// RegisterUsersAPI は`/users`以下のAPIを登録します
func RegisterUsersAPI(context *Context) {
	context.DBmap.AddTableWithName(model.User{}, "users")

	context.API.POST("/users", func(c *gin.Context) {
		var param struct {
			Code     string `json:"code"`
			Name     string `json:"name"`
			Password string `json:"password"`
		}
		err := c.ShouldBindJSON(&param)
		if err != nil {
			setBadRequestResult(c, "invalid parameter.")
			return
		}
		inviteCode := os.Getenv("INVITATION_CODE")
		if param.Code != inviteCode {
			setBadRequestResult(c, "invalid invitation code.")
			return
		}
		if len(param.Password) == 0 {
			setBadRequestResult(c, "invalid password.")
			return
		}
		name, err := context.DBmap.SelectStr("SELECT name FROM users WHERE name=$1", param.Name)
		if param.Name == name {
			setBadRequestResult(c, "exists user.")
			return
		}
		encPass, err := bcrypt.GenerateFromPassword([]byte(param.Password), bcrypt.DefaultCost)
		if err != nil {
			setServerError(c, err)
			return
		}
		user := model.User{
			Name:     param.Name,
			Password: string(encPass),
			Role:     "user",
		}
		err = context.DBmap.Insert(&user)
		if err != nil {
			setServerError(c, err)
			return
		}

		c.JSON(http.StatusCreated, gin.H{
			"status": "ok",
		})
	})
	context.API.POST("/users/login", context.AuthMiddleware.LoginHandler)
	context.API.POST("/users/logout", func(c *gin.Context) {
		c.SetCookie(
			"JWTToken",
			"",
			0,
			"/",
			"",
			context.AuthMiddleware.SecureCookie,
			true,
		)
		c.JSON(http.StatusOK, gin.H{
			"status": "OK",
		})
	})
	context.APIAuth.GET("/users", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		helper.verifyUser()

		exGuestStr, _ := c.GetQuery("exclude_guest")
		exGuest, _ := strconv.ParseBool(exGuestStr)
		optQuery := ""
		if exGuest {
			optQuery = " WHERE role <> 'guest'"
		}
		if helper.getMyRole() == "admin" {
			var result []model.UserFull
			helper.selectAny(&result, "SELECT name, role, line_uid FROM users"+optQuery)
			if helper.err != nil {
				return
			}
			for i, r := range result {
				result[i].ConnectLine = r.LineID.Valid
			}
			c.JSON(http.StatusOK, result)
		} else {
			var result []model.ViewUser
			helper.selectAny(&result, "SELECT name FROM users"+optQuery)
			if helper.err != nil {
				return
			}
			c.JSON(http.StatusOK, result)
		}
	})
	context.APIAuth.GET("/me", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		var result model.UserFull

		name := helper.getMyName()

		err := context.DBmap.SelectOne(&result, "SELECT name, role, line_uid FROM users WHERE name=$1", name)
		if err != nil {
			c.JSON(401, gin.H{
				"code":    401,
				"message": "invalid user.",
			})
			return
		}
		result.ConnectLine = result.LineID.Valid

		c.JSON(http.StatusOK, result)
	})
	context.APIAuth.GET("/users/:name", func(c *gin.Context) {
		userName := c.Param("name")
		log.Print(userName)
		var result model.ViewUser
		err := context.DBmap.SelectOne(&result, "SELECT name FROM users WHERE name=$1", userName)
		if err != nil {
			setServerError(c, err)
			return
		}
		c.JSON(http.StatusOK, result)
	})
	context.APIAuth.DELETE("/users/:name", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}

		helper.verifyAdmin()
		if helper.err != nil {
			return
		}
		userName := c.Param("name")

		log.Print(userName)
		_, err := context.DBmap.Exec("DELETE FROM users WHERE name=$1", userName)
		if err != nil {
			setServerError(c, err)
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"status": "ok",
		})
	})
}
