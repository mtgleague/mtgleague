
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE decks(
    id serial primary key,
    name text not null,
    note text not null default '',
    author text not null,
    cards bytea not null default '\x5b5d',
    side_cards bytea not null default '\x5b5d',
    created_at timestamp not null,
    updated_at timestamp not null,
    use_sets text not null default '',
    is_public boolean not null default false
);

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE decks;

