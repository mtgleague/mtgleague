import React, { useState, useContext } from "react";
import { Input, Pagination, Skeleton, Space } from "antd";
import { Card } from "model/Card";
import Cardlist from "pages/cardlist/Cardlist";
import { Actions } from "pages/cardlist/Types";
import { CardCacheContext } from "pages/cardlist/CardCacheContext";
import Enumerable from "linq";

const { Search } = Input;

const sleep = (msec: number) => new Promise(resolve => setTimeout(resolve, msec));
const ScryfallAPI = async (path: string) => {
  if (process.env.NODE_ENV === "development") {
    console.log(`call Scryfall api ${path}`);
  }
  const res = await fetch(`https://api.scryfall.com${path}`);
  // https://scryfall.com/docs/faqs/i-m-having-trouble-accessing-the-scryfall-api-or-i-m-blocked-17
  // 1秒あたり10リクエスト未満にする必要があるため、スリープをいれることで確実に抑える
  await sleep(200);
  return await res.json();
}
const parseRarity = (rarity: string) => {
  switch (rarity) {
    case "common": return "コモン";
    case "uncommon": return "アンコモン";
    case "rare": return "レア";
    case "mythic": return "神話レア";
    default: return "";
  }
};

const toCardImpl = (commonData: any, d: any): Card => {
  const type = d.printed_type_line?.match(/^(?<super>(?:基本|伝説の|ワールド|氷雪|持続)+)?(?<type>[^—― ]+)(\s*([—― -]+)\s*(?<subtype>.+))?/);
  return {
    ...commonData,
    name: d.printed_name,
    name_kana: "",
    cost: d.mana_cost.replace(/\{/g, "(").replace(/\}/g, ")").replace(/\)\(/g, "),("),
    power: d.power ?? null,
    toughness: d.toughness ?? null,
    loyalty: d.loyalty ?? null,
    supertype: type?.groups["super"] ?? null,
    cardtype: type?.groups["type"] ?? "",
    subtype: type?.groups["subtype"] ?? null,
    text: d.printed_text ?? d.oracle_text ?? "",
    flavor_text: d.flavor_text ?? "",
    extra_img_url: commonData.extra_img_url ?? d.image_uris?.normal,
  };
};
const toCard = (d: any): { card: Card, otherCards: Card[] } => {
  const commonData = {
    key: d.id,
    no: Number(d.collector_number),
    set: d.set,
    rarity: parseRarity(d.rarity),
    back_side_key: null,
    other_keys: null,
    extra_img_url: d.image_uris?.normal,
  };
  if (d.card_faces) {
    if (d.image_uris) {
      // 直接画像を持つ場合は1面に複数の情報があるカード
      const main = {
        ...toCardImpl(commonData, d.card_faces[0]),
        key: d.id,
        other_keys: Enumerable.from(d.card_faces)
          .skip(1)
          .select((_, index) => `${d.id}-${index + 1}`)
          .toJoinedString(","),
      };
      const otherCards = Enumerable.from(d.card_faces)
        .skip(1)
        .select((d, index) => ({
          ...toCardImpl(commonData, d),
          key: `${main.key}-${index + 1}`,
          other_keys: main.key,
        }))
        .toArray();
      return { card: main, otherCards };
    } else {
      // 直接画像を持たない場合は両面カード
      const front = {
        ...toCardImpl(commonData, d.card_faces[0]),
        key: d.id,
        back_side_key: `${d.id}-back`,
      };
      const back = {
        ...toCardImpl(commonData, d.card_faces[1]),
        key: `${d.id}-back`,
        back_side_key: d.id,
      };
      return { card: front, otherCards: [ back ] };
    }
  } else {
    return {
      card: toCardImpl(commonData, d),
      otherCards: []
    };
  }
};

const ExtraSearch = (props: { actions: Actions }) => {
  const [searching, setSearching] = useState(false);
  const [query, setQuery] = useState("");
  const [page, setPage] = useState(1);
  const [pageMax, setPageMax] = useState(1);
  const [totalCount, setTotalCount] = useState(0);
  const [list, setList] = useState<Card[]>([]);
  const [errMsg, setErrMsg] = useState("");
  const { cardCache, setCardCache } = useContext(CardCacheContext);
  const searchCards = async (query: string, page: number) => {
    setPage(page);
    setQuery(query);
    setErrMsg("");
    setSearching(true);
    try {
      const { cards, otherCards } = await (async() => {
        const m = query.match(/https:\/\/scryfall\.com\/card\/(?<setno>[^/]+\/\d+(\/ja)?)\/.+/);
        if (m?.groups) {
          const result = await ScryfallAPI(`/cards/${m.groups["setno"]}`);
          if (result.object === "error") throw result;
          setTotalCount(1);
          const { card, otherCards } = toCard(result);
          return { cards: [ card ], otherCards };
        } else {
          const q = encodeURIComponent(query + " lang:ja");
          const result = await ScryfallAPI(`/cards/search?q=${q}&include_multilingual=true&page=${page}`);
          if (result.object === "error") throw result;
          setTotalCount(result.total_cards);
          setPageMax(n => Math.max(n, result.data.length));
          const list: { card: Card, otherCards: Card[] }[] = result.data.map((d: any) => toCard(d));
          return { cards: list.map(d => d.card), otherCards: list.flatMap(d => d.otherCards) };
        }
      })();
      setList(cards);
      addToCache(otherCards);
    } catch (e) {
      console.error(e);
      setErrMsg("検索に失敗しました。");
    } finally {
      setSearching(false);
    }
  };
  const addToCache = (cards: Card[]) => {
    if (cards.length === 0) {
      return;
    }
    let cache = cardCache;
    let isUpdate = false;
    for (const c of cards) {
      if (!cache[c.key]) {
        cache = { ...cache, [c.key]: c };
        isUpdate = true;
      }
    }
    if (isUpdate) {
      setCardCache(cache);
    }
  };
  const handleSearch = async (value: string) => {
    setTotalCount(0);
    setPageMax(1);
    await searchCards(value, 1);
  };
  const handleChangePage = async (page: number) => {
    await searchCards(query, page);
  };

  const Pager = () => {
    return totalCount > 0 && totalCount > pageMax ? <Pagination
      current={page}
      onChange={handleChangePage}
      total={totalCount}
      pageSize={pageMax}
      showSizeChanger={false}
      /> : null;
  };

  return <div>
    <p>
      Scryfallに登録されているカードをデッキに追加することができます。<br />
      利用可能な検索キーワードの詳細な説明は<a href="https://scryfall.com/docs/syntax">https://scryfall.com/docs/syntax</a>にあります。<br />
      <a href="https://scryfall.com/card/m20/160/ja/%E3%82%B7%E3%83%A7%E3%83%83%E3%82%AF">https://scryfall.com/card/m20/160/ja/%E3%82%B7%E3%83%A7%E3%83%83%E3%82%AF</a> のようなカードの個別ページURLを指定することもできます。
    </p>
    <Search
      enterButton
      onSearch={handleSearch}
      loading={searching}
      allowClear
      />
    {searching ? <>
      <Skeleton active paragraph={{ rows: 30 }} />
    </> : <>
      {query && (<div>「{query}」の検索 {totalCount}件</div>)}
      {errMsg ? <div style={{ color: "red" }}>{errMsg}</div> : <>
        <Space direction="vertical" size="middle" style={{ display: 'flex', marginBottom: "3em" }}>
          <Pager />
          <Cardlist
              cards={list}
              canEdit={false}
              hideFilter
              actions={props.actions}
              />
          <Pager />
        </Space>
      </>}
    </>}
  </div>;
};

export default ExtraSearch;
