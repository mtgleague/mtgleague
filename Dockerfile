# Build the Go API
FROM golang:1.15.0 AS builder
WORKDIR /app
COPY ./server/go.mod ./server/go.sum ./
RUN go mod download
COPY ./server/ ./
RUN GOOS=linux GOARCH=amd64 go build --ldflags '-w -extldflags "-static"' -a -o /main .

# Build the React application
FROM node:14.8.0-alpine3.11 AS node_builder
WORKDIR /app
COPY ./client/package.json ./client/tsconfig.json ./client/yarn.lock ./
RUN yarn install
COPY ./client/src ./src
COPY ./client/public ./public
RUN yarn build

# Final stage build, this will be the container
# that we will deploy to production
FROM alpine:3.12.0
RUN apk --no-cache add ca-certificates tzdata
ENV GIN_MODE=release
ENV PORT=80
COPY --from=builder /main ./
COPY ./server/db/ ./db
COPY --from=node_builder /app/build ./web
RUN chmod +x ./main
CMD ./main

EXPOSE 80
