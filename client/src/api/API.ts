import { notification } from "antd";
import { History, LocationState } from 'history';

class API {
  public static history: History<LocationState> | null;

  static async GET(
    path: string,
    callback: (response: any) => void = _ => {},
    errorCallback: ((response: any) => void) | null = null
    ) {
    return await this.common(
      path,
      {
        method: "GET",
      },
      callback,
      errorCallback
    );
  }

  static async PUT(
    path: string,
    obj: object,
    callback: (response: any) => void = _ => {}
    ) {
    return await this.common(
      path,
      {
        method: "PUT",
        body: JSON.stringify(obj),
      },
      callback,
      null
    );
  }

  static async POST(
    path: string,
    obj: object,
    callback: (response: any) => void = _ => {},
    errorCallback: ((response: any) => void) | null = null
  ) {
    return await this.common(
      path,
      {
        method: "POST",
        body: JSON.stringify(obj),
      },
      callback,
      errorCallback
    );
  }

  static async POSTF(path: string, obj: FormData, callback: (response: any)=>void = _ => {}) {
    return await this.common(
      path,
      {
        method: "POST",
        body: obj,
      },
      callback,
      null
    );
  }

  static async DELETE(path: string, callback: (response: any)=>void = _ => {}) {
    return await this.common(
      path,
      {
        method: "DELETE",
      },
      callback,
      null
    );
  }

  private static async common(
    path: string,
    opt: RequestInit,
    callback: (response: any) => void,
    errorCallback: ((response: any) => void) | null
    ) {
    if (process.env.NODE_ENV === "development") {
      console.log(`call api [${opt.method}] ${path}`);
    }
    const req = await fetch("/api" + path, opt);

    // 認証されてなかったらログインページに飛ばす
    if (req.status === 401) {
      if (this.history) {
        this.history.push("/login?url=" + encodeURIComponent(this.history.createHref(this.history.location)));
      }
      return null;
    }
    const isOK = req.ok;

    const res = await req.json();

    if (!isOK && res.code && res.code !== 200) {
      if (errorCallback) {
        errorCallback(res);
      } else {
        notification.error({ message: res.message });
      }
      console.error(res);
      return res;
    }

    try {
      callback(res);
      return res;
    } catch (e) {
      console.error(e);
      return null;
    }
  }

}

export default API;
