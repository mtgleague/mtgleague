@echo off

set backupfile=backup.bin
set container=go-postgres
docker cp %backupfile% %container%:/
docker exec -it %container% pg_restore -c -U postgres -d postgres %backupfile%
