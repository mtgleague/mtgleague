package api

import (
	"io"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

// RegisterDownloadAPI は`/download`以下のAPIを登録します
func RegisterDownloadAPI(context *Context) {
	context.Download.GET("/mtgcounter.apk", func(c *gin.Context) {
		c.Header("Cache-Control", "private, no-store, no-cache, must-revalidate")
		c.Header("Content-Type", "application/vnd.android.package-archive")
		c.File("./download/mtgcounter.apk")
	})

	context.APIAuth.POST("/download", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		helper.verifyAppDeveloper()
		if helper.err != nil {
			return
		}

		file, header, err := c.Request.FormFile("bin")
		if err != nil {
			setBadRequestResult(c, err.Error())
			return
		}
		fileName := header.Filename
		dir, _ := os.Getwd()
		out, err := os.Create(dir + "/download/" + fileName)
		if err != nil {
			setServerError(c, err)
			return
		}
		defer out.Close()
		_, err = io.Copy(out, file)
		if err != nil {
			setServerError(c, err)
			return
		}
		c.JSON(http.StatusOK, gin.H{
			"status": "ok",
		})
	})
}
