import React, { useContext, useEffect, useState } from 'react';
import { List, Drawer, Typography, Descriptions, Radio, Switch } from 'antd';
import { Card, compareSameCard } from 'model/Card';
import { isMobile } from 'react-device-detect';
import Enumerable from 'linq';
import CardItem, { cardImageUrl } from './CardItem';
import { Action, Actions, CardData, CardFilter, cardTypeToOrder, ColorKind, costToOrder, RarityKind, RarityKindName, SortKind, SortKindName, TypeKind, TypeKindName } from './Types';
import { RadioChangeEvent } from 'antd/lib/radio';
import BitFlags from 'util/BitFlags';
import style from 'assets/Cardlist.module.css';
import { isNumber } from 'lodash';
import API from 'api/API';
import { CardCache, CardCacheContext } from './CardCacheContext';

interface Props {
  cards: Card[];
  onAdd?: Action;
  onDelete?: Action;
  canEdit?: boolean;
  actions?: Actions;
  hideFilter?: boolean;
}

const ListProps = {
  grid: {
    gutter: 16,
    xs: 2,
    sm: 2,
    md: 3,
    lg: 4,
    xl: 6,
    xxl: 8,
  },
  style: {
    margin: 4
  }
};

interface SwitchProps<T extends number>
{
  flags: BitFlags<T>,
  value: T,
  background: string,
  label: string,
  onChange: (value: T, checked: boolean) => void,
};

function SwitchBase<T extends number>(props: SwitchProps<T>) {
  const { flags, value, background, label, onChange } = props;
  const checked = flags.get(value);

  return <Switch
    checked={checked}
    onChange={v => onChange(value, v)}
    className={style["filter-switch"]}
    style={{ backgroundColor: background, opacity: checked ? 1 : .4 }}
    checkedChildren={label}
    unCheckedChildren={label}
    />
};

const ColorSwitch = (props: SwitchProps<ColorKind>) => SwitchBase<ColorKind>(props);
const TypeSwitch = (props: SwitchProps<TypeKind>) => SwitchBase<TypeKind>(props);
const RaritySwitch = (props: SwitchProps<RarityKind>) => SwitchBase<RarityKind>(props);

const AllSwitch = (props: { checked: boolean, onChange: (checked: boolean) => void }) => {
  const { checked, onChange } = props;
  return <Switch
    checked={checked}
    onChange={onChange}
    className={style["filter-switch"]}
    style={{ backgroundColor: "#d145ff", opacity: checked ? 1 : .4 }}
    checkedChildren="全部"
    unCheckedChildren="全部"
    />;
}
const CardFilterItem = (props: { cards: Card[], onFilterChange: (cards: CardData[]) => void }) => {
  const [filter, setFilter] = useState<CardFilter>({
    sort: SortKind.CardNo,
    color: new BitFlags<ColorKind>(ColorKind),
    type: new BitFlags<TypeKind>(TypeKind),
    rarity: new BitFlags<RarityKind>(RarityKind),
  });
  const handleSortFilter = (e: RadioChangeEvent) => {
    const value = e.target.value;
    setFilter({ ...filter, sort: value });
  };
  const handleColorFilter = (color: ColorKind, checked: boolean) => {
    const flags = filter.color.clone();
    flags.set(color, checked);
    setFilter({ ...filter, color: flags });
  };
  const handleAllColorFilter = (checked: boolean) => {
    const flags = filter.color.clone();
    if (checked) {
      flags.fill(ColorKind);
    } else {
      flags.clear();
    }
    setFilter({ ...filter, color: flags });
  };
  const handleTypeFilter = (type: TypeKind, checked: boolean) => {
    const flags = filter.type.clone();
    flags.set(type, checked);
    setFilter({ ...filter, type: flags });
  };
  const handleAllTypeFilter = (checked: boolean) => {
    const flags = filter.type.clone();
    if (checked) {
      flags.fill(TypeKind);
    } else {
      flags.clear();
    }
    setFilter({ ...filter, type: flags });
  };
  const handleRarityFilter = (rarity: RarityKind, checked: boolean) => {
    const flags = filter.rarity.clone();
    flags.set(rarity, checked);
    setFilter({ ...filter, rarity: flags });
  };
  const handleAllRarityFilter = (checked: boolean) => {
    const flags = filter.rarity.clone();
    if (checked) {
      flags.fill(RarityKind);
    } else {
      flags.clear();
    }
    setFilter({ ...filter, rarity: flags });
  };


  useEffect(() => {
    const sortByNo = (a: Card, b: Card) => a.set === b.set ? a.no - b.no : a.key < b.key ? -1 : a.key > b.key ? 1 : 0;
    const sortByType = (a: Card, b: Card) => cardTypeToOrder(a.cardtype) - cardTypeToOrder(b.cardtype);
    const sortByCost = (a: Card, b: Card) => costToOrder(a.cost, a.cardtype) - costToOrder(b.cost, b.cardtype);
    let cards = props.cards.slice();
    cards = cards.filter(c => {
      if (filter.color.get(ColorKind.White) && c.cost.includes("W")) return true;
      if (filter.color.get(ColorKind.Blue) && c.cost.includes("U")) return true;
      if (filter.color.get(ColorKind.Black) && c.cost.includes("B")) return true;
      if (filter.color.get(ColorKind.Red) && c.cost.includes("R")) return true;
      if (filter.color.get(ColorKind.Green) && c.cost.includes("G")) return true;
      if (filter.color.get(ColorKind.Colorless) && !c.cost.match(/W|U|B|R|G/)) return true;
      return false;
    });
    cards = cards.filter(c => {
      for (let key of Object.keys(TypeKind).map(Number).filter(isNumber)) {
        if (filter.type.get(key) && c.cardtype.includes(TypeKindName[key])) return true;
      }
      return false;
    });
    cards = cards.filter(c => {
      for (let key of Object.keys(RarityKind).map(Number).filter(isNumber)) {
        if (filter.rarity.get(key) && c.rarity === RarityKindName[key]) return true;
      }
      return false;
    });
    switch (filter.sort) {
      case SortKind.CardNo: cards.sort(sortByNo); break;
      case SortKind.Type: cards.sort(sortByType); break;
      case SortKind.Cost: cards.sort(sortByCost); break;
    }
    props.onFilterChange(Enumerable.from(cards)
      .groupBy(c => c.key)
      .select(g => ({ card: g.first(), count: g.count() }))
      .toArray());
  }, [props.cards, filter]);

  const colorSwitchList = [
    { value: ColorKind.White, background: "goldenrod", label: "白" },
    { value: ColorKind.Blue, background: "blue", label: "青" },
    { value: ColorKind.Black, background: "black", label: "黒" },
    { value: ColorKind.Red, background: "red", label: "赤" },
    { value: ColorKind.Green, background: "green", label: "緑" },
    { value: ColorKind.Colorless, background: "gray", label: "無色" },
  ];
  const typeSwitchList = [
    { value: TypeKind.Creature },
    { value: TypeKind.Enchantment },
    { value: TypeKind.Artifact },
    { value: TypeKind.Instant },
    { value: TypeKind.Sorcery },
    { value: TypeKind.Planeswalker },
    { value: TypeKind.Battle },
    { value: TypeKind.Land },
  ];
  const raritySwitchList = [
    { value: RarityKind.Land },
    { value: RarityKind.Common },
    { value: RarityKind.Uncommon },
    { value: RarityKind.Rare },
    { value: RarityKind.MythicRare },
  ];
  return <div>
    <div>
      ソート： <Radio.Group
      options={SortKindName.map((n, i) => ({ label: n, value: i }))}
      optionType="button"
      buttonStyle="solid"
      value={filter.sort}
      onChange={handleSortFilter}
      />
    </div>
    <div>
      色：
      <AllSwitch
        checked={filter.color.all(ColorKind)}
        onChange={handleAllColorFilter}
        />
      {colorSwitchList.map(v => <ColorSwitch
        key={v.value}
        {...v}
        flags={filter.color}
        onChange={handleColorFilter}
        />)}
    </div>
    <div>
      種類：
      <AllSwitch
        checked={filter.type.all(TypeKind)}
        onChange={handleAllTypeFilter}
        />
      {typeSwitchList.map(v => <TypeSwitch
        key={v.value}
        {...v}
        flags={filter.type}
        label={TypeKindName[v.value]}
        background="gray"
        onChange={handleTypeFilter}
        />)}
    </div>
    <div>
      レアリティ：
      <AllSwitch
        checked={filter.rarity.all(RarityKind)}
        onChange={handleAllRarityFilter}
        />
      {raritySwitchList.map(v => <RaritySwitch
        key={v.value}
        {...v}
        flags={filter.rarity}
        label={RarityKindName[v.value]}
        background="gray"
        onChange={handleRarityFilter}
        />)}
    </div>
  </div>;
};

const loadCache = async (cache: CardCache, keys: string[]): Promise<CardCache> => {
  const loadKeys = keys.filter(k => !cache[k]);
  if (loadKeys.length > 0) {
    const cards: Card[] = await API.POST("/cards/keys", loadKeys);
    return Object.fromEntries(cards.map(c => [ c.key, c ]));
  }
  return {};
};

const Cardlist = (props: Props) => {
  const [detailCards, setDetailCards] = useState<Card[] | null>(null);
  const [viewCards, setViewCards] = useState<CardData[]>();
  const { cardCache, setCardCache } = useContext(CardCacheContext);

  useEffect(() => {
    if (props.hideFilter) {
      setViewCards(props.cards.map(c => ({ card: c, count: 1 })));
    }
    setCardCache(cache => ({ ...cache, ...Object.fromEntries(props.cards.map(c => [c.key, c])) }));
    (async () => {
      const otherKeys = props.cards
      .flatMap(c => [ c.back_side_key, ...(c.other_keys?.split(",") ?? []) ])
      .filter(key => !!key)
      .map(key => key!);
      const otherCards = await loadCache(cardCache, otherKeys);
      setCardCache(cache => ({ ...cache, ...otherCards }));
    })();
  }, [props.cards, props.hideFilter]);
  const handleClickItem = async (item: Card) => {
    const items = [item];
    const other_keys: string[] = [];
    if (item.back_side_key) other_keys.push(item.back_side_key);
    if (item.other_keys) other_keys.push(...item.other_keys.split(","));
    if (other_keys.length > 0) {
      const addCacheCards = await loadCache(cardCache, other_keys);
      setCardCache(cache => ({ ...cache, ...addCacheCards }));
      items.push(...other_keys.map(k => cardCache[k]));
      items.sort(compareSameCard);
    }
    setDetailCards(items);
  };
  const text2elems = (text: string) => {
    return text?.split("\n").map((s, i) => <span key={i}>{s}<br /></span>);
  };
  const hasPT = (card: Card) => card.power !== null && card.toughness !== null;
  const hasL = (card: Card) => card.loyalty !== null;
  const hasCost = (card: Card) => !!card.cost;
  const hasPTorL = (card: Card) => hasPT(card) || hasL(card);

  return <>
    {!props.hideFilter && <CardFilterItem
      cards={props.cards}
      onFilterChange={cards => setViewCards(cards)}
      />}
    <List
      {...ListProps}
      pagination={!props.hideFilter && {
        defaultPageSize: 48,
      }}
      dataSource={viewCards}
      renderItem={(card, i) => (
        <List.Item key={i}>
          <CardItem
            card={card}
            onAdd={props.onAdd}
            onDelete={props.onDelete}
            actions={props.actions}
            canEdit={props.canEdit}
            onCardClick={() => handleClickItem(card.card)}
            />
        </List.Item>
      )}
      >
    </List>
    {/* ====詳細表示画面==== */}
    <Drawer
      width={isMobile ? "100%" : "50%"}
      placement="right"
      onClose={() => setDetailCards(null)}
      open={!!detailCards}
      >
        {detailCards && detailCards.map(card => (<div className={style["card-detail"]} key={card.key}>
          <Typography.Title level={3}>
            <ruby>{card.name}<rt>{card.name_kana}</rt></ruby>
          </Typography.Title>
          <img
            alt={card.name}
            className={style["card-img"]}
            src={cardImageUrl(cardCache, card)}
            />
          <Descriptions bordered>
            <Descriptions.Item label="カードセット" span={hasCost(card) ? 1 : 2}>{card.set}</Descriptions.Item>
            {hasCost(card) && <Descriptions.Item label="コスト">{card.cost}</Descriptions.Item>}
            <Descriptions.Item label="レアリティ">{card.rarity}</Descriptions.Item>
            <Descriptions.Item label="タイプ" span={hasPTorL(card) ? 2 : 3}>{card.supertype}{card.cardtype}{card.subtype && (` － ${card.subtype}`)}</Descriptions.Item>
            {hasPT(card) && <Descriptions.Item label="パワー/タフネス">{card.power}/{card.toughness}</Descriptions.Item>}
            {hasL(card) && <Descriptions.Item label="忠誠度">{card.loyalty}</Descriptions.Item>}
            <Descriptions.Item label="テキスト" span={3}>
              {text2elems(card.text)}{card.flavor_text && (<div><hr />{text2elems(card.flavor_text)}</div>)}
            </Descriptions.Item>
            <Descriptions.Item label="リンク" span={3}>
              <ul>
                <li>
                  <a href={`http://mtgwiki.com/wiki/index.php?search=${card.name}`} target="_blank" rel="noopener noreferrer">M:TG Wikiで検索</a>
                </li>
                <li>
                  <a href={`https://whisper.wisdom-guild.net/search.php?q=${card.name}`} target="_blank" rel="noopener noreferrer">WisdomGuildで検索</a>
                </li>
                <li>
                  <a href={`https://scryfall.com/card/${card.set.toLowerCase()}/${card.no}/ja`} target="_blank" rel="noopener noreferrer">Scryfallで表示</a>
                </li>
              </ul>
            </Descriptions.Item>
          </Descriptions>
        </div>))}
    </Drawer>
  </>
};

export default Cardlist;
