import React, { Component, useState } from 'react';
import { Route, Switch, RouteChildrenProps, Link, useHistory } from 'react-router-dom';
import { Layout, Menu, Drawer, Button, BackTop, AutoComplete } from 'antd';
import {
  LinkOutlined,
  HomeOutlined,
  HistoryOutlined,
  ToolOutlined,
  MenuOutlined,
} from '@ant-design/icons';
import { isMobile } from 'react-device-detect';
import { Location, LocationState, UnregisterCallback } from 'history';
import queryString from 'query-string';
import League from 'pages/league/League';
import NewLeague from 'pages/NewLeague';
import Import from 'pages/Import';
import API from 'api/API';
import NotFound from './NotFound';
import { Content } from 'model/Content';
import style from "assets/Main.module.css";
import { createUser, User } from 'model/User';
import moment from 'moment';
import Decklist from './deck/Decklist';
import DeckView from './deck/DeckView';
import NewCardSet from './NewCardSet';
import NewCard from './NewCard';
import Socket from 'api/Socket';
import IntegrationService from './integration/IntegrationService';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCogs, faBook, faSearch, faFileCode, faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import CardSetView from './cardlist/CardSetView';
import CounterApplication from './integration/CounterApplication';
import DuelSearchView from './search/DuelSearchView';
import CardSearchView from './search/CardSearchView';
import SiteChangelog from './SiteChangelog';
import SiteHelp from './SiteHelp';

const { Sider } = Layout;
const LayoutContent = Layout.Content;
const { SubMenu } = Menu;

interface Props extends RouteChildrenProps {
}
interface State {
  me: User;
  checked: boolean;
  currentContentId: number | null;
  contents: Content[];
  selected: string | null;
  placeList: string[];
  placeActive: string;
}


interface MenuProps {
  me: User;
  contents: Content[];
  currentContentId: number | null;
  selected: string | null;
}

interface MobileMenuProps extends MenuProps {
  headerItem: React.ReactElement,
}

const SideMenu = (props: MenuProps) => {
  const contents: any = {};
  props.contents.forEach(c => {
    const y = moment(c.start_date).year();
    !contents[y] && (contents[y] = [] as Content[]);
    contents[y].push(c);
  });
  const keys = Object.keys(contents).sort((a, b) => Number(b) - Number(a));
  return <Menu theme="dark" mode="inline" selectable={false} selectedKeys={props.selected ? [ props.selected ] : undefined}>
    <Menu.Item key="home" icon={<HomeOutlined />}>
      <Link to='/'>ホーム</Link>
    </Menu.Item>
    <SubMenu key="old-league" icon={<HistoryOutlined />} title="過去のリーグ">
      {keys.map(key => <SubMenu key={key} title={key}>
        {contents[key].map((c: Content) => <Menu.Item key={"page-" + c.id}>
          <Link to={`/?id=${c.id}`}>{c.title}</Link>
        </Menu.Item>)}
      </SubMenu>)}
    </SubMenu>
    {!props.me.isGuest() && (
      <Menu.Item key="search" icon={<FontAwesomeIcon icon={faSearch} />}>
        <Link to='/search'>履歴検索</Link>
      </Menu.Item>
    )}
    <Menu.Item key="cardsearch" icon={<FontAwesomeIcon icon={faSearch} />}>
      <Link to='/cardsearch'>カード検索</Link>
    </Menu.Item>
    {!props.me.isGuest() && (
      <Menu.Item key="deck" icon={<FontAwesomeIcon icon={faBook} />}>
        <Link to='/deck'>デッキ一覧</Link>
      </Menu.Item>
    )}
    <Menu.Item key="cardlist" icon={<FontAwesomeIcon icon={faBook} />}>
      <Link to='/cardlist'>カードリスト</Link>
    </Menu.Item>
    {!props.me.isGuest() && (
      <SubMenu key="tools" icon={<ToolOutlined />} title="管理ツール">
        <Menu.Item key="new">
          <Link to='/new'>新規ページ</Link>
        </Menu.Item>
        <Menu.Item key="import">
          <Link to={"/import" + (props.currentContentId ? `?id=${props.currentContentId}` : "")}>インポート</Link>
        </Menu.Item>
      </SubMenu>
    )}
    {!props.me.isGuest() && (
      <Menu.Item key="integration" icon={<FontAwesomeIcon icon={faCogs} />}>
        <Link to='/integration'>外部サービス</Link>
      </Menu.Item>
    )}
    {!props.me.isGuest() && (
      <Menu.Item key="counterapp" icon={<FontAwesomeIcon icon={faCogs} />}>
        <Link to='/counterapp'>カウンターアプリ</Link>
      </Menu.Item>
    )}
    {!props.me.isGuest() && (
      <Menu.Item key="changelog" icon={<FontAwesomeIcon icon={faFileCode} />}>
        <Link to='/changelog'>更新履歴</Link>
      </Menu.Item>
    )}
    {!props.me.isGuest() && (
      <Menu.Item key="help" icon={<FontAwesomeIcon icon={faQuestionCircle} />}>
        <Link to='/help'>ヘルプ</Link>
      </Menu.Item>
    )}
    <SubMenu key="ext-tools" icon={<LinkOutlined />} title="便利リンク集">
      <Menu.Item>
        <a href="http://mtgwiki.com/wiki/" target="_blank" rel="noopener noreferrer">M:TG Wiki</a>
      </Menu.Item>
      <Menu.Item>
        <a href="http://whisper.wisdom-guild.net/apps/autodic/" target="_blank" rel="noopener noreferrer">カード名辞書</a>
      </Menu.Item>
      <Menu.Item>
        <a href="https://scryfall.com/" target="_blank" rel="noopener noreferrer">Scryfall</a>
      </Menu.Item>
    </SubMenu>
  </Menu>
}

const LogoutButton = () => {
  const history = useHistory();
  const action = () => {
    console.log("logout.");
    API.POST("/users/logout", {}, _ => {
      history.push("/login");
    });
  };
  return <Button onClick={action} ghost={true}>ログアウト</Button>
}

const ActivePlace = (props: {
  list: string[],
  active: string,
  onChange: (value: string) => void,
}) => {
  const options = props.list.map((v: string) => ({ value: v }));

  return <div style={{ marginRight: "4em" }}>
    <AutoComplete
      style={{ width: "6em" }}
      options={options}
      value={props.active}
      onChange={props.onChange}
      placeholder="活動場所"
    />
  </div>;
}
const MobileMenu = (props: MobileMenuProps) => {

  const [visible, setVisible] = useState(false);
  const onMenuClick = () => setVisible(true);
  const onClise = () => setVisible(false);

  return <>
    <Layout.Header style={{ padding: "0 10px" }} className={style.header}>
      <Button
        onClick={onMenuClick}
        style={{ backgroundColor: "transparent", border: "none", marginRight: "auto" }}
        >
        <MenuOutlined style={{ color: "#fff", fontSize: 20 }} />
      </Button>
      {props.headerItem}
      <LogoutButton />
    </Layout.Header>
    <Drawer
      placement="left"
      closable={false}
      open={visible}
      onClose={onClise}
      bodyStyle={{
        backgroundColor: "#001529",
        padding: "12px 0"
      }}
      width="60%"
    >
      <SideMenu {...props} />
    </Drawer>
  </>
}

const PcMenu = (props: MenuProps) => {
  const [collapsed, setCollapsed] = useState(false);
  const onClollapse = (collapsed: boolean) => setCollapsed(collapsed);
  return <Sider
    collapsible
    collapsed={collapsed}
    onCollapse={onClollapse}
    breakpoint="md"
    >
    <SideMenu {...props} />
  </Sider>
}

const MainContent = (props: any) => {
  const isGuest = props.me.isGuest();
  return (<LayoutContent className={style["content"]}>
    <Switch>
      <Route exact path="/" render={p => <League
          {...p}
          me={props.me}
          contentId={props.currentContentId}
          contents={props.contents}
          onDelete={props.onDeletePage}
        />} />
      {!isGuest && <Route exact path="/new" render={p => <NewLeague
            {...p}
            onAdd={props.onAddPage}
            />} />}
      {!isGuest && <Route exact path="/import" component={Import} />}
      <Route exact path="/deck" render={p => <Decklist
        {...p}
        me={props.me}
      />} />
      {!isGuest && <Route exact path="/new_set" component={NewCardSet} />}
      {!isGuest && <Route exact path="/new_card" component={NewCard} />}
      <Route exact path="/deck/:id" render={p => <DeckView
        {...p}
        me={props.me}
        id={p.match.params.id}
      />} />
      <Route exact path="/deck/:id/edit" render={p => <DeckView
        {...p}
        me={props.me}
        id={p.match.params.id}
        isEdit={true}
      />} />
      <Route exact path="/cardlist" render={p => <CardSetView
        {...p}
        me={props.me}
      />} />
      <Route exact path="/search" render={p => <DuelSearchView
        {...p}
        me={props.me}
      />} />
      <Route exact path="/cardsearch" render={p => <CardSearchView
        {...p}
        me={props.me}
      />} />
      <Route exact path="/integration" render={p => <IntegrationService
        {...p}
        me={props.me}
        onUnlink={props.onUnlinkIntegration}
      />} />
      <Route exact path="/integration/:id" render={p => <IntegrationService
        {...p}
        me={props.me}
        id={p.match.params.id}
        onUnlink={props.onUnlinkIntegration}
      />} />
      <Route exact path="/counterapp" render={p => <CounterApplication
        {...p}
        me={props.me}
      />} />
      <Route exact path="/changelog" render={p => <SiteChangelog
        {...p}
        me={props.me}
      />} />
      <Route exact path="/help" render={p => <SiteHelp
        {...p}
        me={props.me}
      />} />
      <Route component={NotFound} />
    </Switch>
  </LayoutContent>);
}

class Main extends Component<Props, State> {
  unlink: UnregisterCallback | null = null;
  timer: any = 0;

  constructor(props: Props) {
    super(props);
    this.state = {
      me: createUser({ 
        name: "",
        role: "guest",
      }),
      checked: false,
      currentContentId: null,
      contents: [],
      selected: null,
      placeList: [],
      placeActive: "",
    };
    this.handleAddPage = this.handleAddPage.bind(this);
    this.handleDeletePage = this.handleDeletePage.bind(this);
    this.handlePlaceChange = this.handlePlaceChange.bind(this);
    this.handleUnlinkIntegration = this.handleUnlinkIntegration.bind(this);

    this.handleSocketPlaceChange = this.handleSocketPlaceChange.bind(this);
  }

  componentDidMount() {
    API.history = this.props.history;
    Socket.connect();
    (async () => {
      const promises = [] as Promise<void>[];
      promises.push(API.GET(`/me`, e => this.setState({ me: createUser(e) })));
      promises.push(API.GET("/places", e => this.setState({
        placeActive: moment(e.updated_at).isAfter(moment().subtract(4, "hours")) ? e.active : "",
        placeList: e.list,
      })));
      promises.push(this.reloadContents());
      const parseId: (location: Location<LocationState>)=>{ currentContentId: number | null, selected: string | null } = location => {
        const param = queryString.parse(location.search);
        const currentContentId = Number(param.id) || null;
        let selected: string | null = null;
        switch (location.pathname) {
          case "/":
            if (currentContentId !== null) {
              selected = "page-" + currentContentId;
            }
            break;
          default:
            selected = location.pathname.substring(1);
            break;
          }
        return { currentContentId, selected, };
      };
  
      // URLが変わったら現在のIDをリロード
      this.unlink = this.props.history.listen((location) => {
        document.title = `ギャザ部`;
        const state = parseId(location);
        this.setState(state);
      });
      const state = parseId(this.props.location);
      await Promise.all(promises);
      this.setState({ ...state, checked: true });
    })();
    Socket.addListener("change_place", this.handleSocketPlaceChange);
  }

  componentWillUnmount() {
    if (this.unlink) {
      this.unlink();
      this.unlink = null;
    }
    API.history = null;
    Socket.removeListener("change_place", this.handleSocketPlaceChange);
    Socket.disconnect();
  }

  async reloadContents() {
    await API.GET(`/contents`, (e: Content[]) => {
      const state: any = {};
      const contents = e;
      // 開始日時でソート
      contents.sort((a, b) => 
        a.start_date > b.start_date ? -1
        : a.start_date < b.start_date ? 1
        : a.title < b.title ? -1
        : a.title > b.title ? 1
        : 0
      );
      state.contents = contents;
      this.setState(state);
    });
  }

  handleSocketPlaceChange(data: { updated_at: moment.Moment, active: string }) {
    this.setState({
      placeActive: data.active
    });
  }

  handleDeletePage() {
    this.reloadContents();
  }

  handleAddPage(item: Content) {
    this.reloadContents();
    this.setState({ currentContentId: item.id });
    this.props.history.push("/?id=" + item.id);
  }

  handlePlaceChange(value: string) {
    this.setState({ placeActive: value });
    if (value) {
      clearTimeout(this.timer);
      this.timer = setTimeout(() => {
        API.POST("/places/active", {
          active: value,
        });
        this.timer = 0;
      }, 1000);
    }
  }
  handleUnlinkIntegration(id: string) {
    if (id === "line") {
      this.setState(st => {
        const me = createUser(st.me);
        me.connect_line_id = false;
        return { me };
      });
    }
  }

  render = () => (
    <div>
      {this.state.checked && (
        <Layout style={{ minHeight: '100vh' }}>
          {isMobile ? 
            <>
              <MobileMenu
                me={this.state.me}
                contents={this.state.contents}
                currentContentId={this.state.currentContentId}
                selected={this.state.selected}
                headerItem={<ActivePlace
                  list={this.state.placeList}
                  active={this.state.placeActive}
                  onChange={this.handlePlaceChange}
                  />}
                />
              <MainContent
                {...this.props}
                me={this.state.me}
                currentContentId={this.state.currentContentId}
                contents={this.state.contents}
                onDeletePage={this.handleDeletePage}
                onAddPage={this.handleAddPage}
                onUnlinkIntegration={this.handleUnlinkIntegration}
                />
            </>
           :
            <>
              <Layout.Header className={style.header}>
                <ActivePlace
                  list={this.state.placeList}
                  active={this.state.placeActive}
                  onChange={this.handlePlaceChange}
                  />
                <LogoutButton />
              </Layout.Header>
              <Layout>
                <PcMenu
                  me={this.state.me}
                  contents={this.state.contents}
                  currentContentId={this.state.currentContentId}
                  selected={this.state.selected}
                  />
                <MainContent
                  {...this.props}
                  me={this.state.me}
                  currentContentId={this.state.currentContentId}
                  contents={this.state.contents}
                  onDeletePage={this.handleDeletePage}
                  onAddPage={this.handleAddPage}
                  onUnlinkIntegration={this.handleUnlinkIntegration}
                  />
              </Layout>
            </>
          }
          <BackTop />
        </Layout>
      )}
    </div>
  );
}

export default Main;
