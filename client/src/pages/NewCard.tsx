import React, { Component } from 'react';
import { RouteChildrenProps } from 'react-router-dom';
import { Button, notification, Form, Input, Typography, PageHeader, InputNumber, Upload } from 'antd';
import API from 'api/API';

interface Props extends RouteChildrenProps {
}
interface State {
  fileList: any;
}

class NewCard extends Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      fileList: []
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(value: any) {
    (async() => {
      const card = await API.GET(`/cards/${value.key}`);
      if (card.key === value.key) {
        await API.DELETE(`/cards/${value.key}`);
      }
      const newCard = await API.POST("/cards", value);
      const formData = new FormData();
      formData.append("image", this.state.fileList[0]);
      await API.POSTF(`/cards/${newCard.key}/image`, formData);
      notification.success({ message: "追加しました" });
    })()
  }
  render = () => {

    return <>
      <PageHeader title={<Typography.Title>新規カード</Typography.Title>} />
      <Form
        onFinish={this.handleSubmit}
        >
        <Form.Item
          name="key"
          label="キー"
          rules={[{ required: true }]}
          >
          <Input />
        </Form.Item>
        <Form.Item
          name="no"
          label="番号"
          rules={[{ required: true }]}
          >
          <InputNumber />
        </Form.Item>
        <Form.Item
          name="name"
          label="名前"
          rules={[{ required: true }]}
          >
          <Input />
        </Form.Item>
        <Form.Item
          name="name_kana"
          label="名前(かな)"
          rules={[{ required: true }]}
          >
          <Input />
        </Form.Item>
        <Form.Item
          name="set"
          label="カードセット"
          rules={[{ required: true }]}
          >
          <Input placeholder="M21" />
        </Form.Item>
        <Form.Item
          name="cost"
          label="コスト"
          >
          <Input placeholder="(1),(W),(U),(B),(R),(G)" />
        </Form.Item>
        <Form.Item
          name="power"
          label="パワー"
          >
          <Input />
        </Form.Item>
        <Form.Item
          name="toughness"
          label="タフネス"
          >
          <Input />
        </Form.Item>
        <Form.Item
          name="loyalty"
          label="忠誠度"
          >
          <Input />
        </Form.Item>
        <Form.Item
          name="supertype"
          label="特殊タイプ"
          >
          <Input placeholder="伝説の" />
        </Form.Item>
        <Form.Item
          name="cardtype"
          label="カードタイプ"
          rules={[{ required: true }]}
          >
          <Input placeholder="クリーチャー" />
        </Form.Item>
        <Form.Item
          name="subtype"
          label="サブタイプ"
          >
          <Input />
        </Form.Item>
        <Form.Item
          name="rarity"
          label="レアリティ"
          rules={[{ required: true }]}
          >
          <Input />
        </Form.Item>
        <Form.Item
          name="text"
          label="テキスト"
          >
          <Input.TextArea />
        </Form.Item>
        <Form.Item
          name="flavor_text"
          label="フレーバーテキスト"
          >
          <Input.TextArea />
        </Form.Item>
        <Form.Item
          name="image"
          label="画像"
          rules={[{ required: true }]}
          >
          <Upload beforeUpload={file => {
              this.setState(state => ({ fileList: [...state.fileList, file] }));
              return false;
            }} listType="picture" fileList={this.state.fileList}>
            <Button>選択</Button>
          </Upload>
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">作成</Button>
        </Form.Item>
      </Form>
    </>
  };
}

export default NewCard;
