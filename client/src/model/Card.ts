import Enumerable, { IEnumerable } from "linq";


export interface Card {
  key: string;
  no: number;
  name: string;
  name_kana: string;
  set: string;
  cost: string;
  power: string | null;
  toughness: string | null;
  loyalty: string | null;
  supertype: string | null;
  cardtype: string;
  subtype: string | null;
  rarity: string;
  text: string;
  flavor_text: string;
  back_side_key: string | null;
  other_keys: string | null;
  extra_img_url?: string;
}

export interface CardSet {
  code: string;
  name: string;
  name_en: string;
  start_date: moment.Moment;
  release_note: string;
}

export interface CardResult {
  cards: Card[];
  total: number;
}

export const sortCards = (cards: IEnumerable<Card> | Card[]) => {
  if (Array.isArray(cards)) cards = Enumerable.from(cards);
  return cards
    .orderBy(c => c.set + ("000" + c.no).slice(-3))
    .toArray();
};

export const compareSameCard = (card1: Card, card2: Card) => {
  const isLowPriority = (c: Card) => c.subtype === "出来事";
  if (isLowPriority(card1)) return 1;
  if (isLowPriority(card2)) return -1;
  return card1.key < card2.key ? -1 : 1;
};
