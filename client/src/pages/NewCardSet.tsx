import React, { Component } from 'react';
import { RouteChildrenProps } from 'react-router-dom';
import { Button, notification, Form, DatePicker, Input, Typography, PageHeader } from 'antd';
import API from 'api/API';

interface Props extends RouteChildrenProps {
}
interface State {
}

class NewCardSet extends Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(value: any) {
    API.POST("/card_sets", value, e => {
      notification.success({ message: "追加しました" });
    });
  }
  render = () => {

    return <>
      <PageHeader title={<Typography.Title>新規カードセット</Typography.Title>} />
      <Form
        onFinish={this.handleSubmit}
        >
        <Form.Item
          name="code"
          label="コード"
          rules={[{ required: true }]}
          >
          <Input />
        </Form.Item>
        <Form.Item
          name="name"
          label="名前"
          rules={[{ required: true }]}
          >
          <Input />
        </Form.Item>
        <Form.Item
          name="name_en"
          label="名前(英語)"
          rules={[{ required: true }]}
          >
          <Input />
        </Form.Item>
        <Form.Item
          name="start_date"
          label="発売日"
          rules={[{ required: true }]}
          >
          <DatePicker />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">作成</Button>
        </Form.Item>
      </Form>
    </>
  };
}

export default NewCardSet;
