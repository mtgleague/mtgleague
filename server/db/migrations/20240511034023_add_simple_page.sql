
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE simple_page(
    key text not null primary key,
    text text not null
);

INSERT INTO simple_page VALUES
    ('counterapp', '## Android向けアプリ

以下のリンクからインストールしてください。
<p style=''color: #faad14''>事前に提供元不明のアプリのインストールを許可する必要があります。</p>

[ダウンロード](/download/mtgcounter.apk)

'),
    ('changelog', ''),
    ('help', '');

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE simple_page;
