import { createContext } from "react";
import { Card } from "model/Card";

export interface CardCache
{
  [key:string]: Card;
}

export interface CardCacheContextType
{
  cardCache: CardCache;
  setCardCache: React.Dispatch<React.SetStateAction<CardCache>>;
}

export const CardCacheContext = createContext<CardCacheContextType>({
  cardCache: {},
  setCardCache: _ => {}
});
