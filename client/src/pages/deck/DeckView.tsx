import React, { useState, useEffect } from "react";
import API from "api/API";
import Enumerable from "linq";
import { AccessLevel, CardNum, Deck } from "model/Deck";
import { Card, CardSet, compareSameCard, sortCards } from "model/Card";
import { Alert, Button, Col, Descriptions, Dropdown, Input, MenuProps, Modal, notification, PageHeader, Row, Select, Tabs, Tooltip, Typography } from "antd";
import {
  EditOutlined,
  ExclamationCircleOutlined,
  UploadOutlined,
} from '@ant-design/icons';
import { Bar, Column, ColumnConfig } from '@ant-design/plots';
import Cardlist from "pages/cardlist/Cardlist";
import style from 'assets/Decklist.module.css';
import AddButton from "./AddButton";
import { useHistory } from "react-router-dom";
import { User } from "model/User";
import CardSetTags from "./CardSetTags";
import CloneButton from "./CloneButton";
import AccessLevelIcon, { InternalIcon, PrivateIcon, PublicIcon } from "./AccessLevelIcon";
import { ColorSymbol, parseCost, TypeKind, TypeKindName } from "pages/cardlist/Types";
import { BarConfig } from "@ant-design/charts";
import ExtraSearch from "./ExtraSearch";
import { CardCache, CardCacheContext } from "pages/cardlist/CardCacheContext";

const { TabPane } = Tabs;
const { confirm } = Modal;

const PageTitle = (props: {
  me: User,
  title: string,
  author?: string,
  tags: string,
  isEdit?: boolean,
  accessLevel: AccessLevel,
  onBack: () => void,
  onCreate: (name: string) => void,
  onClone: (name: string) => void,
  onEdit: () => void,
  onSave: (name: string, accessLevel: AccessLevel) => void,
  onDelete: () => void,
  onExport: () => void,
}) => {
  const [title, setTitle] = useState(props.title);
  const [accessLevel, setAccessLevel] = useState(props.accessLevel);
  useEffect(() => {
    setTitle(props.title);
    setAccessLevel(props.accessLevel);
  }, [props.title, props.accessLevel]);
  const handleEdit = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTitle(e.target.value);
  };
  const handleLock: MenuProps['onClick'] = e => {
    setAccessLevel(e.key as AccessLevel);
  };
  const lockButtons: MenuProps = {
    items: [
      {
        label: "公開",
        key: "public",
        icon: <PublicIcon />,
      },
      {
        label: "限定公開(URLを知っている人のみ)",
        key: "internal",
        icon: <InternalIcon />,
      },
      {
        label: "非公開",
        key: "private",
        icon: <PrivateIcon />,
      },
    ],
    onClick: handleLock
  };
  const extra: React.ReactNode[] = [];

  if (props.isEdit) {
    extra.push(<Button key="save" type="primary" onClick={() => props.onSave(title, accessLevel)}>保存</Button>);
    extra.push(<Button key="del" danger onClick={props.onDelete}>削除</Button>);
  } else if (props.me.isPerson(props.author || "")) {
    extra.push(
    <Button
      key="edit"
      type="primary"
      onClick={props.onEdit}
      icon={<EditOutlined />}
      >
        編集
      </Button>
    );
  }
  if (!props.isEdit) {
    extra.push(<CloneButton key="clone" name={title + "のコピー"} onClone={props.onClone} />);
    extra.push(<Button
      key="export"
      onClick={props.onExport}
      icon={<UploadOutlined />}
      >エクスポート</Button>);
    extra.push(<AddButton key="new" onCreate={props.onCreate} />);
  }

  let subTitle = "";
  if (props.author) {
    subTitle = `作成者：${props.author}`;
  }

  if (props.isEdit) {
    return <PageHeader
      title={<span>
        <Input
          value={title}
          onChange={handleEdit}
          bordered={false}
          className={style["title-edit"]}
          />
        </span>
        }
      subTitle={
        <span>
          {subTitle}
          <Tooltip title="公開範囲">
            <Dropdown menu={lockButtons}>
              <Button type="text">
                <AccessLevelIcon level={accessLevel} />
              </Button>
            </Dropdown>
          </Tooltip>
          {accessLevel === "private" && <Alert message="現在このデッキは非公開です" type="warning" style={{ display: "inline-block" }} />}
        </span>}
      tags={<CardSetTags value={props.tags} />}
      extra={extra}
      />
  }
  return <PageHeader
    onBack={props.onBack}
    title={<Typography.Title>{title}</Typography.Title>}
    subTitle={
      <span>
        {subTitle}
        <span style={{ padding: "1em", color: "black" }}>
          <AccessLevelIcon showTooltip level={accessLevel} />
        </span>
      </span>}
    tags={<CardSetTags value={props.tags} />}
    extra={extra}
    />
};

const DeckBrakdown = React.memo((props: { cards: Card[] }) => {
  const { cards } = props;
  const [ costData, setCostData ] = useState<any[]>([]);
  const [ colorData, setColorData ] = useState<any[]>([]);
  const [ typeData, setTypeData ] = useState<number[] | null>();

  useEffect(() => {
    setCostData(Enumerable.from(cards)
    .where(c => !c.cardtype.includes("土地"))
    .select(c => {
      const cost = parseCost(c.cost);
      return {
        cost: cost.value < 6 ? "" + cost.value : `6+`,
        type: c.cardtype.includes("クリーチャー") ? "クリーチャー" : "呪文",
      };
    })
    .groupBy(x => x.cost + x.type)
    .select(g => ({
      ...g.elementAt(0),
      value: g.count(),
    }))
    .concat(Enumerable.range(1, 6).selectMany(n => ([
      { cost: n < 6 ? "" + n : `6+`, value: 0, type: "クリーチャー" },
      { cost: n < 6 ? "" + n : `6+`, value: 0, type: "呪文" },
    ])))
    .orderBy(x => x.type + x.cost)
      .toArray());
    setColorData(Enumerable.from(cards)
      .selectMany(c => parseCost(c.cost).color)
      .groupBy(c => c)
      .select(g => ({
        type: "マナシンボル",
        color: g.key(),
        value: g.count(),
        count: g.count(),
      }))
      .concat(Enumerable.from(cards)
        .where(c => c.cardtype.includes("土地"))
        .selectMany(c => {
          const color = new Set<ColorSymbol>();
          if (c.subtype?.includes("平地")) color.add("W");
          if (c.subtype?.includes("島")) color.add("U");
          if (c.subtype?.includes("沼")) color.add("B");
          if (c.subtype?.includes("山")) color.add("R");
          if (c.subtype?.includes("森")) color.add("G");
          if (c.text.match(/^.*[({](tap|T)[)}].*：.*[({]W[)}].*を加える.*$/gm)) color.add("W");
          if (c.text.match(/^.*[({](tap|T)[)}].*：.*[({]U[)}].*を加える.*$/gm)) color.add("U");
          if (c.text.match(/^.*[({](tap|T)[)}].*：.*[({]B[)}].*を加える.*$/gm)) color.add("B");
          if (c.text.match(/^.*[({](tap|T)[)}].*：.*[({]R[)}].*を加える.*$/gm)) color.add("R");
          if (c.text.match(/^.*[({](tap|T)[)}].*：.*[({]G[)}].*を加える.*$/gm)) color.add("G");

          return Array.from(color);
        })
        .groupBy(c => c)
        .select(g => ({
          type: "土地",
          color: g.key(),
          value: g.count(),
          count: g.count(),
        }))
        )
      .toArray());

    const typeData = Enumerable.range(0, TypeKind.Term).select(_ => 0).toArray();
    cards.forEach(c => {
      Enumerable.from(TypeKindName)
        .select((name, i) => ({ name, i }))
        .where(x => c.cardtype.includes(x.name))
        .forEach(x => typeData[x.i]++);
      });
    setTypeData(typeData);
  }, [cards]);

  const constConfig: ColumnConfig = {
    data: costData,
    isStack: true,
    xField: "cost",
    yField: "value",
    seriesField: "type",
    height: 225,
    maxColumnWidth: 30,
    label: {
      content: (data: any) => {
        return data.value > 0 ? data.value : "";
      },
    },
  };
  
  const colorConfig: BarConfig = {
    data: colorData,
    xField: "value",
    yField: "type",
    seriesField: "color",
    isStack: true,
    isPercent: true,
    height: 75,
    legend: false,
    color: (data) => {
      if (data.color === "W") return "#ffe6a7";
      if (data.color === "U") return "#75a8ff";
      if (data.color === "B") return "#805e9c";
      if (data.color === "R") return "#ff8f8f";
      if (data.color === "G") return "#86ed86";
      return "gray";
    },
    label: {
      content: (data: any) => {
        return data.count > 0 ? data.count : "";
      },
    },
  };

  return <Row>
    <Col span={12}><Column {...constConfig} /></Col>
    <Col span={12}>
      <Bar {...colorConfig} />
      <Descriptions
        bordered
        column={2}
        size="small"
        labelStyle={{
          fontSize: 11,
          padding: "4px 6px",
        }}
        contentStyle={{
          padding: "4px 6px",
        }}
        style={{
          marginTop: "1em",
        }}>
        {typeData && typeData.map((d, i) => <Descriptions.Item key={i} label={TypeKindName[i]}>
          {d}
        </Descriptions.Item>)}
      </Descriptions>
    </Col>
  </Row>;
});

const DeckView = (props: {
  id: string,
  me: User,
  isEdit?: boolean,
}) => {
  const { id, isEdit } = props;
  const history = useHistory();
  const [isModify, setIsModify] = useState(false);
  const [pool, setPool] = useState([] as Card[]);
  const [cards, setCards] = useState([] as Card[]);
  const [sideCards, setSideCards] = useState([] as Card[]);
  const [deck, setDeck] = useState(null as (Deck | null));
  const [sets, setSets] = useState([] as CardSet[]);
  const [activeSet, setActiveSet] = useState("");
  const [cardCache, setCardCache] = useState<CardCache>({});

  useEffect(() => {
    (async () => {
      if (!id) return;
      const res = await API.GET(`/decks/${id}`);
      if (res.code && res.code !== 200) {
        history.push("/deck");
        return;
      }
      const deck = res as Deck;
      if (Number(id) === deck.id) {
        history.replace(`/deck/${deck.key}`);
        return;
      }
      setDeck(deck);
      setIsModify(false);

      const cardItems: Card[] = await API.POST(
        "/cards/keys",
        deck.cards.concat(deck.side_cards).map(c => c.key));
      const cardkey2instance = (cards: CardNum[]) => cards.reduce((v, current) => {
        const targetCard = cardItems.find(c => c.key === current.key);
        // 見つからない場合は無視
        if (!targetCard) return v;
        return v.concat(
          Array(current.num)
          .fill(targetCard)
        );
      }, [] as Card[]);
      
      setCards(cardkey2instance(deck.cards));
      setSideCards(cardkey2instance(deck.side_cards));

      const sets = await API.GET("/card_sets");
      setSets(sets);
      setActiveSet(sets[0].code);
    })();
  }, [ id, history ]);
  useEffect(() => {
    if (!activeSet) return;
    API.GET(`/card_sets/${activeSet}/cards`, (pool: Card[]) => {
      const newPool = sortCards(
        Enumerable.from(pool)
        .groupBy(c => c.set + c.no)
        .select(g => g.orderBy(c => c, compareSameCard).first()));
      setPool(newPool);
    });
  }, [activeSet]);
  if (!deck) return <div />;

  const handleEdit = () => {
    history.push(`/deck/${deck.key}/edit`);
  };
  const handleSave = async (name: string, accessLevel: AccessLevel) => {
    const newDeck = JSON.parse(JSON.stringify(deck));
    newDeck.name = name;
    newDeck.access_level = accessLevel;
    newDeck.cards = Enumerable.from(cards)
      .groupBy(c => c.key)
      .select(g => ({
        key: g.key(),
        num: g.count(),
      }))
      .toArray();
      newDeck.side_cards = Enumerable.from(sideCards)
        .groupBy(c => c.key)
        .select(g => ({
          key: g.key(),
          num: g.count(),
        }))
        .toArray();
    newDeck.use_sets = Enumerable.from(cards)
      .concat(sideCards)
      .where(c => c.rarity !== "基本土地")
      .where(c => !c.extra_img_url)
      .select(c => c.set)
      .distinct()
      .orderBy(s => sets.find(v => v.code === s)?.start_date)
      .toJoinedString(",");
    console.log(newDeck);
    // 外部カードは登録が必要
    const extraCards = Enumerable.from(cards)
      .concat(sideCards)
      .where(c => !!c.extra_img_url)
      .distinct(c => c.key)
      .toArray();
    const extraOtherCards = extraCards
      .flatMap(c => [ c.back_side_key, ...(c.other_keys?.split(",") ?? []) ])
      .filter(key => !!key)
      .map(key => cardCache[key!])
    await API.POST(`/cards`, [ ...extraCards, ...extraOtherCards ]);

    API.PUT(`/decks/${deck.id}`, newDeck, () => {
      notification.success({
        message: "デッキを保存しました"
      });
      setIsModify(false);
      setDeck(newDeck);
      history.push(`/deck/${deck.key}`);
    });
  };
  const handleDelete = () => {
    confirm({
      title: 'デッキを削除しますか？',
      icon: <ExclamationCircleOutlined />,
      okText: '削除',
      okType: 'danger',
      cancelText: 'キャンセル',
      onOk: () => {
        API.DELETE(`/decks/${deck.id}`, () => {
          notification.success({
            message: "デッキを削除しました"
          });
          history.push(`/deck`);
        });
      },
      onCancel: () => {},
    });
  };
  const handleBack = () => {
    history.push("/deck");
  };
  const handleNew = (name: string) => {
    if (isModify) {

    }
    API.POST("/decks", {
      name
    }, deck => {
      history.push("/deck/" + deck.key);
    });
  };
  const handleClone = (name: string) => {
    API.POST("/decks", {
      name
    }, cloneDeck => {
      const newDeck: Deck = JSON.parse(JSON.stringify(deck));
      newDeck.name = name;
      newDeck.access_level = "private";
      API.PUT(`/decks/${cloneDeck.id}`, newDeck, () => {
        history.push("/deck/" + cloneDeck.key);
      });
    });
  };
  const handleExport = async () => {
    let card_list: CardCache = {};
    const cards = await API.POST("/cards/keys", [...deck.cards, ...deck.side_cards].map(c => c.key));
    cards.forEach((c: Card) => card_list[c.key] = c);
    const export_data = [];
    export_data.push("#デッキ,,");
    export_data.push("カード,枚数,");
    export_data.push(...deck.cards.map(c => `${card_list[c.key].name},${c.num},`));
    if (deck.side_cards.length > 0) {
      export_data.push("#サイド,,");
      export_data.push("カード,枚数,");
      export_data.push(...deck.side_cards.map(c => `${card_list[c.key].name},${c.num},`));
    }
    const text = export_data.join("\n");

    const blob = new Blob([text], { type: "text/csv" });
    const link = document.createElement('a');
    link.href = URL.createObjectURL(blob);
    link.download =`deck-${deck.key}.csv`;
    link.click();
  }

  return <>
    <PageTitle
      me={props.me}
      title={deck.name}
      author={deck.author}
      tags={deck.use_sets}
      accessLevel={deck.access_level}
      isEdit={isEdit}
      onBack={handleBack}
      onCreate={handleNew}
      onClone={handleClone}
      onEdit={handleEdit}
      onSave={handleSave}
      onDelete={handleDelete}
      onExport={handleExport}
      />
    <Typography>{deck.memo}</Typography>
    <DeckBrakdown cards={cards} />
    <CardCacheContext.Provider value={{ cardCache, setCardCache }}>
      <Tabs>
        <TabPane tab={`デッキ(${cards.length})`} key="deck">
          <Cardlist
            cards={cards}
            canEdit={true}
            onAdd={isEdit ? (card: Card) => {
              setCards(c => sortCards([ ...c, card ]));
            } : undefined}
            onDelete={isEdit ? (card: Card) => {
              const copiedCards = cards.slice();
              const i = copiedCards.findIndex(c => c.key === card.key);
              copiedCards.splice(i, 1);
              setCards(copiedCards);
            } : undefined}
            actions={isEdit ? {
              to_side: (card: Card) => {
                setSideCards(c => sortCards([ ...c, card ]));
              },
            } : {}}
            />
        </TabPane>
        <TabPane tab={`サイド(${sideCards.length})`} key="side">
          <Cardlist
            cards={sideCards}
            canEdit={true}
            onAdd={isEdit ? (card: Card) => {
              setSideCards(c => sortCards([ ...c, card ]));
            } : undefined}
            onDelete={isEdit ? (card: Card) => {
              const copiedCards = sideCards.slice();
              const i = copiedCards.findIndex(c => c.key === card.key);
              copiedCards.splice(i, 1);
              setSideCards(copiedCards);
            } : undefined}
            actions={isEdit ? {
              to_main: (card: Card) => {
                setCards(c => sortCards([ ...c, card ]));
              },
            } : {}}
            />
        </TabPane>
        {isEdit && <TabPane tab={`カードリスト(${pool.length})`} key="list">
          カードセット：<Select defaultValue={activeSet} onChange={v => setActiveSet(v)}>
            {sets.map(s => <Select.Option key={s.code} value={s.code}>
              [{s.code}]{s.name}
            </Select.Option>)}
          </Select>
          <Cardlist
            cards={pool}
            canEdit={false}
            actions={{
              to_main: (card: Card) => {
                setCards(c => sortCards([ ...c, card ]));
              },
              to_side: (card: Card) => {
                setSideCards(c => sortCards([ ...c, card ]));
              }
            }}
            />
        </TabPane>}
        {isEdit && <TabPane tab={`外部カード検索`} key="ext">
          <ExtraSearch
            actions={{
              to_main: (card: Card) => {
                setCards(c => sortCards([ ...c, card ]));
              },
              to_side: (card: Card) => {
                setSideCards(c => sortCards([ ...c, card ]));
              }
            }}
          />
        </TabPane>}
      </Tabs>
    </CardCacheContext.Provider>
  </>;
};

export default DeckView;
