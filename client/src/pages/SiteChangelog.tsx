import React, { useState } from 'react';
import { Typography, PageHeader, MenuProps } from 'antd';
import { DropdownMenu, SimpleMarkdownText } from './PageCommon';

const items: MenuProps['items'] = [
  {
    key: 'edit',
    label: "編集",
  },
];

const SiteChangelog = (props: any) => {

  const [isEdit, setIsEdit] = useState(false);

  const onClick: MenuProps['onClick'] = ({ key }) => {
    if (key === "edit") {
      setIsEdit(true);
    }
  };

  return <>
    <PageHeader
      title={<Typography.Title>更新履歴</Typography.Title>}
      extra={<DropdownMenu
        key="menu"
        menu={{ items, onClick }}
      />} />
    <SimpleMarkdownText
      pageName="changelog"
      isEdit={isEdit}
      onSaved={() => setIsEdit(false)}
      />
  </>
}

export default SiteChangelog;
