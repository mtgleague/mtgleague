
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE global_data(
    key text not null unique,
    date timestamp not null,
    value text not null
);
INSERT INTO global_data VALUES
    ('place_active', now(), ''),
    ('place_list', now(), '');

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE global_data;
