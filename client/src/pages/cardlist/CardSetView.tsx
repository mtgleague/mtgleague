import React, { useEffect, useState } from "react";
import { Card, CardSet, compareSameCard, sortCards } from "model/Card";
import { PageHeader, Select, Typography } from "antd";
import Cardlist from "pages/cardlist/Cardlist";
import API from "api/API";
import Enumerable from "linq";
import { useHistory, useLocation } from "react-router-dom";
import queryString from 'query-string';
import { CardCache, CardCacheContext } from "./CardCacheContext";

const CardSetView = (props: any) => {
  const [pool, setPool] = useState<Card[]>([]);
  const [sets, setSets] = useState<CardSet[]>([]);
  const [activeSet, setActiveSet] = useState("");
  const [cardCache, setCardCache] = useState<CardCache>({});
  const history = useHistory();
  const location = useLocation();

  useEffect(() => {
    (async () => {
      const sets = await API.GET("/card_sets");
      setSets(sets);
      const param = queryString.parse(location.search);
      setActiveSet(sets.some((s: CardSet) => s.code === param.set) ? param.set : sets[0].code);
    })();
  }, [location]);
  useEffect(() => {
    if (!activeSet) return;
    API.GET(`/card_sets/${activeSet}/cards`, (pool: Card[]) => {
      const newPool = sortCards(
        Enumerable.from(pool)
          .groupBy(c => c.set + c.no)
          .select(g => g.orderBy(c => c, compareSameCard).first()));
      setPool(newPool);
      history.replace(`/cardlist?set=${activeSet}`);
    });
  }, [activeSet, history]);

  return <div>
    <PageHeader title={<Typography.Title>カードリスト</Typography.Title>} />
    カードセット：<Select value={activeSet} onChange={v => setActiveSet(v)}>
      {sets.map(s => <Select.Option key={s.code} value={s.code}>
        [{s.code}]{s.name}
      </Select.Option>)}
    </Select>
    <CardCacheContext.Provider value={{cardCache, setCardCache}}>
      <Cardlist
        cards={pool}
        canEdit={false}
        />
    </CardCacheContext.Provider>
  </div>
}

export default CardSetView;
