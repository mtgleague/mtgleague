
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE card_sets ADD release_note text;
UPDATE card_sets SET release_note = '';

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE card_sets DROP COLUMN release_note;
