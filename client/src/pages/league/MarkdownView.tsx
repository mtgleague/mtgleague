import React, { useState } from 'react';
import ReactMarkdown from 'react-markdown';
import {
  Button,
} from 'antd';
import DuelData from './DuelData';
import { User } from 'model/User';
import { EventData } from 'model/Event';
import { Card } from 'model/Card';

const Markdown = (props: { source: string }) => {
  return <ReactMarkdown
    source={props.source}
    escapeHtml={false}
    className="markdown-content"
    />
}

const MarkdownView = (props: {
  content: string,
  contentId?: number,
  me?: User,
  events?: EventData[],
  target_cards?: Card[],
  onEventChange?: () => void,
}) => {
  let text = "";
  let block: { name: string, param?: string }[] = [];
  let result: JSX.Element[] = [];
  let hideIndex = 0;
  const [showFlags, setShowFlags] = useState([] as boolean[]);
  const handleShow = (index: number) => {
    const s = showFlags.slice();
    s[index] = !s[index];
    setShowFlags(s);
  }

  if (!props.content) {
    return null;
  }
  props.content.replace("\r", "").split("\n").forEach((line: string) => {
    let m = line.match(/^#(?<w>\w+)(\(\w+:(?<p>.+?)\))?\{\{/);
    if (m && m.groups) {
      if (text) {
        let child = <Markdown key={result.length} source={text} />;
        result.push(child);
        text = "";
      }
      block.push({
        name: m.groups["w"],
        param: m.groups["p"],
      });
      line = "";
    }
    if (line.match(/^\}\}/)) {
      const item = block.pop();
      if (text) {
        let child = <Markdown key={result.length} source={text} />;
        switch (item?.name) {
          case "hide":
            const index = hideIndex;
            child = <div key={result.length}>
              <Button type="link" onClick={() => handleShow(index)}>{!showFlags[index] ? "▼" : "▲"}{item.param}</Button>
              <div hidden={!showFlags[index]}>{child}</div>
            </div>;
            ++hideIndex;
            break;
          case "font":
            child = <div key={result.length} style={{ fontSize: item.param }}>{child}</div>;
            break;
          case "ml_summary":
            if (props.onEventChange && props.events && props.me) {
              child = <DuelData
                key={result.length}
                me={props.me}
                events={props.events}
                target_cards={props.target_cards}
                contentId={props.contentId}
                onEventChange={props.onEventChange}
                />
            }
            break;
        }
        result.push(child);
      }
      text = "";
    } else {
      text += line + "\n";
    }
  });
  if (text) {
    let child = <Markdown key={result.length} source={text} />;
    result.push(child);
    text = "";
  }
  return <div>{result}</div>;
};

export default MarkdownView;
