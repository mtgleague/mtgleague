import React, { useContext, useState } from 'react';
import { Button } from 'antd';
import style from 'assets/Cardlist.module.css';
import {
  MinusOutlined,
  PlusOutlined,
  SyncOutlined,
} from '@ant-design/icons';
import { Action, Actions, CardData } from './Types';
import { Card } from 'model/Card';
import { CardCache, CardCacheContext } from './CardCacheContext';

interface CardItemProps
{
  card: CardData;
  onDelete?: Action;
  onAdd?: Action;
  canEdit?: boolean;
  actions?: Actions;
  onCardClick?: () => void;
}

export const cardImageUrl = (cardCache: CardCache, card: Card, isReverse?: boolean) => {
  if (card.extra_img_url) {
    if (isReverse && card.back_side_key) {
      return cardCache[card.back_side_key].extra_img_url;
    }
    return card.extra_img_url;
  }
  return isReverse ? `/api/cards/${card.back_side_key}/image` : `/api/cards/${card.key}/image`;
}

const CardItem = (props: CardItemProps) => {
  const { cardCache } = useContext(CardCacheContext);
  const { card, actions } = props;
  const action = (key: string) => {
    if (actions && actions[key]) {
      actions[key](card.card);
    }
  };
  const [ isReverse, setIsReverse ] = useState(false);
  return <div>
    <div className={style.card}>
      <img
        alt={card.card.name}
        className={style["card-img"]}
        style={{ width: "100%" }}
        src={cardImageUrl(cardCache, card.card, isReverse)}
        onClick={props.onCardClick}
        />
      <div className={style.actions}>
        {actions && Object.keys(actions).map(key => (
          <Button key={key} onClick={() => action(key)}>{key}</Button>
        ))}
      </div>
      {card.card.back_side_key ?
        <Button
          className={style["card-reverse"]}
          shape="circle" icon={<SyncOutlined />}
          onClick={() => setIsReverse(!isReverse)}
          /> : <div className={style["card-dummy-space"]} />}
    </div>
    {props.canEdit && ((props.onAdd && props.onDelete) ? (
      <Button.Group className={style["card-count"]}>
        <Button onClick={() => props.onDelete?.(card.card)}>
          <MinusOutlined />
        </Button>
        <Button type="text">
          {card.count}
        </Button>
        <Button onClick={() => props.onAdd?.(card.card)}>
          <PlusOutlined />
        </Button>
      </Button.Group>
    ) : (<div className={style["card-count"]}>{card.count}枚</div>))}
  </div>;
};

export default CardItem;
