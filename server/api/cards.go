package api

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"

	"example.com/m/model"
	"github.com/gin-gonic/gin"
)

// RegisterCardsAPI は`/cards`以下のAPIを登録します
func RegisterCardsAPI(context *Context) {
	context.DBmap.AddTableWithName(model.Card{}, "cards").SetKeys(false, "key")
	context.DBmap.AddTableWithName(model.CardSet{}, "card_sets")

	context.APIAuth.GET("/cards", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		limit, err := strconv.Atoi(c.Query("limit"))
		if err != nil {
			limit = 500
		}
		page, err := strconv.Atoi(c.Query("page"))
		if err != nil {
			page = 1
		}
		query := "true"
		queryData := []interface{}{}
		cardSetsStr, _ := c.GetQuery("sets")
		if len(cardSetsStr) > 0 {
			queryData = append(queryData, cardSetsStr)
			query += " AND set=ANY(string_to_array($" + strconv.Itoa(len(queryData)) + ", ' '))"
		}
		rarity, _ := c.GetQuery("rarity")
		if len(rarity) > 0 {
			queryData = append(queryData, rarity)
			query += " AND rarity=ANY(string_to_array($" + strconv.Itoa(len(queryData)) + ", ' '))"
		}
		cardtype, _ := c.GetQuery("cardtype")
		if len(cardtype) > 0 {
			query += " AND (false"
			for _, v := range strings.Split(cardtype, " ") {
				queryData = append(queryData, "%"+v+"%")
				i := strconv.Itoa(len(queryData))
				query += " OR cardtype LIKE $" + i
			}
			query += ")"
		}
		subtype, _ := c.GetQuery("subtype")
		if len(subtype) > 0 {
			query += " AND (false"
			for _, v := range strings.Split(subtype, " ") {
				queryData = append(queryData, "%"+v+"%")
				i := strconv.Itoa(len(queryData))
				query += " OR subtype LIKE $" + i
			}
			query += ")"
		}
		supertype, _ := c.GetQuery("supertype")
		if len(supertype) > 0 {
			query += " AND (true"
			for _, v := range strings.Split(supertype, " ") {
				queryData = append(queryData, "%"+v+"%")
				i := strconv.Itoa(len(queryData))
				query += " AND supertype LIKE $" + i
			}
			query += ")"
		}
		text, _ := c.GetQuery("text")
		if len(text) > 0 {
			for _, v := range strings.Split(text, " ") {
				queryData = append(queryData, "%"+v+"%")
				i := strconv.Itoa(len(queryData))
				query += " AND (text LIKE $" + i +
					" OR c.name LIKE $" + i +
					" OR name_kana LIKE $" + i + ")"
			}
		}
		// 外部カードは検索対象外
		query += " AND c.extra_img_url IS NULL"

		var result model.CardResult
		result.Total = helper.selectInt(
			"SELECT COUNT(*) FROM cards AS c WHERE "+query,
			queryData...,
		)

		queryLen := len(queryData)
		limitQuery := "OFFSET $" + strconv.Itoa(queryLen+1) + " LIMIT $" + strconv.Itoa(queryLen+2)
		queryData = append(queryData, (page-1)*limit, limit)
		helper.selectAny(
			&result.Cards,
			"SELECT key,no,c.name as name,name_kana,set,s.name as set_name,cost,power,toughness,loyalty,supertype,cardtype,subtype,rarity,text,flavor_text,back_side_key,other_keys "+
				"FROM cards AS c LEFT JOIN card_sets AS s ON s.code=c.set "+
				"WHERE "+query+" ORDER BY s.start_date DESC, c.key "+limitQuery,
			queryData...,
		)
		if helper.err != nil {
			return
		}

		c.JSON(http.StatusOK, result)
	})
	context.APIAuth.POST("/cards/:key", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		helper.verifyUser()
		key := helper.getParamStr("key")

		if key != "keys" {
			setNotFoundResult(c, "not found")
			return
		}

		var keys []string
		err := c.ShouldBindJSON(&keys)
		if err != nil {
			setBadRequestResult(c, err.Error())
			return
		}

		var cards []model.Card
		helper.selectAny(
			&cards,
			"SELECT * FROM cards WHERE key=ANY(string_to_array($1, ',')) ORDER BY key",
			strings.Join(keys, ","),
		)
		if helper.err != nil {
			return
		}
		c.JSON(http.StatusOK, cards)
	})
	context.APIAuth.GET("/cards/:key", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		key := helper.getParamStr("key")

		var card model.Card
		helper.selectOne(&card, "SELECT * FROM cards WHERE key=$1", key)
		if helper.err != nil {
			return
		}
		c.JSON(http.StatusOK, card)
	})
	context.APIAuth.POST("/cards", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		helper.verifyUser()

		var cards []model.Card
		err := c.ShouldBindJSON(&cards)
		if err != nil {
			setBadRequestResult(c, err.Error())
			return
		}
		keys := make([]string, len(cards))
		for i, c := range cards {
			keys[i] = c.Key
		}
		var addCards []model.Card

		helper.begin()
		var existKeys []struct {
			Key string `db:"key"`
		}
		helper.selectAny(&existKeys,
			"SELECT key FROM cards WHERE key=ANY(string_to_array($1, ','))",
			strings.Join(keys, ","),
		)
		for _, c := range cards {
			exist := false
			for _, k := range existKeys {
				if c.Key == k.Key {
					exist = true
					break
				}
			}
			if !exist {
				helper.insert(&c)
				addCards = append(addCards, c)
			}
		}
		helper.commit()
		if helper.err != nil {
			return
		}
		c.JSON(http.StatusOK, addCards)
	})
	context.APIAuth.DELETE("/cards/:key", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		helper.verifyUser()
		key := helper.getParamStr("key")

		helper.begin()
		var card model.Card
		helper.selectOne(&card, "SELECT * FROM cards WHERE key=$1", key)
		helper.delete(&card)
		helper.commit()
		if helper.err != nil {
			return
		}
		c.JSON(http.StatusOK, gin.H{
			"status": "OK",
		})
	})
	context.API.GET("/cards/:key/image", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		key := helper.getParamStr("key")

		var card model.CardSetForImage
		helper.selectOne(&card, "SELECT key, set FROM cards WHERE key=$1", key)
		if helper.err != nil {
			return
		}
		c.Header("Cache-Control", "public, max-age=604800, immutable")
		c.File("./image/cards/" + card.Set + "/" + card.Key + ".png")
	})
	context.APIAuth.POST("/cards/:key/image", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		helper.verifyUser()
		key := helper.getParamStr("key")

		fh, err := c.FormFile("image")
		if err != nil {
			setBadRequestResult(c, "invalid file.")
			return
		}

		file, err := fh.Open()
		if err != nil {
			setServerError(c, err)
			return
		}
		defer file.Close()

		buff := make([]byte, 512)
		_, err = file.Read(buff)
		if err != nil {
			setServerError(c, err)
			return
		}
		// ファイルタイプチェック
		if http.DetectContentType(buff) != "image/png" {
			setBadRequestResult(c, "image content type is not `image/png`.")
			return
		}
		_, err = file.Seek(0, io.SeekStart)
		if err != nil {
			setServerError(c, err)
			return
		}

		var card model.Card
		helper.selectOne(&card, "SELECT * FROM cards WHERE key=$1", key)
		if helper.err != nil {
			return
		}
		setPath := "./image/cards/" + card.Set
		if _, err := os.Stat(setPath); err != nil {
			fmt.Println("create dir " + setPath)
			os.Mkdir(setPath, 0775)
		}
		path := setPath + "/" + card.Key + ".png"
		// コピー先ファイル
		output, err := os.Create(path)
		if err != nil {
			setServerError(c, err)
			return
		}
		defer output.Close()
		// コピー
		_, err = io.Copy(output, file)
		if err != nil {
			setServerError(c, err)
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"status": "OK",
		})
	})

	context.APIAuth.GET("/card_sets", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}

		var sets []model.CardSet

		helper.selectAny(&sets, "SELECT * FROM card_sets ORDER BY start_date DESC")
		if helper.err != nil {
			return
		}
		c.JSON(http.StatusOK, sets)
	})
	context.APIAuth.POST("/card_sets", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		helper.verifyUser()

		var set model.CardSet
		err := c.ShouldBindJSON(&set)
		if err != nil {
			setBadRequestResult(c, err.Error())
			return
		}

		helper.insert(&set)
		if helper.err != nil {
			return
		}
		c.JSON(http.StatusOK, set)
	})
	context.APIAuth.GET("/card_sets/:code/cards", func(c *gin.Context) {
		helper := apiHelper{
			context: c,
			dbmap:   context.DBmap,
		}
		code := helper.getParamStr("code")

		helper.begin()
		var set model.CardSet
		helper.selectOne(&set, "SELECT * FROM card_sets WHERE code=$1", code)
		var cards []model.Card
		helper.selectAny(&cards, "SELECT * FROM cards WHERE set=$1 AND extra_img_url IS NULL ORDER BY key", code)
		helper.commit()
		if helper.err != nil {
			return
		}
		c.JSON(http.StatusOK, cards)
	})
}
