
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE cards(
    key text not null primary key,
    no integer not null,
    name text not null,
    name_kana text not null,
    set text not null,
    cost text,
    power text,
    toughness text,
    loyalty text,
    supertype text,
    cardtype text not null,
    subtype text,
    reality text not null,
    text text not null,
    flavor_text text not null,
    image bytea
);
CREATE TABLE card_sets(
    code text not null primary key,
    name text not null,
    name_en text not null,
    start_date date not null
);

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE cards;
DROP TABLE card_sets;
