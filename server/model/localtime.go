package model

import (
	"database/sql/driver"
	"time"
)

func scanLocalTime(value interface{}) time.Time {
	// DBから来たデータはUTCになるのでLocalの時間に調整する
	t := value.(time.Time).In(time.Local)
	_, offset := t.Zone()
	return t.Add(time.Duration(-offset) * time.Second)
}

// LocalDateTime はTimeZone=LocalでJSON<=>DBを変換するためのDateTime型
type LocalDateTime struct {
	time.Time
}

func (u LocalDateTime) format() string {
	s := u.Time.Format("2006-01-02T15:04:05.999999-07:00")
	return s
}

// MarshalJSON はJSONへの変換
func (u LocalDateTime) MarshalJSON() ([]byte, error) {
	return []byte(`"` + u.format() + `"`), nil
}

// Value はDBに保存する値に変換
func (u LocalDateTime) Value() (driver.Value, error) {
	return u.Time, nil
}

// Scan はDBから読み込んだ値をLocalDateに変換
func (u *LocalDateTime) Scan(value interface{}) error {
	u.Time = scanLocalTime(value)
	return nil
}

// LocalDate はTimeZone=LocalでJSON<=>DBを変換するためのDate型
type LocalDate struct {
	time.Time
}

func (u LocalDate) format() string {
	return u.Time.Format("2006-01-02")
}

// MarshalJSON はJSONへの変換
func (u LocalDate) MarshalJSON() ([]byte, error) {
	return []byte(`"` + u.format() + `"`), nil
}

// Value はDBに保存する値に変換
func (u LocalDate) Value() (driver.Value, error) {
	return u.Time, nil
}

// Scan はDBから読み込んだ値をLocalDateに変換
func (u *LocalDate) Scan(value interface{}) error {
	u.Time = scanLocalTime(value)
	return nil
}
