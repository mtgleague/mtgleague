import React, { Component } from 'react';
import { EventData } from 'model/Event';
import DuelTable from './DuelTable';
import DuelEventHistory, { ChangeEventCallback } from './DuelEventHistory';
import DuelDetailTable from './DuelDetailTable';
import DuelEventEdit from './DuelEventEdit';
import { User } from 'model/User';
import API from 'api/API';
import { Card } from 'model/Card';
import { Switch } from 'antd';
import style from 'assets/League.module.css';

class DuelData extends Component<{
  me: User,
  events: EventData[],
  target_cards?: Card[],
  contentId?: number,
  onEventChange: ChangeEventCallback
},
  any
  > {

  constructor(props: any) {
    super(props);
    this.state = {
      users: [] as User[],
      events: props.events,
      myEventOnly: false,
    };
    this.handleMyEventOnlyChange = this.handleMyEventOnlyChange.bind(this);
  }
  componentDidMount() {
    if (!this.props.me.isGuest()) {
      API.GET("/users?exclude_guest=1", (e: User[]) => {
        // 自分が一番上、それ以外はアルファベット順にソート
        const users = e.sort((a, b) => {
          if (a.name === this.props.me.name) return -1;
          if (b.name === this.props.me.name) return 1;
          if (a.name < b.name) return -1;
          if (a.name > b.name) return 1;
          return 0;
        })
        this.setState({ users });
      });
    }
  }
  componentDidUpdate(oldProps: any) {
    if (this.props.events !== oldProps.events) {
      this.setState({ events: this.props.events });
    }
  }

  handleMyEventOnlyChange(checked: boolean) {
    this.setState({ myEventOnly: checked });
  }
  render = () => {
    const events = this.state.events;
    const users = this.state.users;

    return <>
      <DuelTable events={events} />
      <DuelDetailTable me={this.props.me} events={events} />
      <div className={style["my-event-only-switch"]}>
        <Switch
        checked={this.state.myEventOnly}
        onChange={this.handleMyEventOnlyChange}
        />　自分の対戦記録のみ表示する
      </div>
      <DuelEventHistory
        me={this.props.me}
        events={events}
        users={users}
        cards={this.props.target_cards}
        myEventOnly={this.state.myEventOnly}
        onChangeEvent={this.props.onEventChange}
        />
      {!this.props.me.isGuest() && <DuelEventEdit mode="add"
        me={this.props.me}
        users={users}
        cards={this.props.target_cards}
        contentId={this.props.contentId}
        />}
    </>
  }
}

export default DuelData;
