
class Socket {
  private static socket: WebSocket | null = null;
  private static callbacks: {
    [key: string]: ((value: any) => void)[]
  } = {};

  static connect() {
    if (this.socket !== null) {
      this.disconnect();
    }
    const location = window.location;
    const protocol = location.protocol === "https:" ? "wss:" : "ws:";
    const hostname = location.hostname;
    const port = process.env.REACT_APP_WS_PORT || location.port;
    const host = hostname + (port ? ":" + port : "");
    const url = `${protocol}//${host}/ws`;
    console.log(`connect socket to ${url}.`);
    this.socket = new WebSocket(url);

    this.socket.onmessage = e => {
      const d = JSON.parse(e.data);
      console.log(d);
      if (this.callbacks[d.key]) {
        this.callbacks[d.key].forEach(c => c(d.value));
      }
    };
  }

  static disconnect() {
    console.log("disconnect socket.");
    if (this.socket !== null) {
      this.socket.close();
      this.socket = null;
    }
  }

  static addListener(key: string, callback: (value: any) => void) {
    if (!this.callbacks[key]) {
      this.callbacks[key] = [];
    }
    this.callbacks[key].push(callback);
  }

  static removeListener(key: string, callback: (value: any) => void) {
    const list = this.callbacks[key];
    if (list) {
      const i = list.indexOf(callback);
      if (i !== -1) {
        list.splice(i, 1);
      }
    }
  }
}

export default Socket;
